#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 24 09:50:22 2019

@author: schoko
"""


import sys 
if '/home/schoko/Documents/grundpraktikum-2/Python/plottingTemplate' not in sys.path:
    sys.path.append('/home/schoko/Documents/grundpraktikum-2/Python/plottingTemplate')

import plotting as pt
import matplotlib.pyplot as plt
import matplotlib
import scipy.odr
import scipy.optimize as opt
import numpy as np
from scipy.special import gamma

font = {'family' : 'DejaVu Sans',
        'weight' : 'normal',
        'size'   : 18}

matplotlib.rc('font', **font)

def histo(a,bins):
    res=[0.0 for i in bins]
    for x in a:
        for i in range(len(bins)-1):
            if bins[i] <= x < bins[i+1] :
                res[i]+=1.0
    diff=bins[1]-bins[0]
    le=bins[-1]-bins[0]
    norm=0.0
    for xi in res:
        norm+=diff*xi
    reserr=  [np.sqrt(ri)  for ri in res]
    return np.array(res)/norm , np.array(reserr)/norm

gausscurve = lambda x,A,x0,s : A*np.exp(-(x-x0)**2/(2*s**2))

poisscurve = lambda x,A,mu,s : A*mu**(x)*np.exp(-mu)/gamma(x+1) 

exponcurve = lambda x,A,a    : 1/A*np.exp(-1/A*x)

cauchycurve= lambda x,A,x0,j,b : A*j**2/((x-x0)**2+j**2)+b

parabcurve = lambda x,A,D,x0,p : A*(D-1/2.0*p-1/(2.0*p)*(x-x0)**2)

parabcurvep = lambda x,A,D,x0 : A*(D-1/2.0*0.78-1/(2.0*0.78)*(x-x0)**2)

def beamcross (x,A, D,x0,p):
    if x<(-p):
        return A*(D+(x-x0))
    if x>p:
        return A*(D-(x-x0))
    return A*(D-1/2.0*p-1/(2.0*p)*(x-x0)**2)

def chisquare (x,y,xerr,yerr,curve, mean, var):
    h=0.0002
    curveabl=[(curve(xi+h/2 ,mean, var)-curve(xi-h/2,mean, var))/h for xi in x]
    return sum([(y[i]-curve(x[i],mean, var))**2/((yerr[i])**2 +(curveabl[i]*xerr[i])**2 ) if y[i]!=0 else 0 for i in range(len(x))])

def fitparab(x,y,ex,ey,A,D,x0,p):
    
    def f(b, x):
        return b[0]*(b[1]-p/2.0-1/(2.0*p)*(x-b[2])**2) 
    
    model  = scipy.odr.Model(f)
    data   = scipy.odr.RealData(x,y, sx=ex, sy=ey)
    odr    = scipy.odr.ODR(data, model, beta0=[A,D,x0])
    output = odr.run()

    return output.beta, output.cov_beta

filenames="Magnet0"

mag1=[]
x1=[]

mag2=[]
x2=[]
for line in open(filenames+".dat","r"):
    line=line.split("\t")
    x1.append(float(line[1])*1.8)
    mag1.append(float(line[2]))
    if (float(line[3])<50):
        x2.append(float(line[3])*1.8)
        mag2.append(float(line[4]))
    
 
yerr=0.03
xerr=0.1*1.8/np.sqrt(12)
print(xerr)
   
x1=np.array(x1)-x1[mag1.index(max(mag1))]-0.18
mag1=np.array(mag1)
x2=np.array(x2)-x2[mag2.index(max(mag2))]+0.02
mag2=np.array(mag2)

mag1-=2.
mag2-=2.
plt.figure(figsize=(14,8))

plt.xlabel('x in mm')
plt.ylabel('I in mA')

pt.plot_errorbar(x1,mag1, [xerr for i in x1], [yerr for i in x1],'Messung 1','best','blue')
pt.plot_errorbar(x2,mag2, [xerr for i in x2], [yerr for i in x2],'Messung 2','best','red')
xbins=np.linspace(min(x1),max(x1),200)
pt.plot(xbins,[0 for i in xbins], 'Untergrund = 2.2', 'best', 'green', '-')
#pt.plot(x2[a:b],mag2[a:b],'Messung 1','best','blue')
#plt.vlines(direkt[n], 0, 10000)
plt.grid()
#plt.yscale('log')
#plt.xlim([min(delta1s[n]),max(delta1s[n])])
#plt.xlim([-2,2])
#plt.ylim([1000,40000])
plt.subplots_adjust(left=0.1, right=1, top=1, bottom=0.1)
plt.savefig(filenames+'ohne_untergrund.pdf')#'Linearscale'+'.pdf')
plt.show()




a,b = 19, 36
p=x2[b]+x2[b-1]
p/=2

params, cov = opt.curve_fit(parabcurvep , x2[a:b],mag2[a:b],[8, 1.5, 0])
print(params)

params, cov=fitparab(x2[a:b],mag2[a:b],[xerr for i in x2[a:b]], [yerr for i in x2[a:b]],params[0], params[1], params[2] ,p)
print(params)


plt.figure(figsize=(14,8))

plt.xlabel('x in mm')
plt.ylabel('I in mA')


pt.plot_errorbar(x2,mag2, [xerr for i in x2], [yerr for i in x2] ,'Messung 2','best','red')
xbins=np.linspace(-params[1],params[1],200)
#pt.plot(xbins,[beamcross(i,params1[0], 0, params1[2], params1[3]) for i in xbins], 'Beam Crossection Fit 1', 'best', 'cyan', '-')
pt.plot(xbins,[beamcross(i,params[0] , params[1], 0, p) for i in xbins], 'Beam Crossection Fit 2', 'best', 'green', '-')

perr=xerr/np.sqrt(2)
pt.plot([params[1],params[1]+0.0000001], [0,0] ,' $i_0$ '+'= {:02.01f} $\pm$ {:02.01f}\n $D$ = {:02.03f} $\pm$ {:02.03f}\n $p$ = {:02.02f} $\pm$ {:02.02f}'.format(params[0], cov[0][0],params[1], cov[1][1],p, perr),'best' ,'white')
        
#xbins=np.linspace(-3,3,200)
#pt.plot(xbins, [beamcross(i, 1.3, -0.015 , 1, 0.5 ) for i in xbins], 'Beam Crossection Fit')

pt.plot(x2[a:b],mag2[a:b],'Zum Fit verwendete Werte','best','blue')
#plt.vlines(direkt[n], 0, 10000)
plt.grid()
#plt.yscale('log')
#plt.xlim([min(delta1s[n]),max(delta1s[n])])
plt.xlim([-2,2])
#plt.ylim([1000,40000])
plt.subplots_adjust(left=0.1, right=1, top=1, bottom=0.1)
plt.savefig(filenames+'_Beam_fit.pdf')#'Linearscale'+'.pdf')
plt.show()

 