#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 25 11:20:22 2019

@author: schoko
"""

import sys 
if '/home/schoko/Documents/grundpraktikum-2/Python/plottingTemplate' not in sys.path:
    sys.path.append('/home/schoko/Documents/grundpraktikum-2/Python/plottingTemplate')

import plotting as pt
import matplotlib.pyplot as plt
import matplotlib
import scipy as sc
import scipy.optimize as opt
import scipy.odr
import numpy as np
from scipy.special import gamma

font = {'family' : 'DejaVu Sans',
        'weight' : 'normal',
        'size'   : 18}

matplotlib.rc('font', **font)

def lineare_regression(x,y,ey):
    '''
    Lineare Regression.

    :param x: x-Werte der Datenpunkte
    :type x: array_like
    :param y: y-Werte der Datenpunkte
    :type y: array_like
    :param ey: Fehler auf die y-Werte der Datenpunkte
    :type ey: array_like

    Diese Funktion benötigt als Argumente drei Listen:
    x-Werte, y-Werte sowie eine mit den Fehlern der y-Werte.
    Sie fittet eine Gerade an die Werte und gibt die
    Steigung a und y-Achsenverschiebung b mit Fehlern
    sowie das :math:`\chi^2` und die Korrelation von a und b aus.

    :rtype: Liste der Fit-Ergebnisse in der Reihenfolge [a, ea, b, eb, chiq, corr].

    '''

    s   = sum(1./ey**2)
    sx  = sum(x/ey**2)
    sy  = sum(y/ey**2)
    sxx = sum(x**2/ey**2)
    sxy = sum(x*y/ey**2)
    delta = s*sxx-sx*sx
    b   = (sxx*sy-sx*sxy)/delta
    a   = (s*sxy-sx*sy)/delta
    eb  = np.sqrt(sxx/delta)
    ea  = np.sqrt(s/delta)
    cov = -sx/delta
    corr = cov/(ea*eb)
    chiq  = sum(((y-(a*x+b))/ey)**2)

    return(a,ea,b,eb,chiq,corr)

def lineare_regression_xy(x,y,ex,ey):
    
    a_ini,ea_ini,b_ini,eb_ini,chiq_ini,corr_ini = lineare_regression(x,y,ey)

    def f(B, x):
        return B[0]*x + B[1]

    model  = scipy.odr.Model(f)
    data   = scipy.odr.RealData(x, y, sx=ex, sy=ey)
    odr    = scipy.odr.ODR(data, model, beta0=[a_ini, b_ini])
    output = odr.run()
    ndof = len(x)-2
    chiq = output.res_var*ndof
    corr = output.cov_beta[0,1]/np.sqrt(output.cov_beta[0,0]*output.cov_beta[1,1])

    # Es scheint, dass die sd_beta von ODR auf ein chi2/dof=1 reskaliert werden. Deshalb nehmen
    # wir den Fehler direkt aus der Kovarianzmatrix.
    return output.beta[0], np.sqrt(output.cov_beta[0,0]), output.beta[1], np.sqrt(output.cov_beta[1,1]), chiq, corr


def histo(a,bins):
    res=[0.0 for i in bins]
    for x in a:
        for i in range(len(bins)-1):
            if bins[i] <= x < bins[i+1] :
                res[i]+=1.0
    diff=bins[1]-bins[0]
    le=bins[-1]-bins[0]
    norm=0.0
    for xi in res:
        norm+=diff*xi
    reserr=  [np.sqrt(ri)  for ri in res]
    return np.array(res)/norm , np.array(reserr)/norm

gausscurve = lambda x,A,x0,s : A*np.exp(-(x-x0)**2/(2*s**2))

poisscurve = lambda x,A,mu,s : A*mu**(x)*np.exp(-mu)/gamma(x+1) 

exponcurve = lambda x,A,a    : 1/A*np.exp(-1/A*x)

cauchycurve= lambda x,A,x0,j,b : A*j**2/((x-x0)**2+j**2)+b

parabcurve = lambda x,A,x0,D,p : A*(D-1/2.0*p-1/(2.0*p)*(x-x0)**2)

lincurve   = lambda x,a,b : a*x+b

def beamcross (x,A, x0,D,p):
    if x<(-p):
        return A*(D+(x-x0))
    if x>p:
        return A*(D-(x-x0))
    return A*(D-1/2.0*p-1/(2.0*p)*(x-x0)**2)

def chisquare (x,y,xerr,yerr,curve, mean, var):
    h=0.0002
    curveabl=[(curve(xi+h/2 ,mean, var)-curve(xi-h/2,mean, var))/h for xi in x]
    return sum([(y[i]-curve(x[i],mean, var))**2/((yerr[i])**2 +(curveabl[i]*xerr[i])**2 ) if y[i]!=0 else 0 for i in range(len(x))])

u0=   [0   ,0.64000000000000012, 1.1864138018710912,   1.8782769568985975,  1.8471032130202123,   2.1264566426932534]
u0err=[0.03,0.14696938456699071, 0.036742346141747678, 0.10712142642814276, 0.036742346141747678, 0.036742346141747678]

u0=np.array(u0)
u0err=np.array(u0err)

B=np.array([0,0.283,0.422,0.545, 0.650,0.715])
B_8=[0.545]
B_8err=[0.01]
Berr=np.array([0.01 for i in B])

a,ea,b,eb,chiq,corr=lineare_regression_xy(B, u0, Berr, u0err)

plt.figure(figsize=(14,8))

print(chisquare(B,u0,Berr,u0err, lincurve, a,b)/4)

xbins= np.linspace(0,0.8,200)
pt.plot_errorbar_and_plot(B,u0,Berr,u0err, 'Messungen',xbins, lincurve(xbins, a,b),'$ \chi^2 / ndf $ = {:02.02f} \nSteigung {:02.02f} $\pm$ {:02.02f}'.format(chiq/(len(u0)-2), a,ea),'best')

#pt.plot_errorbar_red(B_8,u0_8,B_8err, u0_8err, 'deleted', 'best')

m=a/1000
merr = ea /1000
Lg=0.07
Lk=0.455
a=0.0025
T=438.15
gs=2.00232

muB= m*a*12*T*1.38065*10**-23/(Lg*Lk*(1-Lg/(2*Lk))*gs*0.9661) #

muBerr = merr*a*12*T*1.38065*10**-23/(Lg*Lk*(1-Lg/(2*Lk))*gs*0.9661)

print(muB, muBerr)

pt.plot([0,0.0000001], [0,0] ,' $\mu_B$ '+'= ({:02.02f} $\pm$ {:02.02f} )'.format(muB*10**24,muBerr*10**24)+'$\cdot 10^{-24}$ ','best' ,'white')


plt.xlabel('B in T')
plt.ylabel('$u_0$ in mm')

plt.grid()

plt.subplots_adjust(left=0.1, right=1, top=1, bottom=0.1)
plt.savefig('mu0MethA'+'.pdf')#'Linearscale'+'.pdf')
plt.show()

#################################### Methode B ###########################################
i=0
u0=np.delete(u0,i)
u0err=np.delete(u0err,i)
B=np.delete(B,i)
Berr=np.delete(Berr,i)

u0red=u0
u0rederr=u0err
Bred=B
Brederr=Berr
'''
u0red=np.array([0.665,1.8891])
u0rederr=[0.1 for i in u0red]
Bred=[0.283,0.545]
Brederr=np.array([0.01 for i in Bred])
'''
#for i in [0,1]:
#    u0=np.delete(u0,i)
#    u0err=np.delete(u0err,i)
#    B=np.delete(B,i)
#    Berr=np.delete(Berr,i)


D=1.484
Derr=0.004
p=0.78
perr=0.04

calc = lambda u : 3*u-(D**4-0.2*p**4)/(D**2-(p**2)/3)/u

u0 = calc(u0)

ethaerr2=(((Derr*4.0*D**3)**2+(4/5.0*perr*p**3)**2)/(D**2 - 1/3.0 * p**2)**2)+(((Derr*2.0*D)**2+(2/3.0*perr*p)**2)/(D**2 - 1/3.0 * p**2)**4)

print((D**4-0.2*p**4)/(D**2-(p**2)/3))
print(ethaerr2)

calcerr = lambda u,ue :(3*ue)**2+(ethaerr2/u**2)+(ue*(D**4-0.2*p**4)/(D**2-(p**2)/3)/u**2)**2

u0err=np.array([np.sqrt(calcerr(u0[j], u0err[j])) for j in range(len(u0))])
print(u0err)

calcerr = lambda u,ue :(3*ue)**2+                (ue*(D**4-0.2*p**4)/(D**2-(p**2)/3)/u**2)**2

u0err=np.array([np.sqrt(calcerr(u0[j], u0err[j])) for j in range(len(u0))])
print(u0err)

a,ea,b,eb,chiq,corr=lineare_regression_xy(B, u0, Berr, u0err)

plt.figure(figsize=(14,8))

xbins= np.linspace(0,0.8,200)

pt.plot_errorbar_and_plot(B,u0,Berr,u0err, 'Messungen',xbins, lincurve(xbins, a,b),'$ \chi^2 / ndf $ = {:02.02f} \nSteigung {:02.01f} $\pm$ {:02.01f}'.format(chiq/(len(u0)-2), a,ea),'best')



m=a/1000
merr = ea /1000
Lg=0.07
Lk=0.455
a=0.0025
T=438.15
gs=2.00232

muB= m*a*4*T*1.38065*10**-23/(Lg*Lk*(1-Lg/(2*Lk))*gs*0.9661) #

muBerr = merr*a*4*T*1.38065*10**-23/(Lg*Lk*(1-Lg/(2*Lk))*gs*0.9661)

print(muB, muBerr)

pt.plot([0,0.0000001], [0,0] ,' $\mu_B$ '+'= ({:02.01f} $\pm$ {:02.01f} )'.format(muB*10**24,muBerr*10**24)+'$\cdot 10^{-24}$ ','best' ,'white')


plt.xlabel('B in T')
plt.ylabel('$\eta$ in mm')

plt.grid()

plt.subplots_adjust(left=0.1, right=1, top=1, bottom=0.1)
plt.savefig('mu0MethB'+'.pdf')#'Linearscale'+'.pdf')
plt.show()


 