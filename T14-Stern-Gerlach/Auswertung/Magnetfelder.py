#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 24 09:50:22 2019

@author: schoko
"""


import sys 
if '/home/schoko/Documents/grundpraktikum-2/Python/plottingTemplate' not in sys.path:
    sys.path.append('/home/schoko/Documents/grundpraktikum-2/Python/plottingTemplate')

import plotting as pt
import matplotlib.pyplot as plt
import matplotlib
import scipy.odr 
import scipy.optimize as opt
import numpy as np
from scipy.special import gamma

font = {'family' : 'DejaVu Sans',
        'weight' : 'normal',
        'size'   : 18}

matplotlib.rc('font', **font)

def histo(a,bins):
    res=[0.0 for i in bins]
    for x in a:
        for i in range(len(bins)-1):
            if bins[i] <= x < bins[i+1] :
                res[i]+=1.0
    diff=bins[1]-bins[0]
    #le=bins[-1]-bins[0]
    norm=0.0
    for xi in res:
        norm+=diff*xi
        
    return np.array(res)/norm , np.array([np.sqrt(ri)  for ri in res])/norm

gausscurve = lambda x,x0,s : 1/(np.sqrt(2*np.pi)*s)*np.exp(-(x-x0)**2/(2*s**2))

poisscurve = lambda x,mu,s : mu**(x)*np.exp(-mu)/gamma(x+1) 

exponcurve = lambda x,A,a : 1/A*np.exp(-1/A*x)

parabcurve = lambda x,x0,A,b: A*(x-x0)**2 + b

def chisquare (x,y,xerr,yerr,curve, mean, var):
    h=0.0002
    curveabl=[(curve(xi+h/2 ,mean, var)-curve(xi-h/2,mean, var))/h for xi in x]
    return sum([(y[i]-curve(x[i],mean, var))**2/((yerr[i])**2 +(curveabl[i]*xerr[i])**2 ) if y[i]!=0 else 0 for i in range(len(x))])

def findab (x,y,peak, use):
    a,b=0,0
    if (x[0] > 0):
        x*=-1
    
    for i in range(len(x)):
        if x[i] < (peak-use):
            a=i
        if x[i] > (peak+use):
            b=i
            return a,b
    print(a,b)
    return a,b

def fitparab(x,y,ex,ey,use,peak,b):
    a,b=findab(x,y,peak, use)
    
    def f(B, x):
        return B[1]*(x-B[0])**2 + B[2]
    
    model  = scipy.odr.Model(f)
    data   = scipy.odr.RealData(x[a:b],y[a:b], sx=ex[a:b], sy=ey[a:b])
    odr    = scipy.odr.ODR(data, model, beta0=[peak,use, b])
    output = odr.run()

    return output.beta, output.cov_beta
            

filenames=["Magnet0_4", "Magnet0_6" ,  "Magnet0_8",    "Magnet1",  "Magnet1_2"]#, "Magnet0"
colors={"Magnet0_4": 'blue',
        "Magnet0_6": 'red' ,
        "Magnet0_8": 'green',
        "Magnet1"  : 'pink' ,
        "Magnet1_2": 'cyan'}

norm={  "Magnet0_4": [2.55,0.41], #Verschub, use
        "Magnet0_6": [3.85,0.71],
        "Magnet0_8": [2.6,1.1],
        "Magnet1"  : [3.85,1.3],
        "Magnet1_2": [2.5,1.3]}


labelname={ "Magnet0_4": 'Spulen Strom 0.4 A', #Verschub, use
            "Magnet0_6": 'Spulen Strom 0.6 A',
            "Magnet0_8": 'Spulen Strom 0.8 A',
            "Magnet1"  : 'Spulen Strom 1.0 A',
            "Magnet1_2": 'Spulen Strom 1.2 A'
            }


peak={  "Magnet0_4": [-0.63,0.7], #Peaks
        "Magnet0_6": [-1.17,1.17],
        "Magnet0_8": [-1.9,1.8],
        "Magnet1"  : [-1.8,1.8],
        "Magnet1_2": [-2.16,2.16]}

peakerr={"Magnet0_4": [0.1*1.8/np.sqrt(12)*4, 0.1*1.8/np.sqrt(12)*4], #Peakerror
         "Magnet0_6": [0.1*1.8/np.sqrt(12), 0.1*1.8/np.sqrt(12)],
         "Magnet0_8": [0.1*1.8/np.sqrt(12)*4, 0.1*1.8/np.sqrt(12)],
         "Magnet1"  : [0.1*1.8/np.sqrt(12), 0.1*1.8/np.sqrt(12)],
         "Magnet1_2": [0.1*1.8/np.sqrt(12), 0.1*1.8/np.sqrt(12)]}

iniguess={  "Magnet0_4": [5.2, 1.3, 1.9], #Höhe, ...Breite
            "Magnet0_6": [4.6, 1.3, 1.8],
            "Magnet0_8": [4.0, 1.3, 1.5],
            "Magnet1"  : [3.7, 1.3, 1.3],
            "Magnet1_2": [3.65,1.3, 1.5]}

plt.figure(figsize=[14,8])
mu0=[]
mu0err=[]

for name in filenames:
    mag=[]
    x=[]
    for line in open(name+".dat","r"):
        line=line.split("\t")
        x.append(float(line[1]))
        mag.append(float(line[2]))
        
    x = np.array(x)-norm[name][0]
    x*=1.8
    mag = np.array(mag)+norm[name][1]
    
    #params0, cov0 = fitparab(x, mag, [err for i in x],norm[name][1],0, iniguess[name][0]-peak[name][0])
    #print(params0)
    yerr=0.03
    xerr=1.8/10/np.sqrt(12)
    
    print(xerr)
    
    use=norm[name][1]
    paramsA, covA = fitparab(x, mag,[xerr for i in x], [yerr for i in x],use,peak[name][0], iniguess[name][0])
    paramsB, covB = fitparab(x, mag,[xerr for i in x], [yerr for i in x],use,peak[name][1], iniguess[name][0])

    
    print(paramsA)
    print(paramsB)
    
    plt.figure(figsize=[10,8])
    
    pt.plot_errorbar(x,mag, [xerr for i in x], [yerr for i in x], labelname[name], 'best', 'red')
    
    plerr=peakerr[name][0]
    prerr=peakerr[name][1]
    
    top, bottom= max(paramsA[2], paramsB[2])+0.2, min(paramsA[2], paramsB[2])-0.2
    
    plt.vlines(paramsA[0], top, bottom, label='Peak {:02.02f} $\pm$ {:02.02f}'.format(paramsA[0],plerr), color='blue')
    plt.vlines(paramsA[0]-plerr, top-0.15, bottom+0.15)
    plt.vlines(paramsA[0]+plerr, top-0.15, bottom+0.15)
    plt.hlines((top+bottom)/2,paramsA[0]-plerr,paramsA[0]+plerr)
    
    plt.vlines(paramsB[0], top, bottom, label='Peak  {:02.02f} $\pm$ {:02.02f}'.format(paramsB[0],prerr),color= 'green')
    plt.vlines(paramsB[0]-prerr, top-0.15, bottom+0.15)
    plt.vlines(paramsB[0]+prerr, top-0.15, bottom+0.15)
    plt.hlines((top+bottom)/2,paramsB[0]-prerr,paramsB[0]+prerr)
    
    u0=(paramsB[0]-paramsA[0])/2
    u0err= np.sqrt((covA[0][0])**2+(covB[0][0])**2)/2
    u0err= np.sqrt((plerr)**2+(prerr)**2)/2
    
    mu0.append(u0)
    mu0err.append(u0err)
    
    pt.plot([0,0.0000001], [5,5] ,' $u_0$ '+'= {:02.02f} $\pm$ {:02.02f}'.format(u0,u0err),'best' ,'white')

    #xbins=np.linspace(0-use,0+use, 200)
    #pt.plot(xbins, parabcurve(xbins, *params0 ), 'Fit', 'best', 'yellow', '-')
    
    xbins=np.linspace(-4,4,200)
    #pt.plot(xbins, methAcurve(xbins, 0.0, iniguess[name][0], iniguess[name][1], iniguess[name][2]), 'Fit', 'best', 'red', '-')
    plt.xlabel('x in mm')
    plt.ylabel('I in mA')
    #plt.vlines(peak[name], iniguess[name][0]-0.5,iniguess[name][0]+0.5 )
    #plt.vlines(0, 2, 5)
    #plt.hlines(iniguess[name][0], -2,2 )
    plt.grid()
    #plt.yscale('log')
    #plt.xlim([min(delta1s[n]),max(delta1s[n])])
    plt.xlim([-5,5])
    #plt.ylim([1000,40000])
    plt.subplots_adjust(left=0.1, right=1, top=1, bottom=0.1)
    plt.savefig(name+'.pdf')#'Linearscale'+'.pdf')
    plt.show()
    
print(mu0)
print(mu0err)