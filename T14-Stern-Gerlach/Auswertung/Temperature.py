#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  1 10:22:26 2019

@author: schoko
"""


import sys 
if '/home/schoko/Documents/grundpraktikum-2/Python/plottingTemplate' not in sys.path:
    sys.path.append('/home/schoko/Documents/grundpraktikum-2/Python/plottingTemplate')

import plotting as pt
import matplotlib.pyplot as plt
import matplotlib
import scipy.odr
import scipy.optimize as opt
import numpy as np
from scipy.special import gamma

font = {'family' : 'DejaVu Sans',
        'weight' : 'normal',
        'size'   : 18}

matplotlib.rc('font', **font)

def histo(a,bins):
    res=[0.0 for i in bins]
    for x in a:
        for i in range(len(bins)-1):
            if bins[i] <= x < bins[i+1] :
                res[i]+=1.0
    diff=bins[1]-bins[0]
    le=bins[-1]-bins[0]
    norm=0.0
    for xi in res:
        norm+=diff*xi
    reserr=  [np.sqrt(ri)  for ri in res]
    return np.array(res)/norm , np.array(reserr)/norm

gausscurve = lambda x,A,x0,s : A*np.exp(-(x-x0)**2/(2*s**2))

poisscurve = lambda x,A,mu,s : A*mu**(x)*np.exp(-mu)/gamma(x+1) 

exponcurve = lambda x,A,a    : 1/A*np.exp(-1/A*x)

cauchycurve= lambda x,A,x0,j,b : A*j**2/((x-x0)**2+j**2)+b

parabcurve = lambda x,A,D,x0,p : A*(D-1/2.0*p-1/(2.0*p)*(x-x0)**2)

parabcurvep = lambda x,A,D,x0 : A*(D-1/2.0*0.78-1/(2.0*0.78)*(x-x0)**2)

def beamcross (x,A, D,x0,p):
    if x<(-p):
        return A*(D+(x-x0))
    if x>p:
        return A*(D-(x-x0))
    return A*(D-1/2.0*p-1/(2.0*p)*(x-x0)**2)

def chisquare (x,y,xerr,yerr,curve, mean, var):
    h=0.0002
    curveabl=[(curve(xi+h/2 ,mean, var)-curve(xi-h/2,mean, var))/h for xi in x]
    return sum([(y[i]-curve(x[i],mean, var))**2/((yerr[i])**2 +(curveabl[i]*xerr[i])**2 ) if y[i]!=0 else 0 for i in range(len(x))])

def fitparab(x,y,ex,ey,A,D,x0,p):
    
    def f(b, x):
        return b[0]*(b[1]-p/2.0-1/(2.0*p)*(x-b[2])**2) 
    
    model  = scipy.odr.Model(f)
    data   = scipy.odr.RealData(x,y, sx=ex, sy=ey)
    odr    = scipy.odr.ODR(data, model, beta0=[A,D,x0])
    output = odr.run()

    return output.beta, output.cov_beta

tem={165:[np.mean([1.82,2.0]), np.mean([4.95,4.92])], 
    160: [np.mean([2.05,1.86,1.88]), np.mean([3.6,3.79])],	
    155: [np.mean([1.92,1.83]), np.mean([3.08,2.98])],	
    150: [np.mean([1.90,1.79]), np.mean([2.68,2.78])]
    }

####Background
i=0
plt.figure(figsize=[10,9])

#pt.plot([0,0.0000001], [5,5] ,' $u_0$ '+'= {:02.04f} $\pm$ {:02.04f}'.format(u0,u0err),'best' ,'white')

plt.hlines(np.mean([tem[key][i] for key in tem.keys()]), 150,165 , label = 'Background Mean = {:02.02f}'.format(np.mean([tem[key][i] for key in tem.keys()])))
pt.plot_points(tem.keys(), [tem[key][i] for key in tem.keys()], 'Background Data', 'best', 'blue')
plt.xlabel('T in $ ^\circ$C')
plt.ylabel('I in mA')
#plt.vlines(peak[name], iniguess[name][0]-0.5,iniguess[name][0]+0.5 )
#plt.vlines(0, 2, 5)
plt.grid()
#plt.yscale('log')
#plt.xlim([min(delta1s[n]),max(delta1s[n])])
#plt.xlim([-5,5])
#plt.ylim([1000,40000])
plt.subplots_adjust(left=0.1, right=1, top=1, bottom=0.1)
plt.savefig('Background'+'.pdf')#'Linearscale'+'.pdf')
plt.show()


####Peaks
i=1
plt.figure(figsize=[10,9])

pt.plot_points(tem.keys(), [tem[key][i] for key in tem.keys()], 'Peak Data', 'best', 'red')

back=np.mean([tem[key][0] for key in tem.keys()])

pt.plot([155,155.0000001], [5,5] ,'Vergleich zum Untergrund \n {} $ ^\circ$C     {:02.02f} : 1 \n {} $ ^\circ$C     {:02.02f} : 1 \n {} $ ^\circ$C     {:02.02f} : 1 \n {} $ ^\circ$C     {:02.02f} : 1 '.format(150,tem[150][1]/back,155,tem[155][1]/back,160,tem[160][1]/back,165,tem[165][1]/back),'upper left' ,'white')
plt.xlabel('T in $ ^\circ$C')
plt.ylabel('I in mA')
#plt.vlines(peak[name], iniguess[name][0]-0.5,iniguess[name][0]+0.5 )
#plt.vlines(0, 2, 5)
#plt.hlines(iniguess[name][0], -2,2 )
plt.grid()
#plt.yscale('log')
#plt.xlim([min(delta1s[n]),max(delta1s[n])])
#plt.xlim([-5,5])
#plt.ylim([1000,40000])
plt.subplots_adjust(left=0.1, right=1, top=1, bottom=0.1)
plt.savefig('Peakintensity'+'.pdf')#'Linearscale'+'.pdf')
plt.show()
