\documentclass[12pt]{scrreprt} 
%\documentclass[12pt,draft]{scrreprt} 
\usepackage{graphicx}           % Grafiken
%\pdfcompresslevel=1
\usepackage[english]{babel}     % Sprache
\usepackage{lmodern}            % modernes Schriftbild
\usepackage[T1]{fontenc}       % Trennung von Umlauten
\usepackage{url}
%\usepackage[utf8]{inputenc}    % Umlaute 
%\usepackage{marvosym} 
%\usepackage{array}  
%\usepackage{amsmath}
%\usepackage{amssymb}
%\usepackage{pdfpages}
%\usepackage{subfig}
%\setlength{\emergencystretch}{20pt}
%\setlength{\parindent}{0em}
%\newlength{\drop}
\usepackage[version=4]{mhchem}  % Chemie Elementdarstellung \ce{}

\title{\centering \large RWTH Aachen \\ Physikalisches Institut \\ \vspace*{\baselineskip} \vspace{0.8cm} \rule{\textwidth}{1.6pt}\vspace*{-\baselineskip}\vspace*{2pt} \rule{\textwidth}{0.4pt}\\[\baselineskip] {\LARGE Protocol for the \\ \vspace{0.25cm} Physikalisches Fortgeschrittenenpraktikum}\\[0.9\baselineskip] \rule{\textwidth}{0.4pt}\vspace*{-\baselineskip}\vspace{3.2pt} \rule{\textwidth}{1.6pt}\\[\baselineskip] \scshape \vspace*{2\baselineskip} \textbf{\Huge T07 - Gaseous ionisation detectors and statistics} \\[\baselineskip] {\large group 18} \\[\baselineskip] {\Large David Tebbe (368658)\\ Yannick Kuhl (372006)} \\}

\begin{document}
\maketitle
\tableofcontents
\newpage
% INTRODUCTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Introduction and Theory}
    In this experiment we want to detect ionising radiation with gas-filled detectors. In particular we use these two detectors: 
    
    \begin{enumerate}
        \item Proportional Counter
        \item Geiger-M\"uller Counter
    \end{enumerate}

    Both operate in different regions (voltages) seen in figure (\ref{fig_region}). The main difference is, that the electric field in the Geiger-M\"uller Counter is much higher, which leads to stronger electron avalanches and thus gives a stronger signal. Also, in the Proportional Counter the charge remains proportional to the number of primary ionisation charges (one can differentiate between $\alpha$ and $\beta$ radiation), while in the Geiger-M\"uller Counter, all ionising particles cause the same voltage pulse. Characteristic curves are shown in figure (\ref{fig_charcurProp}) for the Proportional- and in figure (\ref{fig_charcurGeig}) for the Geiger-M\"uller Counter. \newline

    %Operating Regions Graph
    \begin{figure}[h]
        \centering
        \includegraphics[width=7.5cm]{operatingregions.pdf}
        \caption{Total chare versus counter voltage.\newline
        Proportional Counter operating in region III and Geiger-M\"uller Counter in region V. \cite{WEBSITE:1}}
        \label{fig_region}
    \end{figure}

    %Structure Proportional- Geiger-Counter
    %\begin{figure}
    %    \centering
    %    \includegraphics[width=7.5cm]{StructureProportionalGeiger.pdf}
    %    \caption{Schematic representation of a Proportional Counter \cite{WEBSITE:1}}
    %    \label{fig_PropGeiger}
    %\end{figure}

    %Characteristic curve (Proportional) + Geiger
    \begin{figure}[h!]
    \centering
    \begin{minipage}[t]{0.49\linewidth}
        \centering
        \includegraphics[width=\linewidth]{CharCurGeig.pdf}
        \caption{Characteristic curve of a Geiger-M\"uller Counter \cite{WEBSITE:1}}
        \label{fig_charcurGeig}
    \end{minipage}
    \hfill
    \begin{minipage}[t]{0.49\linewidth}
        \centering
        \includegraphics[width=\linewidth]{CharCurProp.pdf}
        \caption{Characteristic curve of a proportional Counter in the presence of both $\alpha$ and $\beta$ particles. \cite{WEBSITE:1}}
        \label{fig_charcurProp}
   \end{minipage}
   \end{figure}

    In the Geiger-M\"uller counter, due to its nature, after a particle has caused a discharge in the counter it takes some time to recover. During a death time no pulses can be registered, after this only lower pulses can be registered. The time from the initial pulse with trigger threshold voltage $U_t$ to the next pulse with full pulse hight is called recovery time $T_r$. The time between the initial pulse and the next registered pulse (with lower pulse height) is called dead time $T_d$ (see figure (\ref{fig_SteverSchema}) called Stever diagram).

    %Stever diagram
    \begin{figure}
        \centering
        \includegraphics[width=7.4cm]{Stever.pdf}
        \caption{Stever diagram fpr determining the dead time and recovery time \cite{WEBSITE:1}}
        \label{fig_SteverSchema}
    \end{figure}

    In the next sections we use both counters to take multiple measurements and analyse our results. Note that the counts are given as the number of counts in a chosen time interval (e.g. $500$ counts in $30s$) and the pulse rate as the the number of counts normalized to time, this means the pulse rate has the unit $1/s$.

% Geiger-M\"uller Counter %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Geiger-M\"uller Counter}
    \section{Procedure}
        We start with a measurement to characterize the Geiger-M\"uller Counter. For the characterisation we use a \ce{^{90}_{38}Sr} sample, NIM modules and voltages up to $700V$. First of all we want to determine the threshold voltage $U_T$ and the Geiger threshold $U_G$ of our Geiger-M\"uller Counter. We operate all further measurements at $U=U_G+100V$. We then check the intensity of the background and produce a Stever diagram, using the oscilloscope, from which we can determine the dead time and recovery time of the used Geiger-M\"uller Counter.
        
        \subsection{Measurement of the characteristic curve}
            We put the \ce{^{90}_{38}Sr} sample into the Geiger-M\"uller Counter and measure the pulse rate as a function of the high voltage applied, to determine the threshold voltage $U_T$ and the Geiger threshold $U_G$. We start from $200V$ where we measure 0 counts (same for lower voltages) and go in $10V$ steps up to the limit of $700V$. At every voltage we measure for a time duration $\Delta t$ of 10 seconds. Our raw data is shown in table (\ref{fig_GeigerCharCur_raw}) in Appendix A. Since we are measuring the counts, which are random events, we assume a Poission distribution and calculate the Errors to be $\sigma_{counts} = \sqrt{counts}$.
            The resulting plot (Voltage vs. pulse rate) is depicted in figure (\ref{fig_GeigerCharCur}), the right one has a logarithmic scaled y-Axes. Here we plot the pulse rate for which we get the error thou Gaussian propagation of uncertainty as follows:
			\begin{equation}
			\sigma_{puls rate} = \frac{\sigma_{counts}}{\Delta t} = \frac{\sqrt{counts}}{\Delta t}
			\end{equation}			            
             We can determine from the Plots:
            
            % Results for the threshold voltages
            \begin{table}[h]
                \centering
                \begin{tabular}{|c|c|c|}\hline
                    $U_T$ in $V$    &   $U_G$ in $V$    &   $U$ in $V$   \\\hline
                    $250$           &   $500$           &   $600$        \\\hline
                \end{tabular}
                \caption{Results for the threshold voltage $U_T$, the Geiger threshold $U_G$ and the voltage $U=U_G+100V$.}
            \end{table}

            % Threshold voltages plot (Geiger)
            \begin{figure}[h]
            \centering
            \begin{minipage}[t]{0.49\linewidth}
                \centering
                \includegraphics[width=\linewidth]{Auswertung/GM1.pdf}
            \end{minipage}
            \hfill
            \begin{minipage}[t]{0.49\linewidth}
                \centering
                \includegraphics[width=\linewidth]{Auswertung/GM1log.pdf}
            \end{minipage}
            \caption{Characteristic Curve of the Geiger-M\"uller Counter with threshold voltage $U_T$ and Geiger threshold $U_G$.}
            \label{fig_GeigerCharCur}
            \end{figure}


        \subsection{Intensity of the background}
            We now check the intensity of the background, which means that we measure the pulse rate without the \ce{^{90}_{38}Sr} sample. With the measured values depicted in figure (\ref{fig_Geigerbackground_raw}) we used the weighted mean, between the multiple measurements we took, to receive our mean background:

            % Background result
            \begin{table}[h]
                \centering
                \begin{tabular}{|c|}\hline
                    background pulse rate in Hz   \\\hline
                    $0.29 \pm 0.04$         \\\hline
                \end{tabular}
                \caption{Background pulse rate at voltage $U=600V$.}
            \end{table}

            % Background raw data
            \begin{figure}
                \centering
                \includegraphics[width=10cm]{Geigerbackground-cropped.pdf}
                \caption{Intensity of the background - raw data + weighted mean}
                \label{fig_Geigerbackground_raw}
            \end{figure}

        \subsection{Stever diagram}
            In the following we produce a Stever diagram with the oscilloscope to estimate the dead time and recovery time of the Geiger-M\"uller Counter. By setting a trigger at the beginning for a high Peak we are able to make out the death time and the recovery time of our Geiger M\"uller Counter. We measure multiple Stever diagrams and mark the dead time and the recovery time.
            In figure (\ref{fig_Stever}) one of those diagrams is shown with cursors that represent the dead time $T_d$ and the recovery time $T_r$.

            %Stever diagram example plot

            \begin{figure}[h!]
            \centering
            \begin{minipage}[t]{0.49\linewidth}
                \centering
                \includegraphics[width=\linewidth]{Auswertung/F0001TEK.pdf}
            \end{minipage}
            \hfill
            \begin{minipage}[t]{0.49\linewidth}
                \centering
                \includegraphics[width=\linewidth]{Auswertung/F0002TEK.pdf}
            \end{minipage}
            \caption{One example of our Stever diagrams. $T_d$ on the left and $T_r$ on the right side.}
            \label{fig_Stever}
            \end{figure}

            You can see that it is not always clear where exactly to set the dead and recovery time, this results in a relatively high standard deviation (see raw data in Figure (\ref{fig_Stever_raw})). 

            % Stever diagram - raw data
            \begin{figure}[h!]
                \centering
                \includegraphics[width=8cm]{GeigerStever-cropped.pdf}
                \caption{Stever diagram - raw data}
                \label{fig_Stever_raw}
            \end{figure}

            Furthermore we get a systematic error thou the bin width ($4\mu s$ for each cursor) which we estimate to $\sigma_{sys}=8\mu s/\sqrt{12}$. Compared to the standard deviation, $\sigma_{sys}$ is almost negligible. All in all we get:

            % Result Stever
            \begin{table}[h]
                \centering
                \begin{tabular}{|c|c|}\hline
                    $T_d$ in $\mu s$    &   $T_r$ in $\mu s$     \\\hline
                    $148 \pm 27$    &   $616 \pm 48$   \\\hline
                \end{tabular}
                \caption{Stever diagram - Results for dead and recovery time.}
            \end{table}

    \section{Analysis}
        Our goal in this section is to determine the resolution time of the Geiger-M\"uller Counter. We therefore use electronically generated dead times ($1\mu s$ and $2ms$). As a signal source we use an $1MHz$-generator.

        \subsection{Dead time stage}
        \subsubsection{$1MHz$-Generator}
            First we want to determine the dead times of the electronic device (given as $1\mu s$ and $2ms$) with a $1-MHz$-generator. We count the signals for both dead times and without dead time for $0.2s$ and get figure (\ref{fig_GeigGenerator}). It is obvious that a dead time of $1\mu s$ dose not affect the counts at all, since it is as small as the time between the individual pulses which is $1\mu s$. 

            % Counts Generator
            \begin{figure}[h]
                \centering
                \includegraphics[width=10cm]{GeigerGenerator-cropped.pdf}
                \caption{Generator - Pulse rates for different dead times - raw data}
                \label{fig_GeigGenerator}
            \end{figure}
            
            We determine the pulse rates $n_1=520 Hz \pm 1.5 Hz$ for dead time $\tau{'}_1=2ms$ and $n_2=1000005 Hz \pm 1.5 Hz$ for dead time $\tau{'}_2=1\mu s$. (Note that we use $\tau{'}_x$ because this are the values given by the Manufacturer and in the following we calculate the "real" dead time $\tau_1$ with the generator.) We see that $n_2$ equals the pulse rate without dead time. This is the case because $\tau_2$ is about the same as the period time of the generator.  
            With the formula 

            % Formel N und tau allgemein
            \begin{equation}
                n_2=\frac{n_1}{1-n_1\tau_1} 
                \label{eq_N1}
            \end{equation}

            ($n_2 = 1000005 \sim $ ($1MHz$)) we get an equation for the real dead time $\tau_1$:

            %Formel changed to calculate tau_1
            \begin{equation*}
                \tau_1=\frac{1}{n_1}-\frac{1}{N}
            \end{equation*}
            
            and the result

            % Result tau_1
            \begin{table}[h]
                \centering
                \begin{tabular}{|c|c|}\hline
                    $\tau{'}_1$ in $ms$ &   $\tau_1$  in $ms$   \\\hline
                    $2$                 &   $1,921 \pm 0,005$   \\\hline
                \end{tabular}
                \caption{Generator - Result for $\tau_1$ compared with the given dead time.}
            \end{table}

            Here we have assumed that the error on the counts measurement is $1/\sqrt{12}$ because it not a coincidence experiment and the bin-width is $1$ count, as the events from the $1MHz$ generator are evenly spaced.

        \subsubsection{Resolution time of the Geiger-M\"uller Counter}

            Now we use the formula for the dead time to extract the resolution time of the Geiger-M\"uller Counter $\tau$ :

            %Formel changed to calculate tau
            \begin{equation*}
                N=\frac{n_2}{1-n_2\tau} \qquad (\tau_2 \ll \tau)
                \label{eq_N2}
            \end{equation*}
            \begin{equation*}
                N=\frac{n_1}{1-n_1\tau_1} \qquad (\tau_1 \gg \tau)
                \label{eq_N2}
            \end{equation*}

            %We have used $\tau$ in this equation because for the Geiger-M\"uller Counter the measured counter rate $n_2$ is determined by the resolution time of the counter.

            After that we measure the pulse rates in the same way as for the generator and get Figure (\ref{fig_Geigrestime}) we calculate the weighted mean and get the results:

            % Results n1, n2 pulse rates Geiger
            \begin{table}[h]
                \centering
                \begin{tabular}{|c|c|}\hline
                    $n_1$           &   $n_2$           \\\hline
                    $58.9 \pm 0.4$  &   $63.9 \pm 0.4$  \\\hline
                \end{tabular}
                \caption{Geiger-M\"uller - Results for the pulse rates of the different dead times.}
            \end{table}

            %  Pulse rates Geiger raw data n1, n2
            \begin{figure}
                \centering
                \includegraphics[width=14cm]{Geigerdeadtime-cropped.pdf}
                \caption{Geiger-M\"uller - Puls rates for different dead times - raw data}
                \label{fig_Geigrestime}
            \end{figure}
            
            We set equations (\ref{eq_N1}) and (\ref{eq_N2}) to be equal, since $N=N$ and get a formula for the resolution time $\tau$: 

            %Formel tau nach Gleichsetzten
            \begin{equation*}
                \tau=\frac{1}{n_2}-\frac{1}{n_1}+\tau_1
            \end{equation*}

            If we insert the found values into this equation, we get the final result for $\tau$. We compare this to the estimated dead $T_d$ and recovery time $T_r$. 

            % Result tau
            \begin{table}[h]
                \centering
                \begin{tabular}{|c|c|c|}\hline
                    $T_d$ in $\mu s$ & $\tau$ in $\mu s$   &   $T_r$ in $\mu s$    \\\hline
                    $148 \pm 27$     & $593 \pm 151$       &   $616 \pm 63$        \\\hline
                \end{tabular}
                \caption{Geiger-M\"uller - Result for the real dead time compared with the recovery time.}
            \end{table}

            Compared with the estimated dead $T_d$ and recovery time $T_r$ we got from the Stever diagrams, we are able to see, that the resolution time sits between the two since the NIM module we used is able to trigger at lower peaks and thus dose not need the full recovery time. The recovery time will be revered now as the effective dead time of our Geiger-M\"uller Counter.

        \subsection{Dead time correction}
            We plot our measured pulse rate ($n$) and the corrected pulse rate ($N$) that we calculate with the formula:
            
            % Equation corrected dead time
            \begin{equation*}
                N=\frac{n}{1-n\tau}
            \end{equation*}

            We obtain figure (\ref{fig_GeigCorr1}) which shows that the real pulse rate is higher than the pulse rate without correction. This makes scene, because the effective dead time results in less measured events. 
            
            %Plot dead time  correction theo
            \begin{figure}[h]
                \centering
                \includegraphics[width=14cm]{Auswertung/GM1_tot_theo.pdf}
                \caption{Comparison between the corrected and not corrected count rates.}
                \label{fig_GeigCorr1}
            \end{figure}

            Now we can plot the Geiger-M\"uller Counter characteristic curve with the correction, shown in figure (\ref{fig_GeigerCharCurCorr}).
            
            %Plot Geiger characteristic curve corrected
            \begin{figure}
                \centering
                \includegraphics[width=14cm]{Auswertung/GM1log_tot.pdf}
                \caption{Comparison between the corrected and not corrected characteristic curve of the Geiger-M\"uller Counter.}
                \label{fig_GeigerCharCurCorr}
            \end{figure}

            We see that the count rates are about the same for voltages up to $320V$. Even above count rates stay fairly similar but as the measured count rate becomes bigger the difference increases as well. This is logical, since for higher voltages the count rates rise, which means that the dead time plays a greater role.

      
\chapter{Statistics}
    \section{Procedure}
        In this section we apply statistical distributions like the Gaussian and Poisson distribution to different measurements with the Geiger-M\"uller Counter. To see which model fits the measured data best. Also we look at the difference between a $\xi^2 $ fit and the analytical solution.

        \subsection{Gaussian Distribution}
            In this experiment we put the \ce{^{241}_{95}Am} sample into the counter.
            We choose a measurement time of $\Delta t=0.7s$ because during this time we counted more than 25 events, which is needed for a good Gaussian Distribution. In total we did 153 measurements. You can see our data presented as histograms in figure (\ref{fig_Gauss}). From out data we get the mean and the standard deviation:

            \begin{table}[h]
                \centering
                \begin{tabular}{|c|c|}\hline
                    mean counts &   standard deviation  \\\hline
                    $45.5$      &   $6.4$               \\\hline                    
                \end{tabular}
                \caption{Gauss - mean and standard deviation from our data ($0.7s$ measurement time).}
            \end{table}

            In figure (\ref{fig_Gauss}) you can see the Gauss-Fits \footnote{The red curve results from our calculated mean and standard deviation whereas the green curve is a Gauss distribution, as seen in equation (\ref{eq_Gauss}), fitted with a $\xi^2 $ fit. We neglected the "Counts-Bins" with $Amount=0$.} as well. The fit is theoretically described by the equation:

            \begin{equation}
                P(k)=\frac{1}{\sqrt{2\pi}\sigma}exp\left(-\frac{(k-\mu)^2}{2\sigma^2}\right)
                \label{eq_Gauss}
            \end{equation}

            where $\sigma$ is the standard deviation, $\mu$ the mean and $k$ the counts.

            % Gaussian histogram
            \begin{figure}[h]
                \centering
                \includegraphics[width=14cm]{Auswertung/Gauss1.pdf}
                \caption{Gauss Distribution - 153 measurements, counts measured for $t=0.7s$, bin width = 1 count.}
                \label{fig_Gauss}
            \end{figure}

            The error on the amount is calculated as before, since here we still have a random counting process which is describes thou a Possion. Thus we get $\sigma_{amount} = \sqrt{amount}$.

            We see that the red and the green curves are quite similar with a $\chi^2$ of 28 to 29. Using figure (\ref{fig_chi2}) and $n=29-2$, we obtain $F_n(\chi^2)=0.6\pm0.1$ which means that experimental data and theory are in good correspondence. %XY Freiheitsgrade richtig?

            % Chi2 graphic
            \begin{figure}
                \centering
                \includegraphics[width=10cm]{Chi2Grafik}
                \caption{The function $F_n(\chi^2)$ for several values of $n$ (degrees of freedom)}
                \label{fig_chi2}
            \end{figure}


        \subsection{Poisson Distribution}
            In this section we measure without the \ce{^{241}_{95}Am} sample.
            We chose a measurement time of $t=3s$ because during this time we counted less than 2 events on average, which is needed for a good Poisson Distribution. In total we did 100 measurements. You can see our data presented as histograms in figure(\ref{fig_Poisson}). From out data we get the mean:

            %Poisson mean
            \begin{table}[h]
                \centering
                \begin{tabular}{|c|}\hline
                    mean counts \\\hline
                    0.92        \\\hline
                \end{tabular}
                \caption{Poisson - mean from our data ($3s$ measurement time)}
            \end{table}

            In figure (\ref{fig_Poisson}) you can see the Poisson-Fit \footnote{The red curve results from our calculated mean whereas the green fit is a Poisson distribution as seen in equation (\ref{eq_Poisson}), fitted with a $\xi^2 $ fit.} as well. The fit is theoretically described by the equation:

            \begin{equation*}
                P(k)=\frac{\mu^ke^{-\mu}}{k!}
                \label{eq_Poisson}
            \end{equation*}

            with $\mu$ the mean and $k$ the counts.

            %Poisson histogram
            \begin{figure}
                \centering
                \includegraphics[width=14cm]{Auswertung/Poission1.pdf}
                \caption{Poisson Distribution - 100 measurements, counts measured for $t=3s$, bin width = 1 count.}
                \label{fig_Poisson}
            \end{figure}

            The error on the amount is calculated as before, since here we still have a random counting process which is describes thou a Possion. Thus we get $\sigma_{amount} = \sqrt{amount}$.

            Both curves are very similar so that we expect $F_n(\chi^2)$ to be close to 1. Again with figure (\ref{fig_chi2}) and $n=5-1$ we get $F_n(\chi^2)=0.9\pm 0.05$ as expected.


        \subsection{Exponential Distribution}
            We take the \ce{^{90}_{38}Sr} and the Geiger-M\"uller Counter and measure with the oscilloscope the time between two events. An example plot is shown in figure (\ref{fig_times}).

            % times oscilloscope
            \begin{figure}
                \centering
                \includegraphics[width=10cm]{Auswertung/F0017TEK.pdf}
                \caption{Oscilloscope - Voltage as a function of time to determine the time between two events.}
                \label{fig_times}
            \end{figure}

            We determined 137 times out of 3 measurements and receive the following results:

            % mean and standard dev times between two events
            \begin{table}[h]
                \centering
                \begin{tabular}{|c|}\hline
                    time $T$ between two events [s] (mean) \\\hline
                    $36.991$                   \\\hline
                \end{tabular}
            \end{table}
            
            In figure (\ref{fig_Exp}) you can see our data presented as a histogram and with our chosen Exponential-Fits \footnote{The red curve results from our calculated mean $T$ whereas the green fit is a Exponential Distribution as seen in equation (\ref{eq_Exp}). We neglected the "Counts-Bins" with $Amount=0$.} as well. The fit is theoretically described by the equation: %XY Stimmt die footnote so?

            \begin{equation}
                \rho(\Delta t)=A exp(-A\Delta t)
                \label{eq_Exp}
            \end{equation}

            where $\rho$ is the probability density and $A=\mu/T$. In our case it is $\mu=1$ and $T$ is our calculated mean between two events.

            % Gaussian histogram
            \begin{figure}[h]
                \centering
                \includegraphics[width=14cm]{Auswertung/Zeit2.pdf}
                \caption{Exponential Distribution - 99 measured time intervals between two events.}
                \label{fig_Exp}
            \end{figure}

            The error in the histogram on the amount is calculated as before, since here we still have a random counting process which is describes thou a Possion. Thus we get $\sigma_{amount} = \sqrt{amount}$.
            Both curves are similar which confirms our assumption of an Exponential-Distribution. With figure \ref{fig_chi2} and $n=14-1$ we obtain $F_n(\chi^2)=0.7\pm 0.1$. 


\chapter{Proportional Counter}
    \section{Procedure}
        For this task we use a methane-gas-flow proportional counter (argon-methane mixture) schematically shown in figure (\ref{fig_PropSchema}) with an \ce{^{241}_{95}Am} and a \ce{^{14}_{6}C} sample. We use voltages up to $2kV$. From $300V - 1000V$ we can use the $x1$-multiplication mode and from $600V - 2000V$ we use the $x2$-multiplication mode. We will see that the transition between theses modes has an offset of about $300V$.
        % Proportional Counter schematically
        \begin{figure}[h]
            \centering
            \includegraphics[width=8cm]{gasflowProp.pdf}
            \caption{Schematic representation of a methane-gas-flow proportional counter.}
            \label{fig_PropSchema}
        \end{figure}

    \section{Analysis}
        We measured for both samples the pulse rate ($\Delta t = 30s$ measurement time) and the pulse hight as a function of the applied voltage. In addition we did a background measurement without the sample, in which we only measured the pulse rate. We chose voltage steps between $10V$ and $50V$ in dependence of the pulse rate behaviour (strong increase $\rightarrow$ smaller steps). Our raw data is shown in figure (\ref{fig_PropAm_raw}) for \ce{^{241}_{95}Am}, in figure (\ref{fig_PropC_raw}) for \ce{^{14}_{6}C} and in figure (\ref{fig_Propback_raw}), which are all listes in Appendix B.

    \subsection{Background}
        Without any sample inside the chamber we get the background pulse rate or counts ($counts=30s\cdot pulse rate$). We plot the background against the applied Voltage and get the plot shown in figure (\ref{fig_prBack}) the errors are calculates as in the previous experiments, since a random counting process describes our counts we get a Poission distribution:
        \begin{equation}
			\sigma_{puls rate} = \frac{\sigma_{counts}}{\Delta t} = \frac{\sqrt{counts}}{\Delta t}
		\end{equation}
        
        % pulse rate Background
        \begin{figure}
            \centering
            \includegraphics[width=13.5cm]{Auswertung/PropBackground.pdf}
            \caption{Background - Pulse rate.}
            \label{fig_prBack}
        \end{figure}

        A linear correlation (rate $=$ a $\cdot$ voltage $+$ b) between pulse rate and voltage can be clearly seen. In the following we subtract this background from our measurements starting at the Voltage $U_E = 884.7 V$ to to account for the Background effects.
        
        \begin{table}[h]
             \centering
             \begin{tabular}{|c|c|}\hline
                 a in Hz$/V \cdot 10^{-3}$ & b in [Hz] \\\hline
                  3.36144 $\pm$ 0.00005 & -2.97 $\pm$ 0.08) \\\hline
             \end{tabular}
        \end{table}

    \subsection{Pulse hights of Americium and Carbon}
        First of all we measure the pulse hight (logarithmic) of Americium as a function of the voltage applied and notice that there is a shift between two linear regions (figure (\ref{fig_phAmnoshift})). But when we add $+300V$ to the values measured in the $x2$-mode, we get figure (\ref{fig_phAm}) with good matching linear regions. It is also to notice, since the two linear Fits are almost parallel, we are only talking about a constant shift and no a scaling factor. We conclude that the $x2$-mode displays voltages that are in reality about $300V$ higher.

        The pulse hight for Carbon as a function of the voltage is shown in figure (\ref{fig_phC}). Here we can't do a shift correction because the pulse rate rise starts at around $1000V$ (seen in figure (\ref{fig_phC}) and (\ref{fig_prAmC}), which means that our relevant measurement ($pulse rate\neq 0$) happens in the $x2$-mode, but we still add the shift correction for the $x2$-mode we calculated with the Americium sample ($+300V$), since we want to compare the two samples later on. 
        
        % Am and Am - Pulse hight
        \begin{figure}[h]
            \centering
            \begin{minipage}[t]{0.49\linewidth}
                \centering
                \includegraphics[width=\linewidth]{Auswertung/PropAmericumPeakheightwhoutcorrect.pdf}
                \caption{Americium - log(Pulse hight) as a function of the voltage applied}
                \label{fig_phAmnoshift}
            \end{minipage}
            \hfill
            \begin{minipage}[t]{0.49\linewidth}
                \centering
                \includegraphics[width=\linewidth]{Auswertung/PropAmericumPeakheight.pdf} 
                \caption{Americium - log(Pulse hight) as a function of the voltage applied - $x2$-mode values shifted by $+300V$.} 
                \label{fig_phAm}
            \end{minipage}
        \end{figure}
       
        % Carbon - Pulse hight no correction
         \begin{figure}[h]
             \centering
             \includegraphics[width=10cm]{Auswertung/PropKohlenstoffPeakheight.pdf}
             \caption{Carbon - Pulse hight (log) as a function of the voltage applied.}
             \label{fig_phC}
         \end{figure}


        We expected an exponential relation between pulse hight and voltage, respectively a linear relation between log(pulse hight) and the voltage which is clearly seen in both plots for Americium and Carbon. At about $1250V$ for Americium and at about $1650V$ for Carbon the pulse rates stay constant at $\approx 68V$ which we interpret as a cut of in the Measurement Setup.

    \subsection{Pulse rates Americium and Carbon}
        For the pulse rate of \ce{^{241}_{95}Am} we get figure (\ref{fig_prAmnocorr}). Here we see overlapping regions between $600V$ and $1000V$. This is a result of the already discussed transition from the $x1$ to the $x2$ mode. We do the same shift as for the pulse hight which results in figure (\ref{fig_prAmC}) (red dots). The pulse rate of \ce{^{14}_{6}C} is shown in figure (\ref{fig_prAmC}) as well (blue dots).
% Americium - Pulse rate no correction
        \begin{figure}[h]
        \centering
            \includegraphics[width=13cm]{Auswertung/PropAmericumRate.pdf}
            \caption{Americium - Counts without shift correction but with background correction.}
            \label{fig_prAmnocorr}
        \end{figure}
        \begin{figure}[h]
        \centering
            \includegraphics[width=13cm]{Auswertung/PropKolenstoffRate.pdf}
            \caption{Carbon and Americium - Pulse rates with shift ($+300V$ for the $x2$-mode values) and background correction.}
            \label{fig_prAmC}
        \end{figure}

        In both curves we identify characteristic measurement types like the threshold voltages ($U_{T_{Am}}\approx 300V$, $U_{T_{C}}\approx 1200V$) and the plateaus. Americium is an $\alpha$ and $\beta$ radiator so that we see an $\alpha$-plateau from $700-1200V$ and an ($\alpha+\beta$)-plateau starting from $1500V$. On the other hand Carbon is just a $\beta$ radiator so that we just see the $\beta$-plateau starting from $1500V$.


\chapter{Conclusion}
In the experiments using the Geiger M\"uller Counter we measured a good characteristic curve between the voltage and the pulse rate and we were able to determine $U_T = 250 V$ as well as $U_G = 500 V$ quite well.
It was also possible to determine the intensity of the background at a voltage of $U =600V$ to be $0.29Hz \pm 0.04Hz$.
In the Stever diagramm generated by the Oscilloscope we were able to determine the dead time and the recovery time of our Geiger  M\"uller Counter. 
With a previously measured Dead time stage ($1,921 ms \pm 0,005 ms$) we were able to also measure the resolution time of our Geiger  M\"uller Counter:
% Result tau
            \begin{table}[h]
                \centering
                \begin{tabular}{|c|c|c|}\hline
                    $T_d$ in $\mu s$ & $\tau$ in $\mu s$   &   $T_r$ in $\mu s$    \\\hline
                    $148 \pm 27$     & $593 \pm 151$       &   $616 \pm 63$        \\\hline
                \end{tabular}
            \end{table}
   With this we were able to account for the effective dead time of our Geiger  M\"uller Counter and could correct our data accordingly.

In the Statistics Section we looked at different Distribution Models and fitted them to our measured data.
\begin{table}[h]
\centering
   \begin{tabular}{|c|c|c|c|}\hline
   Model                     & Gaussian                     & Poission       &   Exponential  \\\hline
   Calculated                & $<N> = 45.5$   $\sigma= 6.4$ & $\mu = 0.92$   &  $ T= 37.0$  \\\hline
   $\xi^2$ --> $F_n(\xi^2)$  & $28.91$  -->   $\sim 0.6$    & $1.152$ --> $\sim 0.9$   &  $9.76$ --> $\sim 0.7$ \\\hline
   Fitted                    & $<N> = 45.2$   $\sigma= 6.8$ & $\mu = 0.91$&  $ T= 39.2$  \\\hline
   $\xi^2$ --> $F_n(\xi^2)$  & $27.90$  -->   $\sim 0.9$    & $1.143$ --> $\sim 0.95$  &  $9.56$ --> $\sim 0.9$ \\\hline
  \end{tabular}
\end{table}
We see that all of them yield a good Probability in the $\xi^2$ calculations, wich means they are good models.

With the Proportional Counter we first collected data on the background and estimated it to be proportional with the voltage starting from a voltage $U_E = 884.7V$ with the proportionality Factor $a= (3.36144 \pm 0.00005 )$ Hz$/V \cdot 10^{-3}$. From the Pulse heights we could derive, the Voltage x2-mode has an offset of $+300V$ to the x1-mode. Also we saw that there is a Measurement Setup specific Cut off at $\approx 68V$.
The Plots where we potted the Pulse Rates vs. the applied Voltage show clearly both of the characteristic plateaus of the Proportional Counter, the $\alpha$-plateau from 700-1200V and an ($\alpha + \beta$)-plateau starting at 1500V. It is also to notice, that Carbon, which is just a $\beta$ radiator, only has an $\beta$-plateau.


\newpage
\chapter{Appendix A}

% Table Geiger Char Curve raw data
            \begin{figure}[h]
                \centering
                \includegraphics[height=15.5cm]{GeigerCharCur-cropped.pdf}
                \caption{Measurement of the characteristic curve (Geiger-M\"uller) - raw data - 10 second measurement time}
                \label{fig_GeigerCharCur_raw}
            \end{figure}



\newpage
\chapter{Appendix B}
        %raw data (Am+C+back)
        
        \begin{figure}[!h]
            \centering
            \includegraphics[height=16cm]{ProportionalAm-cropped.pdf}
            \caption{Raw data for \ce{^{241}_{95}Am}.}
            \label{fig_PropAm_raw}
        \end{figure}
        
        \begin{figure}
        \centering
        \begin{minipage}[t]{0.6\linewidth}
            \centering
            \includegraphics[width=9cm]{ProportionalC-cropped.pdf}
            \caption{Raw data for \ce{^{14}_{6}C}}
            \label{fig_PropC_raw}
        \end{minipage}
        \hfill
        \begin{minipage}[t]{0.39\linewidth}
            \centering
            \includegraphics[width=4.2cm]{Proportionalback-cropped.pdf}
            \caption{Raw data for the background}
            \label{fig_Propback_raw}
        \end{minipage}
        \end{figure}



\newpage
\addcontentsline{toc}{chapter}{Literaturverzeichnis}
\bibliography{litVerzeichnis}
\bibliographystyle{ieeetr}


\end{document}