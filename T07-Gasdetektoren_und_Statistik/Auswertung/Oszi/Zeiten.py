#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 27 09:30:13 2019

@author: schoko
"""


import sys 
if '/home/schoko/Documents/grundpraktikum-2/Python/plottingTemplate' not in sys.path:
    sys.path.append('/home/schoko/Documents/grundpraktikum-2/Python/plottingTemplate')

import plotting as pt
import matplotlib.pyplot as plt
import matplotlib
import scipy as sc
import scipy.optimize as opt
import numpy as np

font = {'family' : 'DejaVu Sans',
        'weight' : 'normal',
        'size'   : 18}

matplotlib.rc('font', **font)

def histo(a,bins):
    res=[0 for i in bins]
    for x in a:
        for i in range(len(bins)-1):
            if bins[i] <= x < bins[i+1] :
                res[i]+=1
    return res
    
def findpeak(x,y,a):
    peak=[]
    begin=0
    end=0
    for i in range(len(x)-1):
        if y[i]<a and y[i+1]>a :
            begin=i+1
        if y[i]>a and y[i+1]<a :
            end=i+1
        if begin!=0 and end!=0 and abs(begin-end)<5:
            peak.append([begin,end])
            begin=0
            end=0
    #print(peak)
    peaktime=[]
    for l in peak:
        index=l[0]
        high=y[l[0]]
        for i in range(l[0],l[1]):
            if y[i]>high:
                index=i
        peaktime.append(x[index])
    return peaktime    

filename=["F0015CH1.CSV","F0018CH1.CSV","F0019CH1.CSV"]
zeit=[]
for name in filename:    
    time=[]
    volt=[]
    for line in open(name,"r"):
        l=line.split(',')
        time.append(float(l[3]))
        volt.append(float(l[4]))

    peaks=findpeak(time,-np.array(volt),0.05)
    vor=0
    for z in peaks:
        zeit.append((z-vor)*1000)
        vor=z
    print(len(zeit))
    xbins=np.linspace(25,65,10)
    #abberr=np.array([5/np.sqrt(12) for i in range(len(data.values()))])
    #print()
    #m,em,b,eb,chiq,corr = ana.lineare_regression(mi,si , abberr)
    #print(m,em)
    #print(b,eb)
    #xbins=np.linspace(0,180)
    #anz=histo(gauss, xbins)
    plt.figure(figsize=(13,8))
    plt.xlabel('Counts/0,7s')
    plt.ylabel('Anzahl')
    pt.plot(time,volt,"points", 'best')
    plt.vlines(peaks,0,-0.5 ,label= '$\omega_0$', linewidth=1)
    plt.grid()
    ##plt.yscale('log')
    #plt.xlim([-0.001,0.002])
    #plt.ylim([0.01,-0.5])
    plt.savefig('Zeiten.pdf')
    plt.show()
print(zeit)