#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun Mar  3 01:11:52 2019

@author: schoko
"""

import sys 
if '/home/schoko/Documents/grundpraktikum-2/Python/plottingTemplate' not in sys.path:
    sys.path.append('/home/schoko/Documents/grundpraktikum-2/Python/plottingTemplate')

import plotting as pt
import matplotlib.pyplot as plt
import matplotlib
import scipy as sc
import scipy.optimize as opt
import numpy as np
from scipy.special import gamma

font = {'family' : 'DejaVu Sans',
        'weight' : 'normal',
        'size'   : 18}

matplotlib.rc('font', **font)

def histo(a,bins):
    res=[0.0 for i in bins]
    for x in a:
        for i in range(len(bins)-1):
            if bins[i] <= x < bins[i+1] :
                res[i]+=1.0
    diff=bins[1]-bins[0]
    norm=0.0
    for xi in res:
        norm+=diff*xi
    reserr=  [np.sqrt(ri)  for ri in res]
    return np.array(res)/norm , np.array(reserr)/norm

gausscurve = lambda x,x0,s : 1/(np.sqrt(2*np.pi)*s)*np.exp(-(x-x0)**2/(2*s**2))

poisscurve = lambda x,mu,s : mu**(x)*np.exp(-mu)/gamma(x+1) 

exponcurve = lambda x,A,a : 1/A*np.exp(-1/A*x)

linfit = lambda x,b,a : b+a*x

def chisquare (x,y,xerr,yerr,curve, mean, var):
    h=0.0002
    curveabl=[(curve(xi+h/2 ,mean, var)-curve(xi-h/2,mean, var))/h for xi in x]
    return sum([(y[i]-curve(x[i],mean, var))**2/((yerr[i])**2 +(curveabl[i]*xerr[i])**2 ) if y[i]!=0 else 0 for i in range(len(x))])

filenames=["PropAmer.dat","PropC.dat","PropUnter.dat"]
volt=[]
counts=[]
for line in open(filenames[2],"r"):
    l=line.split(',')
    volt.append(float(l[0]))
    counts.append(float(l[1]))

delete=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 21, 23, 31]
volt = np.delete(np.array(volt), delete)
counts = np.delete(np.array(counts), delete)

delete=[ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
volt = np.delete(np.array(volt), delete)
counts = np.delete(np.array(counts), delete)

rate=counts/30

##Proportionalzähler Background
xpoints=np.linspace(850,1700,500)
plt.figure(figsize=(16,8))
plt.xlabel('Voltage')
plt.ylabel('Rate [1/s]')
guess=[-2.97, 0.00336]
params, corr = opt.curve_fit(linfit, volt, rate, sigma=[np.sqrt(i)/30 for i in counts], p0=guess )
print(round(params[0],2),round(corr[0][0],2))
print(round(params[1]*1000,5),round(corr[1][1]*1000,5))
print([i if counts[i]==1 else '' for i in range(len(counts))])
chiq=chisquare( volt, rate,[0 for i in counts], [np.sqrt(i)/30 for i in counts], linfit ,params[0],params[1])
pt.plot_errorbar(volt,rate,[0 for i in counts], [np.sqrt(i)/30 for i in counts],"points", 'best')
plt.vlines([-params[0]/params[1]],0,2 ,label= '$U_E$ = '+str(round(-params[0]/params[1],1)), linewidth=1)
pt.plot(xpoints,linfit(xpoints,params[0],params[1]),"Linfit $\chi^2 /ndf$ = "+str(round(chiq/(len(volt)-2),5)),'best','blue','-')

plt.grid()
#plt.yscale('log')
plt.subplots_adjust(left=0.1, right=1, top=1, bottom=0.1)
plt.savefig('PropBackground.pdf')
plt.show()

aB=params[1]
bB=params[0]

volt=[]
counts=[]
hight=[]
for line in open(filenames[0],"r"):
    l=line.split(',')
    volt.append(float(l[0]))  #Übergang 21 --> 22
    counts.append(float(l[1]))
    hight.append(float(l[2]))

#Potentieller Fit um Steigungen zu verifiezieren : lower: 9 - 21, higher : 22-27
#Voltage Multiklicator Offset Korrektur
#volt = np.array([volt[i]+300 if i>21 else volt[i] for i in range(len(volt))])


# Background Korrectur:
rate=np.array(counts)/30
rate=np.array([rate[i]+linfit(volt[i], bB, aB) if volt[i] > bB/aB else rate[i] for i in range(len(rate))])


##Proportionalzähler Armericum
xpoints=np.linspace(850,1700,500)
plt.figure(figsize=(16,8))
plt.xlabel('Voltage')
plt.ylabel('Rate [1/s]')
#guess=[-89, 0.1]
#params, corr = opt.curve_fit(linfit, volt, counts, sigma=[np.sqrt(i) for i in counts], p0=guess )
#print(params[0],params[1])
print([i if rate[i]==1 else '' for i in range(len(counts))])
#chiq=chisquare( volt, counts,[0 for i in counts], [np.sqrt(i) for i in counts], linfit ,params[0],params[1])
pt.plot_errorbar(volt,rate,[0 for i in counts], [np.sqrt(i)/30 for i in counts],"points", 'best')
#pt.plot(xpoints,linfit(xpoints,params[0],params[1]),"Linfit $\chi^2 /ndf$ = "+str(round(chiq/(len(volt)-2),5)),'best','blue','-')
#plt.vlines([850],0,20 ,label= '$\omega_0$', linewidth=1)
plt.grid()
#plt.yscale('log')
plt.subplots_adjust(left=0.1, right=1, top=1, bottom=0.1)
plt.savefig('PropAmericumRate.pdf')
plt.show()

loghight=np.log(hight)

voltC=[]
countsC=[]
hightC=[]
for line in open(filenames[1],"r"):
    l=line.split(',')
    voltC.append(float(l[0]))  #Übergang 21 --> 22
    countsC.append(float(l[1]))
    hightC.append(float(l[2]))

print('Here')
print([i if hightC[i]<1 else '' for i in range(len(countsC))])
delete=[]#[ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25]#[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 21,22,23,24]
voltC = np.delete(np.array(voltC), delete)
countsC = np.delete(np.array(countsC), delete)
hightC = np.delete(np.array(hightC), delete)

#Potentieller Fit um Steigungen zu verifiezieren : lower: 9 - 21, higher : 22-27
#Voltage Multiklicator Offset Korrektur
voltC = np.array([voltC[i]+300 if i>21 else voltC[i] for i in range(len(voltC))])


# Background Korrectur:
rateC=np.array(countsC)/30
rateC=np.array([rateC[i]+linfit(voltC[i], bB, aB) if voltC[i] > bB/aB else rateC[i] for i in range(len(rateC))])
'''
##Proportionalzähler Kolenstoff
xpoints=np.linspace(850,1700,500)
plt.figure(figsize=(11,8))
plt.xlabel('Voltage')
plt.ylabel('Rate [1/s]')
#guess=[-89, 0.1]
#params, corr = opt.curve_fit(linfit, volt, counts, sigma=[np.sqrt(i) for i in counts], p0=guess )
#print(params[0],params[1])
print([i if countsC[i]<0.1 else '' for i in range(len(countsC))])
#chiq=chisquare( volt, counts,[0 for i in counts], [np.sqrt(i) for i in counts], linfit ,params[0],params[1])
pt.plot_errorbar(voltC,rateC,[0 for i in countsC], [np.sqrt(i)/30 if i > 0.2 else 0 for i in countsC],"points", 'best')
#pt.plot(xpoints,linfit(xpoints,params[0],params[1]),"Linfit $\chi^2 /ndf$ = "+str(round(chiq/(len(volt)-2),5)),'best','blue','-')
#plt.vlines([850],0,20 ,label= '$\omega_0$', linewidth=1)
plt.grid()
#plt.yscale('log')
plt.subplots_adjust(left=0.1, right=1, top=1, bottom=0.1)
plt.savefig('PropKolenstoffRate.pdf')
plt.show()
'''

##Proportionalzähler Armericum und Kolenstoff
xpoints=np.linspace(850,1700,500)
plt.figure(figsize=(16,8))
plt.xlabel('Voltage')
plt.ylabel('Rate [1/s]')
pt.plot_errorbar(volt,rate,[0 for i in counts], [np.sqrt(i)/30 if i > 0.2 else 0 for i in counts],"Americum", 'best', 'red')
pt.plot_errorbar(voltC,rateC,[0 for i in countsC], [np.sqrt(i)/30 if i > 0.2 else 0 for i in countsC],"Kohlenstoff", 'best', 'blue')
#plt.vlines([850],0,20 ,label= '$\omega_0$', linewidth=1)
plt.grid()
#plt.yscale('log')
plt.subplots_adjust(left=0.1, right=1, top=1, bottom=0.1)
#plt.savefig('PropKolenstoffRate.pdf')
plt.show()

'''
xpoints=np.linspace(700,1300,500)
plt.figure(figsize=(11,8))
plt.xlabel('Voltage')
plt.ylabel('log(Peak Hight [V])')
guess=[0, 0.0054]
params1, corr1 = opt.curve_fit(linfit, volt[9:21], loghight[9:21], p0=guess )
guess=[0, 0.0054]
params2, corr2 = opt.curve_fit(linfit, volt[22:27], loghight[22:27], p0=guess )
print(params1[0],params1[1])
print(params2[0],params2[1])
print([i if counts[i]==1 else '' for i in range(len(counts))])
#chiq=chisquare( volt, counts,[0 for i in counts], [np.sqrt(i) for i in counts], linfit ,params[0],params[1])
pt.plot_errorbar(volt,loghight,[0 for i in counts], [0 for i in hight],"points", 'best')
pt.plot(xpoints,linfit(xpoints,params1[0],params1[1]),'x1','best','green','-')
pt.plot(xpoints,linfit(xpoints,params2[0],params2[1]),'x2','best','blue','-')
#pt.plot(xpoints,linfit(xpoints,params[0],params[1]),"Linfit $\chi^2 /ndf$ = "+str(round(chiq/(len(volt)-2),5)),'best','blue','-')
#plt.vlines([850],0,20 ,label= '$\omega_0$', linewidth=1)
plt.grid()
#plt.yscale('log')
plt.subplots_adjust(left=0.1, right=1, top=1, bottom=0.1)
plt.savefig('PropAmericumPeakheight.pdf')
plt.show()

delete=[ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25]#[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 21,22,23,24]
voltC = np.delete(np.array(voltC), delete)
countsC = np.delete(np.array(countsC), delete)
hightC = np.delete(np.array(hightC), delete)

xpoints=np.linspace(700,1300,500)
plt.figure(figsize=(15,8))
plt.xlabel('Voltage')
plt.ylabel('Peak Hight [V]')
#guess=[0, 0.0054]
#params1, corr1 = opt.curve_fit(linfit, volt[9:21], loghight[9:21], p0=guess )
#guess=[0, 0.0054]
#params2, corr2 = opt.curve_fit(linfit, volt[22:27], loghight[22:27], p0=guess )
#print(params1[0],params1[1])
#print(params2[0],params2[1])
#print([i if counts[i]==1 else '' for i in range(len(counts))])
#chiq=chisquare( volt, counts,[0 for i in counts], [np.sqrt(i) for i in counts], linfit ,params[0],params[1])
pt.plot_errorbar(voltC,hightC,[0 for i in countsC], [0 for i in hightC],"points", 'best')
#pt.plot(xpoints,linfit(xpoints,params1[0],params1[1]),'x1','best','green','-')
#pt.plot(xpoints,linfit(xpoints,params2[0],params2[1]),'x2','best','blue','-')
#pt.plot(xpoints,linfit(xpoints,params[0],params[1]),"Linfit $\chi^2 /ndf$ = "+str(round(chiq/(len(volt)-2),5)),'best','blue','-')
#plt.vlines([850],0,20 ,label= '$\omega_0$', linewidth=1)
plt.grid()
plt.yscale('log')
plt.subplots_adjust(left=0.1, right=1, top=1, bottom=0.1)
plt.savefig('PropKohlenstoffPeakheight.pdf')
plt.show()


#Potentieller Fit um Steigungen zu verifiezieren : lower: 9 - 21, higher : 22-27
#Voltage Multiklicator Offset Korrektur
volt = np.array([volt[i]-300 if i>21 else volt[i] for i in range(len(volt))])

xpoints=np.linspace(500,1100,500)
plt.figure(figsize=(11,8))
plt.xlabel('Voltage')
plt.ylabel('log(Peak Hight [V])')
guess=[0, 0.0054]
params1, corr1 = opt.curve_fit(linfit, volt[9:21], loghight[9:21], p0=guess )
guess=[0, 0.0054]
params2, corr2 = opt.curve_fit(linfit, volt[22:27], loghight[22:27], p0=guess )
print(params1[0],params1[1])
print(params2[0],params2[1])
print([i if counts[i]==1 else '' for i in range(len(counts))])
#chiq=chisquare( volt, counts,[0 for i in counts], [np.sqrt(i) for i in counts], linfit ,params[0],params[1])
pt.plot_errorbar(volt,loghight,[0 for i in counts], [0 for i in hight],"points", 'best')
pt.plot(xpoints,linfit(xpoints,params1[0],params1[1]),'x1','best','green','-')
pt.plot(xpoints,linfit(xpoints,params2[0],params2[1]),'x2','best','blue','-')
#pt.plot(xpoints,linfit(xpoints,params[0],params[1]),"Linfit $\chi^2 /ndf$ = "+str(round(chiq/(len(volt)-2),5)),'best','blue','-')
#plt.vlines([850],0,20 ,label= '$\omega_0$', linewidth=1)
plt.grid()
#plt.yscale('log')
plt.subplots_adjust(left=0.1, right=1, top=1, bottom=0.1)
plt.savefig('PropAmericumPeakheightwhoutcorrect.pdf')
plt.show()
'''
