#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 26 17:05:13 2019

@author: schoko
"""


import sys 
if '/home/schoko/Documents/grundpraktikum-2/Python/plottingTemplate' not in sys.path:
    sys.path.append('/home/schoko/Documents/grundpraktikum-2/Python/plottingTemplate')

import plotting as pt
import matplotlib.pyplot as plt
import matplotlib
import scipy as sc
import scipy.optimize as opt
import numpy as np
from scipy.special import gamma

font = {'family' : 'DejaVu Sans',
        'weight' : 'normal',
        'size'   : 18}

matplotlib.rc('font', **font)

def histo(a,bins):
    res=[0.0 for i in bins]
    for x in a:
        for i in range(len(bins)-1):
            if bins[i] <= x < bins[i+1] :
                res[i]+=1.0
    diff=bins[1]-bins[0]
    le=bins[-1]-bins[0]
    norm=0.0
    for xi in res:
        norm+=diff*xi
    reserr=  [np.sqrt(ri)  for ri in res]
    return np.array(res)/norm , np.array(reserr)/norm

gausscurve = lambda x,x0,s : 1/(np.sqrt(2*np.pi)*s)*np.exp(-(x-x0)**2/(2*s**2))

poisscurve = lambda x,mu,s : mu**(x)*np.exp(-mu)/gamma(x+1) 

exponcurve = lambda x,A,a : 1/A*np.exp(-1/A*x)

def chisquare (x,y,xerr,yerr,curve, mean, var):
    h=0.0002
    curveabl=[(curve(xi+h/2 ,mean, var)-curve(xi-h/2,mean, var))/h for xi in x]
    return sum([(y[i]-curve(x[i],mean, var))**2/((yerr[i])**2 +(curveabl[i]*xerr[i])**2 ) if y[i]!=0 else 0 for i in range(len(x))])

gauss=[]
for line in open("gauss.dat","r"):
    gauss.append(int(line))
    
poission=[]
for line in open("Poission.dat","r"):
    poission.append(int(line))
    
zeit=[49.6, 2.2000000000000006, 11.400000000000007, 0.5999999999999894, 16.4, 30.000000000000014, 2.799999999999997, 14.599999999999987, 38.20000000000001, 5.599999999999994, 0.7999999999999952, 4.200000000000009, 2.3999999999999853, 4.200000000000009, 4.400000000000015, 5.8, 3.799999999999998, 5.000000000000004, 9.99999999999998, 1.2000000000000066, 14.399999999999997, 6.200000000000011, 53.80000000000001, 1.0000000000000009, 50.59999999999998, 10.000000000000009, 37.39999999999999, 58.199999999999974, 18.400000000000027, 7.599999999999996, -155.0, 14.000000000000012, 4.999999999999977, 4.0000000000000036, 26.00000000000001, 20.000000000000004, 20.99999999999999, 110.0, 25.000000000000007, 1.999999999999988, 39.00000000000001, 50.0, 10.000000000000009, 33.99999999999997, 21.000000000000018, 35.0, 4.0000000000000036, 27.999999999999968, 57.99999999999999, 11.00000000000001, 70.0, 4.0000000000000036, 69.0, 69.99999999999996, 9.000000000000007, 10.000000000000009, 154.00000000000003, 10.000000000000009, 86.99999999999997, 50.00000000000004, 133.9999999999999, 22.00000000000002, 21.000000000000128, 44.00000000000004, 19.999999999999794, 11.00000000000012, 54.999999999999936, 28.000000000000025, 8.000000000000007, 60.99999999999994, 10.000000000000009, 54.00000000000005, 92.99999999999997, 18.000000000000014, 58.00000000000005, 2.9999999999998916, 5.0000000000001155, 72.00000000000006, 27.9999999999998, 27.000000000000135, 40.99999999999993, 44.99999999999993, 127.0, 14.000000000000012, 27.000000000000135, 94.99999999999997, 40.000000000000036, 12.9999999999999, 97.99999999999986, 28.000000000000025, 9.000000000000341, 40.99999999999993, 72.99999999999996, -73.0, 73.0, 76.0, 13.999999999999998, 32.0, 70.0, 113.99999999999999, 131.0, 124.00000000000006, 49.999999999999936, 25.00000000000002, 46.00000000000004, 84.99999999999997, 107.99999999999999, 20.000000000000018, 35.00000000000003, 36.99999999999992, 18.000000000000014, 51.00000000000004, 40.000000000000036, 10.000000000000009, 24.00000000000002, 4.999999999999893, 56.00000000000005, 90.99999999999997, 22.999999999999908, 48.00000000000004, 29.000000000000135, 39.999999999999815, 108.0000000000001, 34.00000000000003, 181.00000000000006, 6.000000000000005, 54.999999999999936, 10.000000000000009, 42.99999999999993, 28.000000000000025, 157.99999999999991, 34.000000000000256, 98.99999999999976, 11.00000000000012, 20.000000000000018, 27.000000000000135, 62.99999999999972]
#for line in open("Zeit.dat","r"):
#    zeit.append(int(line))


##Fit eienrGausskurve
n=63-25
xbins=np.linspace(26,63,n)
xbins+=(xbins[1]-xbins[0])/2
xpoints=np.linspace(20,70,500)

anz, err=histo(gauss, xbins)
print(max(anz))
plt.figure(figsize=(15,8))
plt.xlabel('Counts')
plt.ylabel('Amount')
plt.hist(gauss, xbins , normed = 'true')

xbins+=(xbins[1]-xbins[0])/2

print([i if anz[i]==0 else '' for i in range(len(anz))])
anz = np.delete(anz, [0, 1, 2, 4, 5, 33, 34, 36, 37])
err = np.delete(err, [0, 1, 2, 4, 5, 33, 34, 36, 37])
xbins = np.delete(xbins, [0, 1, 2, 4, 5, 33, 34, 36, 37])
xerr= [0 for i in xbins]
print(len(anz))
guess=[np.mean(gauss), np.sqrt(np.var(gauss))]
params, corr = opt.curve_fit(gausscurve, xbins, anz, sigma=err, p0=guess )

print(params)
chiq=chisquare(xbins,anz,xerr, err, gausscurve,np.mean(gauss), np.sqrt(np.var(gauss)))
pt.plot_errorbar(xbins,anz,xerr, err,"points", 'best')
pt.plot(xpoints,gausscurve(xpoints,np.mean(gauss), np.sqrt(np.var(gauss))),"Gaussdistribution errechnet $\chi^2$ = "+str(round(chiq,3)),'best','red','-')

chiq=chisquare(xbins,anz,xerr, err, gausscurve,params[0],params[1])
pt.plot(xpoints,gausscurve(xpoints,params[0],params[1]),"Gaussdistribution gefittet $\chi^2$ = "+str(round(chiq,3)),'best','green','-')
#pt.plot(xpoints,gausscurve(xpoints,np.mean(gauss), 7.38),'gauss 7.38','best','blue','-')  mit $x_0$ = "+str(round(np.mean(gauss),0)) +"$\pm$"+ str(round( np.sqrt(np.var(gauss)),3))
#pt.plot(xpoints,poisscurve(xpoints,np.mean(gauss)),'poission','best','green','-')
#plt.vlines([30],0,0.14 ,label= '$\omega_0$', linewidth=1)
plt.grid()
#plt.yscale('log')
plt.subplots_adjust(left=0.1, right=1, top=1, bottom=0.1)
plt.savefig('Gauss1.pdf')
plt.show()

##Fit einer Poissionkurve

xbins=np.linspace(-0.5,4.5,6)
xpoints=np.linspace(0,6,500)
#abberr=np.array([5/np.sqrt(12) for i in range(len(data.values()))])
#print()
#m,em,b,eb,chiq,corr = ana.lineare_regression(mi,si , abberr)
#print(m,em)
#print(b,eb)
#xbins=np.linspace(0,180)
print(np.mean(poission))
anz, err=histo(poission, xbins)
plt.figure(figsize=(15,8))
plt.xlabel('Counts')
plt.ylabel('Amount')
plt.hist(poission, xbins , normed = 'true')
xbins+=(xbins[1]-xbins[0])/2

print([i if anz[i]==0 else '' for i in range(len(anz))])
anz = np.delete(anz, [5])
err = np.delete(err, [5])
xbins = np.delete(xbins, [5])
xerr= [0 for i in xbins]

print(len(anz))
guess=[np.mean(poission), np.sqrt(np.var(poission))]
params, corr = opt.curve_fit(poisscurve, xbins, anz, sigma=err, p0=guess )

print(params)
xerr= [0 for i in xbins]
pt.plot_errorbar(xbins,anz,xerr, err,"points", 'best')
pt.plot(xpoints,poisscurve(xpoints,np.mean(poission),0                 ),"Poissiondistribution $\chi^2$ = "+str(round(chisquare(xbins,anz,xerr, err, poisscurve,np.mean(poission),0),3)),'best','red','-')
chiq=chisquare(xbins,anz,xerr, err, poisscurve,params[0],params[1])
pt.plot(xpoints,poisscurve(xpoints,params[0],params[1]),"Poissiondistribution gefittet $\chi^2$ = "+str(round(chiq,3)),'best','green','-')

#plt.vlines([1000,1170],0,0.14 ,label= '$\omega_0$', linewidth=1)
plt.grid()
#plt.yscale('log')
plt.subplots_adjust(left=0.1, right=1, top=1, bottom=0.1)
plt.savefig('Poission1.pdf')
plt.show()

##Fit einer Exponentialkurve

xbins=np.linspace(5,155,16)
#xbins=np.linspace(5,155,32)
xpoints=np.linspace(0,160,500)



anz, err=histo(zeit, xbins)
plt.figure(figsize=(15,8))
plt.xlabel('Time between Counts')
plt.ylabel('Ammount')
plt.hist(zeit, xbins , normed = 'true')
xbins+=(xbins[1]-xbins[0])/2

print([i if anz[i]==0 else '' for i in range(len(anz))])

deletion=[13,15]
#deletion=[12,15,20,23,27,28,29,31]

anz = np.delete(anz,deletion )
err = np.delete(err, deletion)
xbins = np.delete(xbins, deletion)
xerr= [0 for i in xbins]

xerr= [(xbins[1]-xbins[0])/np.sqrt(12) for i in xbins]

print(len(zeit))
##Fit Expocurve
guess=[np.mean(zeit), np.sqrt(np.var(zeit))]
params, corr = opt.curve_fit(exponcurve, xbins, anz, sigma=err, p0=guess )

print(params)
print(np.mean(zeit), np.sqrt(np.var(zeit)))
pt.plot_errorbar(xbins,anz,xerr, err,"points", 'best')
pt.plot(xpoints,exponcurve(xpoints,np.mean(zeit),0                 ),"Exponentialdistribution $\chi^2$ = "+str(round(chisquare(xbins,anz,xerr, err, exponcurve,np.mean(zeit),0),3)),'best','red','-')

chiq=chisquare(xbins,anz,xerr, err, exponcurve,params[0],params[1])
pt.plot(xpoints,exponcurve(xpoints,params[0],params[1]),"Exponentialdistribution gefittet $\chi^2$ = "+str(round(chiq,3)),'best','green','-')

#plt.vlines([1000,1170],0,0.14 ,label= '$\omega_0$', linewidth=1)
plt.grid()
#plt.yscale('log')
plt.subplots_adjust(left=0.1, right=1, top=1, bottom=0.1)
plt.savefig('Zeit2.pdf')
plt.show()
'''
##Fit einer Poissionkurve

xbins=np.linspace(5,155,16)
xpoints=np.linspace(0,160,500)
#abberr=np.array([5/np.sqrt(12) for i in range(len(data.values()))])
#print()
#m,em,b,eb,chiq,corr = ana.lineare_regression(mi,si , abberr)
#print(m,em)
#print(b,eb)
#xbins=np.linspace(0,180)
print(np.mean(zeit))
anz, err=histo(zeit, xbins)
plt.figure(figsize=(15,8))
plt.xlabel('Counts')
plt.ylabel('Amount')
plt.hist(zeit, xbins , normed = 'true')
xbins+=(xbins[1]-xbins[0])/2

print([i if anz[i]==0 else '' for i in range(len(anz))])
anz = np.delete(anz, [5])
err = np.delete(err, [5])
xbins = np.delete(xbins, [5])
xerr= [0 for i in xbins]

print(len(anz))
guess=[np.mean(zeit), np.sqrt(np.var(zeit))]
params, corr = opt.curve_fit(poisscurve, xbins, anz, sigma=err, p0=guess )

xerr= [0 for i in xbins]
pt.plot_errorbar(xbins,anz,xerr, err,"points", 'best')
pt.plot(xpoints,poisscurve(xpoints,np.mean(zeit),0                 ),"Poissiondistribution $\chi^2$ = "+str(round(chisquare(xbins,anz,xerr, err, poisscurve,np.mean(zeit),0),5)),'best','red','-')
chiq=chisquare(xbins,anz,xerr, err, poisscurve,params[0],params[1])
pt.plot(xpoints,poisscurve(xpoints,params[0],params[1]),"Poissiondistribution gefittet $\chi^2$ = "+str(round(chiq,5)),'best','green','-')

#plt.vlines([1000,1170],0,0.14 ,label= '$\omega_0$', linewidth=1)
plt.grid()
#plt.yscale('log')
plt.savefig('ZeitPoission.pdf')
plt.show()
'''