#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 26 14:39:47 2019

@author: schoko
"""

import sys 
if '/home/schoko/Documents/grundpraktikum-2/Python/plottingTemplate' not in sys.path:
    sys.path.append('/home/schoko/Documents/grundpraktikum-2/Python/plottingTemplate')

import plotting as pt
import matplotlib.pyplot as plt
import matplotlib
import scipy as sc
import scipy.optimize as opt
import numpy as np

font = {'family' : 'DejaVu Sans',
        'weight' : 'normal',
        'size'   : 18}

matplotlib.rc('font', **font)

tau=593*10**-6 
tauerr= 151*10**-6

spannung = [200,210,230,240,250,260,270,280,290,300,310,320,330,340,350,360,370,380,390,400,410,420,430,440,450,460,470,480,490,500,510,520,530,540,550,560,570,580,590,600,610,620,630,640,650,660,670,680,690,700]
counts = [0,0,2,0,4,7,15,43,41,39,61,148,167,190,247,259,298,329,307,377,439,499,509,512,538,617,591,663,655,680,666,669,676,704,724,758,760,766,768,804,795,791,782,806,801,764,810,866,766,804]



rate=np.array(counts)/10

#abberr=np.array([5/np.sqrt(12) for i in range(len(data.values()))])
#print()
#m,em,b,eb,chiq,corr = ana.lineare_regression(mi,si , abberr)
#print(m,em)
#print(b,eb)
#xbins=np.linspace(0,180)
plt.figure(figsize=(11,8))
plt.xlabel('U/V')
plt.ylabel('Pulsrate [1/s]')
plt.vlines([250],0,80 ,label= '$U_T = 250 V$',color = 'blue', linestyles ='solid', linewidth=3)
plt.vlines([500],0,80 ,label= '$U_G = 500 V$',color = 'green', linestyles ='dashed', linewidth=3)
pt.plot_errorbar(spannung,rate, [0 for i in range(len(counts))],[np.sqrt(i)/10 for i in counts], 'Datenpunkte',  'lower right')
plt.grid()
#plt.yscale('log')
plt.subplots_adjust(left=0.1, right=1, top=1, bottom=0.1)
plt.savefig('GM1.pdf')
plt.show()

plt.figure(figsize=(11,8))
plt.xlabel('U/V')
plt.ylabel('Pulsrate (log) [1/s]')
plt.vlines([250],0,80 ,label= '$U_T = 250 V$',color = 'blue', linestyles ='solid', linewidth=3)
plt.vlines([500],0,80 ,label= '$U_G = 500 V$',color = 'green', linestyles ='dashed', linewidth=3)
pt.plot_errorbar(spannung,rate, [0 for i in range(len(counts))],[np.sqrt(i)/10 for i in counts], 'Datenpunkte',  'lower right')
plt.grid()
plt.yscale('log')
plt.subplots_adjust(left=0.1, right=1, top=1, bottom=0.1)
plt.savefig('GM1log.pdf')
plt.show()


spannung = [250,260,270,280,290,300,310,320,330,340,350,360,370,380,390,400,410,420,430,440,450,460,470,480,490,500,510,520,530,540,550,560,570,580,590,600,610,620,630,640,650,660,670,680,690,700]
counts = [4,7,15,43,41,39,61,148,167,190,247,259,298,329,307,377,439,499,509,512,538,617,591,663,655,680,666,669,676,704,724,758,760,766,768,804,795,791,782,806,801,764,810,866,766,804]


rate=np.array(counts)/10


plt.figure(figsize=(11,8))
plt.xlabel('U/V')
plt.ylabel('Pulsrate [1/s]')
pt.plot_errorbar(spannung,rate, [0 for i in range(len(counts))],[np.sqrt(i)/10 for i in counts], 'Datenpunkte',  'best')
pt.plot_errorbar(spannung,rate/(1-rate*tau), [0 for i in range(len(counts))],[np.sqrt((np.sqrt(float(i))/(10*(float(i)/10)**2))**2+float(tauerr)**2)/(10/float(i)+tau)**2 for i in counts], 'Mit Totzeitkorrektur',  'best', 'blue')
#plt.vlines([1000,1170],0,0.14 ,label= '$\omega_0$', linewidth=1)
plt.grid()
#plt.yscale('log')
plt.subplots_adjust(left=0.1, right=1, top=1, bottom=0.1)
plt.savefig('GM1_tot.pdf')
plt.show()

plt.figure(figsize=(11,8))
plt.xlabel('U/V')
plt.ylabel('Pulsrate (log) [1/s]')
pt.plot_errorbar(spannung,rate, [0 for i in range(len(counts))],[np.sqrt(i)/10 for i in counts], 'Datenpunkte',  'best')
pt.plot_errorbar(spannung,rate/(1-rate*tau), [0 for i in range(len(counts))],[np.sqrt((np.sqrt(float(i))/(10*(float(i)/10)**2))**2+float(tauerr)**2)/(10/float(i)+tau)**2 for i in counts], 'Mit Totzeitkorrektur',  'best', 'blue')
#plt.vlines([1000,1170],0,0.14 ,label= '$\omega_0$', linewidth=1)
plt.grid()
plt.yscale('log')
plt.subplots_adjust(left=0.1, right=1, top=1, bottom=0.1)
plt.savefig('GM1log_tot.pdf')
plt.show()


xpoints=np.arange(0,100)


plt.figure(figsize=(11,8))
plt.xlabel('Gemessene Pulsrate / s')
plt.ylabel('Pulsrate (log) [1/s]')
pt.plot(xpoints,xpoints,'N=n','best','red','-')
pt.plot(xpoints,xpoints/(1-xpoints*tau),'N==n/(1-n*tau)','best','blue','-')
#plt.vlines([1000,1170],0,0.14 ,label= '$\omega_0$', linewidth=1)
plt.grid()
plt.yscale('log')
plt.subplots_adjust(left=0.1, right=1, top=1, bottom=0.1)
plt.savefig('GM1log_tot_theo.pdf')
plt.show()


plt.figure(figsize=(11,8))
plt.xlabel('Gemessene Pulsrate / s')
plt.ylabel('Pulsrate [1/s]')
pt.plot(xpoints,xpoints,'N=n','best','red','-')
pt.plot(xpoints,xpoints/(1-xpoints*tau),'N=n/(1-n*tau)','best','blue','-')
#plt.vlines([1000,1170],0,0.14 ,label= '$\omega_0$', linewidth=1)
plt.grid()
#plt.yscale('log')
plt.subplots_adjust(left=0.1, right=1, top=1, bottom=0.1)
plt.savefig('GM1_tot_theo.pdf')
plt.show()
