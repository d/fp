#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 18 18:59:17 2019

@author: schoko
"""
#%%
import numpy as np

'''
print(num*ac*np.sqrt((1-(x2/x1)**2)/(y2**2-(y1*x2/x1)**2)))


print(num*ac*np.sqrt((1-(x3/x1)**2)/(y3**2-(y1*x3/x1)**2)))


print(num*ac*np.sqrt((1-(x2/x3)**2)/(y2**2-(y3*x2/x3)**2)))

print(num*ac*np.sqrt((1-(x3/x2)**2)/(y3**2-(y2*x3/x2)**2)))


print(num*ac*np.sqrt((1-(x1/x2)**2)/(y1**2-(y2*x1/x2)**2)))

print()
'''

'''
print(num*ac*np.sqrt((1-(x2/x1)**2)/(y2**2-(y1*x2/x1)**2)))


print(num*ac*np.sqrt((1-(x3/x1)**2)/(y3**2-(y1*x3/x1)**2)))


print(num*ac*np.sqrt((1-(x2/x3)**2)/(y2**2-(y3*x2/x3)**2)))

print()

ay=num*ac*np.sqrt((1-(x3/x1)**2)/(y3**2-(y1*x3/x1)**2))
print(np.sqrt((num*ac)**2-(ay*y1)**2)/x1)

ay=num*ac*np.sqrt((1-(x2/x3)**2)/(y2**2-(y3*x2/x3)**2))
print(np.sqrt((num*ac)**2-(ay*y3)**2)/x3)
'''

# Gain 12,5nm
num=35
ac=0.246
x1=0.63
y1=6.24
x2=7.69
y2=1.92
x3=7.04
y3=4.35

print("-- Gain 12,5nm --")

print("-> y")
a=(np.sqrt((num*ac)**2/x3**2/((y2**2-y1**2)/(x1**2-x2**2)+(y3/x3)**2)))

b=(np.sqrt((num*ac)**2/x2**2/((y3**2-y1**2)/(x1**2-x3**2)+(y2/x2)**2)))

c=(np.sqrt((num*ac)**2/x1**2/((y2**2-y3**2)/(x3**2-x2**2)+(y1/x1)**2)))


sigx1=0.05
sigx2=0.05
sigx3=0.05
sigy1=0.05
sigy2=0.05
sigy3=0.05
sig1=0
sig2=0
sig3=0

sig1=(num*ac)**2/(x3**2)*((y2**2-y1**2)/(x1**2-x2**2)-y3**2/x3**2)**(-1.5)*((-1/x3*((y2**2-y1**2)/(x1**2-x2**2)+y3**2/(x3**2))+y3**2/(2*x3**2))**2*sigx3**2+((y2**2-y1**2)**2*x1**2*sigx1**2+(y2**2-y1**2)**2*x2**2*sigx2**2)/((x1**2-x2**2)**4)+(4*y1**2*sigy1**2+4*y2**2*sigy2**2)/((x1**2-x2**2)**2)+4*y3**2/x3**4*sigy3**2)
sig2=(num*ac)**2/(x2**2)*((y3**2-y1**2)/(x1**2-x3**2)-y2**2/x2**2)**(-1.5)*((-1/x2*((y3**2-y1**2)/(x1**2-x3**2)+y2**2/(x2**2))+y2**2/(2*x2**2))**2*sigx2**2+((y3**2-y1**2)**2*x1**2*sigx1**2+(y3**2-y1**2)**2*x3**2*sigx3**2)/((x1**2-x3**2)**4)+(4*y1**2*sigy1**2+4*y3**2*sigy3**2)/((x1**2-x3**2)**2)+4*y2**2/x2**4*sigy2**2)
#sig3=(num*ac)**2/(x1**2)*((y2**2-y3**2)/(x3**2-x2**2)-y1**2/x1**2)**(-1.5)*((1/x1*((y2**2-y3**2)/(x3**2-x2**2))+y1**2/(2*x1**2))**2*sigx1**2+((y2**2-y3**2)**2*x3**2*sigx3**2+(y2**2-y3**2)**2*x2**2*sigx2**2)/((x3**2-x2**2)**4)+(4*y3**2*sigy3**2+4*y2**2*sigy2**2)/((x3**2-x2**2)**2)+4*y1**2/x1**4*sigy1**2)

alphay1=0
alphay1=(a/sig1+b/sig2)/(1/sig1+1/sig2)
Fehler1=0
Fehler1=1/np.sqrt(1/sig1+1/sig2)
print("Mittelwert: ",alphay1)
print("Fehler: ", Fehler1)


# Gain 4nm
num=15
ac=0.246
x1=0.22
y1=2.56
x2=3.07
y2=1.79
x3=3.3
y3=0.78

print("-- Gain 4nm --")

print("-> y")
a=(np.sqrt((num*ac)**2/x3**2/((y2**2-y1**2)/(x1**2-x2**2)+(y3/x3)**2)))

b=(np.sqrt((num*ac)**2/x2**2/((y3**2-y1**2)/(x1**2-x3**2)+(y2/x2)**2)))

c=(np.sqrt((num*ac)**2/x1**2/((y2**2-y3**2)/(x3**2-x2**2)+(y1/x1)**2)))

sigx1=0.05
sigx2=0.05
sigx3=0.05
sigy1=0.05
sigy2=0.05
sigy3=0.05
sig1=0
sig2=0
sig3=0

sig1=(num*ac)**2/(x3**2)*((y2**2-y1**2)/(x1**2-x2**2)-y3**2/x3**2)**(-1.5)*((-1/x3*((y2**2-y1**2)/(x1**2-x2**2)+y3**2/(x3**2))+y3**2/(2*x3**2))**2*sigx3**2+((y2**2-y1**2)**2*x1**2*sigx1**2+(y2**2-y1**2)**2*x2**2*sigx2**2)/((x1**2-x2**2)**4)+(4*y1**2*sigy1**2+4*y2**2*sigy2**2)/((x1**2-x2**2)**2)+4*y3**2/x3**4*sigy3**2)
sig2=(num*ac)**2/(x2**2)*((y3**2-y1**2)/(x1**2-x3**2)-y2**2/x2**2)**(-1.5)*((-1/x2*((y3**2-y1**2)/(x1**2-x3**2)+y2**2/(x2**2))+y2**2/(2*x2**2))**2*sigx2**2+((y3**2-y1**2)**2*x1**2*sigx1**2+(y3**2-y1**2)**2*x3**2*sigx3**2)/((x1**2-x3**2)**4)+(4*y1**2*sigy1**2+4*y3**2*sigy3**2)/((x1**2-x3**2)**2)+4*y2**2/x2**4*sigy2**2)
#sig3=(num*ac)**2/(x1**2)*((y2**2-y3**2)/(x3**2-x2**2)-y1**2/x1**2)**(-1.5)*((1/x1*((y2**2-y3**2)/(x3**2-x2**2))+y1**2/(2*x1**2))**2*sigx1**2+((y2**2-y3**2)**2*x3**2*sigx3**2+(y2**2-y3**2)**2*x2**2*sigx2**2)/((x3**2-x2**2)**4)+(4*y3**2*sigy3**2+4*y2**2*sigy2**2)/((x3**2-x2**2)**2)+4*y1**2/x1**4*sigy1**2)

alphay2=0
alphay2=(a/sig1+b/sig2)/(1/sig1+1/sig2)
Fehler2=0
Fehler2=1/np.sqrt(1/sig1+1/sig2)
print("Mittelwert: ",alphay2)
print("Fehler: ", Fehler2)



# Gain 1,5nm
num=5
ac=0.246
x1=0.271
y1=0.595
x2=1.155
y2=0.158
x3=0.882
y3=0.439

print("-- Gain 1,5nm --")

print("-> y")
a=(np.sqrt((num*ac)**2/x3**2/((y2**2-y1**2)/(x1**2-x2**2)+(y3/x3)**2)))

b=(np.sqrt((num*ac)**2/x2**2/((y3**2-y1**2)/(x1**2-x3**2)+(y2/x2)**2)))

c=(np.sqrt((num*ac)**2/x1**2/((y2**2-y3**2)/(x3**2-x2**2)+(y1/x1)**2)))

sigx1=0.05
sigx2=0.05
sigx3=0.05
sigy1=0.05
sigy2=0.05
sigy3=0.05
sig1=0
sig2=0
sig3=0

sig1=(num*ac)**2/(x3**2)*((y2**2-y1**2)/(x1**2-x2**2)-y3**2/x3**2)**(-1.5)*((-1/x3*((y2**2-y1**2)/(x1**2-x2**2)+y3**2/(x3**2))+y3**2/(2*x3**2))**2*sigx3**2+((y2**2-y1**2)**2*x1**2*sigx1**2+(y2**2-y1**2)**2*x2**2*sigx2**2)/((x1**2-x2**2)**4)+(4*y1**2*sigy1**2+4*y2**2*sigy2**2)/((x1**2-x2**2)**2)+4*y3**2/x3**4*sigy3**2)
sig2=(num*ac)**2/(x2**2)*((y3**2-y1**2)/(x1**2-x3**2)-y2**2/x2**2)**(-1.5)*((-1/x2*((y3**2-y1**2)/(x1**2-x3**2)+y2**2/(x2**2))+y2**2/(2*x2**2))**2*sigx2**2+((y3**2-y1**2)**2*x1**2*sigx1**2+(y3**2-y1**2)**2*x3**2*sigx3**2)/((x1**2-x3**2)**4)+(4*y1**2*sigy1**2+4*y3**2*sigy3**2)/((x1**2-x3**2)**2)+4*y2**2/x2**4*sigy2**2)
#sig3=(num*ac)**2/(x1**2)*((y2**2-y3**2)/(x3**2-x2**2)-y1**2/x1**2)**(-1.5)*((1/x1*((y2**2-y3**2)/(x3**2-x2**2))+y1**2/(2*x1**2))**2*sigx1**2+((y2**2-y3**2)**2*x3**2*sigx3**2+(y2**2-y3**2)**2*x2**2*sigx2**2)/((x3**2-x2**2)**4)+(4*y3**2*sigy3**2+4*y2**2*sigy2**2)/((x3**2-x2**2)**2)+4*y1**2/x1**4*sigy1**2)

alphay3=0
alphay3=(a/sig1+b/sig2)/(1/sig1+1/sig2)
Fehler3=0
Fehler3=1/np.sqrt(1/sig1+1/sig2)
print("Mittelwert: ",alphay3)
print("Fehler: ", Fehler3)


# Current 4nm
num=11
ac=0.246
x1=0.02
y1=2.24
x2=2.43
y2=0.71
x3=2.42
y3=1.52

print("-- Current 4nm --")

print("-> y")
a=(np.sqrt((num*ac)**2/x3**2/((y2**2-y1**2)/(x1**2-x2**2)+(y3/x3)**2)))

b=(np.sqrt((num*ac)**2/x2**2/((y3**2-y1**2)/(x1**2-x3**2)+(y2/x2)**2)))

c=(np.sqrt((num*ac)**2/x1**2/((y2**2-y3**2)/(x3**2-x2**2)+(y1/x1)**2)))

sigx1=0.05
sigx2=0.05
sigx3=0.05
sigy1=0.05
sigy2=0.05
sigy3=0.05
sig1=0
sig2=0
sig3=0

sig1=(num*ac)**2/(x3**2)*((y2**2-y1**2)/(x1**2-x2**2)-y3**2/x3**2)**(-1.5)*((-1/x3*((y2**2-y1**2)/(x1**2-x2**2)+y3**2/(x3**2))+y3**2/(2*x3**2))**2*sigx3**2+((y2**2-y1**2)**2*x1**2*sigx1**2+(y2**2-y1**2)**2*x2**2*sigx2**2)/((x1**2-x2**2)**4)+(4*y1**2*sigy1**2+4*y2**2*sigy2**2)/((x1**2-x2**2)**2)+4*y3**2/x3**4*sigy3**2)
sig2=(num*ac)**2/(x2**2)*((y3**2-y1**2)/(x1**2-x3**2)-y2**2/x2**2)**(-1.5)*((-1/x2*((y3**2-y1**2)/(x1**2-x3**2)+y2**2/(x2**2))+y2**2/(2*x2**2))**2*sigx2**2+((y3**2-y1**2)**2*x1**2*sigx1**2+(y3**2-y1**2)**2*x3**2*sigx3**2)/((x1**2-x3**2)**4)+(4*y1**2*sigy1**2+4*y3**2*sigy3**2)/((x1**2-x3**2)**2)+4*y2**2/x2**4*sigy2**2)
#sig3=(num*ac)**2/(x1**2)*((y2**2-y3**2)/(x3**2-x2**2)-y1**2/x1**2)**(-1.5)*((1/x1*((y2**2-y3**2)/(x3**2-x2**2))+y1**2/(2*x1**2))**2*sigx1**2+((y2**2-y3**2)**2*x3**2*sigx3**2+(y2**2-y3**2)**2*x2**2*sigx2**2)/((x3**2-x2**2)**4)+(4*y3**2*sigy3**2+4*y2**2*sigy2**2)/((x3**2-x2**2)**2)+4*y1**2/x1**4*sigy1**2)

alphay4=0
alphay4=(a/sig1+b/sig2)/(1/sig1+1/sig2)
Fehler4=0
Fehler4=1/np.sqrt(1/sig1**2+1/sig2**2)
print("Mittelwert: ",alphay4)
print("Fehler: ", Fehler4)


# Current 3nm
num=6
ac=0.246
x1=1.24
y1=0.84
x2=1.38
y2=0.39
x3=0.15
y3=1.23

print("-- Current 3nm --")

print("-> y")
a=(np.sqrt((num*ac)**2/x3**2/((y2**2-y1**2)/(x1**2-x2**2)+(y3/x3)**2)))

b=(np.sqrt((num*ac)**2/x2**2/((y3**2-y1**2)/(x1**2-x3**2)+(y2/x2)**2)))

c=(np.sqrt((num*ac)**2/x1**2/((y2**2-y3**2)/(x3**2-x2**2)+(y1/x1)**2)))

sigx1=0.05
sigx2=0.05
sigx3=0.05
sigy1=0.05
sigy2=0.05
sigy3=0.05
sig1=0
sig2=0
sig3=0

#sig1=(num*ac)**2/(x3**2)*((y2**2-y1**2)/(x1**2-x2**2)-y3**2/x3**2)**(-1.5)*((-1/x3*((y2**2-y1**2)/(x1**2-x2**2)+y3**2/(x3**2))+y3**2/(2*x3**2))**2*sigx3**2+((y2**2-y1**2)**2*x1**2*sigx1**2+(y2**2-y1**2)**2*x2**2*sigx2**2)/((x1**2-x2**2)**4)+(4*y1**2*sigy1**2+4*y2**2*sigy2**2)/((x1**2-x2**2)**2)+4*y3**2/x3**4*sigy3**2)
sig2=(num*ac)**2/(x2**2)*((y3**2-y1**2)/(x1**2-x3**2)-y2**2/x2**2)**(-1.5)*((-1/x2*((y3**2-y1**2)/(x1**2-x3**2)+y2**2/(x2**2))+y2**2/(2*x2**2))**2*sigx2**2+((y3**2-y1**2)**2*x1**2*sigx1**2+(y3**2-y1**2)**2*x3**2*sigx3**2)/((x1**2-x3**2)**4)+(4*y1**2*sigy1**2+4*y3**2*sigy3**2)/((x1**2-x3**2)**2)+4*y2**2/x2**4*sigy2**2)
sig3=(num*ac)**2/(x1**2)*((y2**2-y3**2)/(x3**2-x2**2)-y1**2/x1**2)**(-1.5)*((1/x1*((y2**2-y3**2)/(x3**2-x2**2))+y1**2/(2*x1**2))**2*sigx1**2+((y2**2-y3**2)**2*x3**2*sigx3**2+(y2**2-y3**2)**2*x2**2*sigx2**2)/((x3**2-x2**2)**4)+(4*y3**2*sigy3**2+4*y2**2*sigy2**2)/((x3**2-x2**2)**2)+4*y1**2/x1**4*sigy1**2)

alphay5=0
alphay5=(c/sig3+b/sig2)/(1/sig3+1/sig2)
Fehler5=0
Fehler5=1/np.sqrt(1/sig3+1/sig2)
print("Mittelwert: ",alphay5)
print("Fehler: ", Fehler5)


# Current 2nm
num=6
ac=0.246
x1=0.008
y1=1.25
x2=1.326
y2=0.863
x3=1.348
y3=0.386

print("-- Current 2nm --")

print("-> y")
a=(np.sqrt((num*ac)**2/x3**2/((y2**2-y1**2)/(x1**2-x2**2)+(y3/x3)**2)))

b=(np.sqrt((num*ac)**2/x2**2/((y3**2-y1**2)/(x1**2-x3**2)+(y2/x2)**2)))

c=(np.sqrt((num*ac)**2/x1**2/((y2**2-y3**2)/(x3**2-x2**2)+(y1/x1)**2)))

sigx1=0.05
sigx2=0.05
sigx3=0.05
sigy1=0.05
sigy2=0.05
sigy3=0.05
sig1=0
sig2=0
sig3=0

sig1=(num*ac)**2/(x3**2)*((y2**2-y1**2)/(x1**2-x2**2)-y3**2/x3**2)**(-1.5)*((-1/x3*((y2**2-y1**2)/(x1**2-x2**2)+y3**2/(x3**2))+y3**2/(2*x3**2))**2*sigx3**2+((y2**2-y1**2)**2*x1**2*sigx1**2+(y2**2-y1**2)**2*x2**2*sigx2**2)/((x1**2-x2**2)**4)+(4*y1**2*sigy1**2+4*y2**2*sigy2**2)/((x1**2-x2**2)**2)+4*y3**2/x3**4*sigy3**2)
sig2=(num*ac)**2/(x2**2)*((y3**2-y1**2)/(x1**2-x3**2)-y2**2/x2**2)**(-1.5)*((-1/x2*((y3**2-y1**2)/(x1**2-x3**2)+y2**2/(x2**2))+y2**2/(2*x2**2))**2*sigx2**2+((y3**2-y1**2)**2*x1**2*sigx1**2+(y3**2-y1**2)**2*x3**2*sigx3**2)/((x1**2-x3**2)**4)+(4*y1**2*sigy1**2+4*y3**2*sigy3**2)/((x1**2-x3**2)**2)+4*y2**2/x2**4*sigy2**2)
#sig3=(num*ac)**2/(x1**2)*((y2**2-y3**2)/(x3**2-x2**2)-y1**2/x1**2)**(-1.5)*((1/x1*((y2**2-y3**2)/(x3**2-x2**2))+y1**2/(2*x1**2))**2*sigx1**2+((y2**2-y3**2)**2*x3**2*sigx3**2+(y2**2-y3**2)**2*x2**2*sigx2**2)/((x3**2-x2**2)**4)+(4*y3**2*sigy3**2+4*y2**2*sigy2**2)/((x3**2-x2**2)**2)+4*y1**2/x1**4*sigy1**2)

alphay6=0
alphay6=(a/sig1+b/sig2)/(1/sig1+1/sig2)
Fehler6=0
Fehler6=1/np.sqrt(1/sig1+1/sig2)
print("Mittelwert: ",alphay6)
print("Fehler: ", Fehler6)


print("---- Gesamter Mittelwert mit Fehler (ohne 1,5nm Gain) ---")
alphay=0
alphay=(alphay1/(Fehler1**2)+alphay2/(Fehler2**2)+alphay3/Fehler3**2+alphay4/Fehler4**2+alphay5/Fehler5**2+alphay6/Fehler6**2)/(1/Fehler1**2+1/Fehler2**2+1/Fehler3**2+1/Fehler4**2+1/Fehler5**2+1/Fehler6**2)
alphay=np.mean([alphay1,alphay2,alphay4,alphay5,alphay6])
sigalphay=0
sigalphay=1/np.sqrt(1/Fehler1**2+1/Fehler2**2+1/Fehler3**2+1/Fehler4**2+1/Fehler5**2+1/Fehler6**2)
sigalphay=np.sqrt(np.var([Fehler1,Fehler2,Fehler4,Fehler5,Fehler6]))
print("Alpha y: ",alphay, "+-", sigalphay)
print(".................................................................")







#%% Gain 12,5nm
num=35
ac=0.246
x1=0.63
y1=6.24
x2=7.69
y2=1.92
x3=7.04
y3=4.35

print("-- Gain 12,5nm --")

print("-> x")
a=(np.sqrt(((num*ac)**2-(alphay*y3)**2)/x3**2))

b=(np.sqrt(((num*ac)**2-(alphay*y2)**2)/x2**2))

sigx1=0.05
sigx2=0.05
sigx3=0.05
sigy1=0.05
sigy2=0.05
sigy3=0.05
sig1=0
sig2=0
sig3=0

sig1=(((num*ac)**2-(alphay*y3)**2)*sigx3**2+alphay**2*y3**4*sigalphay**2+y3**2*alphay**4*sigy3**2)/(x3**4)
sig2=(((num*ac)**2-(alphay*y2)**2)*sigx2**2+alphay**2*y2**4*sigalphay**2+y2**2*alphay**4*sigy2**2)/(x2**4)
#sig3=(((num*ac)**2-(alphay*y1)**2)*sigx1**2+alphay**2*y1**4*sigalphay**2+y1**2*alphay**4*sigy1**2)/(x1**4)


xmean1=0
xmean1=(c/sig1+b/sig2)/(1/sig1+1/sig2)
Fehlerx1=0
Fehlerx1=1/np.sqrt(1/sig1+1/sig2)
print("Mittelwert: ",xmean1)
print("Fehler: ", Fehlerx1)



# Gain 4nm
num=15
ac=0.246
x1=0.22
y1=2.56
x2=3.07
y2=1.79
x3=3.3
y3=0.78

print("-- Gain 4nm --")

print("-> x")
a=(np.sqrt(((num*ac)**2-(alphay*y3)**2)/x3**2))

b=(np.sqrt(((num*ac)**2-(alphay*y2)**2)/x2**2))

sigx1=0.05
sigx2=0.05
sigx3=0.05
sigy1=0.05
sigy2=0.05
sigy3=0.05
sig1=0
sig2=0
sig3=0

sig1=(((num*ac)**2-(alphay*y3)**2)*sigx3**2+alphay**2*y3**4*sigalphay**2+y3**2*alphay**4*sigy3**2)/(x3**4)
sig2=(((num*ac)**2-(alphay*y2)**2)*sigx2**2+alphay**2*y2**4*sigalphay**2+y2**2*alphay**4*sigy2**2)/(x2**4)
#sig3=(((num*ac)**2-(alphay*y1)**2)*sigx1**2+alphay**2*y1**4*sigalphay**2+y1**2*alphay**4*sigy1**2)/(x1**4)

xmean2=0
xmean2=(c/sig1+b/sig2)/(1/sig1+1/sig2)
Fehlerx2=0
Fehlerx2=1/np.sqrt(1/sig1+1/sig2)
print("Mittelwert: ",xmean2)
print("Fehler: ", Fehlerx2)


# Gain 1,5nm
num=5
ac=0.246
x1=0.271
y1=0.595
x2=1.155
y2=0.158
x3=0.882
y3=0.439

print("-- Gain 1,5nm --")

print("-> x")
a=(np.sqrt(((num*ac)**2-(alphay*y3)**2)/x3**2))

b=(np.sqrt(((num*ac)**2-(alphay*y2)**2)/x2**2))

sigx1=0.05
sigx2=0.05
sigx3=0.05
sigy1=0.05
sigy2=0.05
sigy3=0.05
sig1=0
sig2=0
sig3=0

sig1=(((num*ac)**2-(alphay*y3)**2)*sigx3**2+alphay**2*y3**4*sigalphay**2+y3**2*alphay**4*sigy3**2)/(x3**4)
sig2=(((num*ac)**2-(alphay*y2)**2)*sigx2**2+alphay**2*y2**4*sigalphay**2+y2**2*alphay**4*sigy2**2)/(x2**4)
#sig3=(((num*ac)**2-(alphay*y1)**2)*sigx1**2+alphay**2*y1**4*sigalphay**2+y1**2*alphay**4*sigy1**2)/(x1**4)

xmean3=0
xmean3=(c/sig1+b/sig2)/(1/sig1+1/sig2)
Fehlerx3=0
Fehlerx3=1/np.sqrt(1/sig1+1/sig2)
print("Mittelwert: ",xmean3)
print("Fehler: ", Fehlerx3)


# Current 4nm
num=11
ac=0.246
x1=0.02
y1=2.24
x2=2.43
y2=0.71
x3=2.42
y3=1.52

print("-- Current 4nm --")

print("-> x")
a=(np.sqrt(((num*ac)**2-(alphay*y3)**2)/x3**2))

b=(np.sqrt(((num*ac)**2-(alphay*y2)**2)/x2**2))

sigx1=0.05
sigx2=0.05
sigx3=0.05
sigy1=0.05
sigy2=0.05
sigy3=0.05
sig1=0
sig2=0
sig3=0

sig1=(((num*ac)**2-(alphay*y3)**2)*sigx3**2+alphay**2*y3**4*sigalphay**2+y3**2*alphay**4*sigy3**2)/(x3**4)
sig2=(((num*ac)**2-(alphay*y2)**2)*sigx2**2+alphay**2*y2**4*sigalphay**2+y2**2*alphay**4*sigy2**2)/(x2**4)
#sig3=(((num*ac)**2-(alphay*y1)**2)*sigx1**2+alphay**2*y1**4*sigalphay**2+y1**2*alphay**4*sigy1**2)/(x1**4)

xmean4=0
xmean4=(c/sig1+b/sig2)/(1/sig1+1/sig2)
Fehlerx4=0
Fehlerx4=1/np.sqrt(1/sig1+1/sig2)
print("Mittelwert: ",xmean4)
print("Fehler: ", Fehlerx4)


# Current 3nm
num=6
ac=0.246
x1=1.24
y1=0.84
x2=1.38
y2=0.39
x3=0.15
y3=1.23

print("-- Current 3nm --")

print("-> x")
a=(np.sqrt(((num*ac)**2-(alphay*y2)**2)/x2**2))

b=(np.sqrt(((num*ac)**2-(alphay*y1)**2)/x1**2))

sigx1=0.05
sigx2=0.05
sigx3=0.05
sigy1=0.05
sigy2=0.05
sigy3=0.05
sig1=0
sig2=0
sig3=0

#sig1=(((num*ac)**2-(alphay*y3)**2)*sigx3**2+alphay**2*y3**4*sigalphay**2+y3**2*alphay**4*sigy3**2)/(x3**4)
sig2=(((num*ac)**2-(alphay*y2)**2)*sigx2**2+alphay**2*y2**4*sigalphay**2+y2**2*alphay**4*sigy2**2)/(x2**4)
sig3=(((num*ac)**2-(alphay*y1)**2)*sigx1**2+alphay**2*y1**4*sigalphay**2+y1**2*alphay**4*sigy1**2)/(x1**4)

xmean5=0
xmean5=(c/sig3+b/sig2)/(1/sig3+1/sig2)
Fehlerx5=0
Fehlerx5=1/np.sqrt(1/sig3+1/sig2)
print("Mittelwert: ",xmean5)
print("Fehler: ", Fehlerx5)


# Current 2nm
num=6
ac=0.246
x1=0.008
y1=1.25
x2=1.326
y2=0.863
x3=1.348
y3=0.386

print("-- Current 2nm --")

print("-> x")
a=(np.sqrt(((num*ac)**2-(alphay*y3)**2)/x3**2))

b=(np.sqrt(((num*ac)**2-(alphay*y2)**2)/x2**2))

sigx1=0.05
sigx2=0.05
sigx3=0.05
sigy1=0.05
sigy2=0.05
sigy3=0.05
sig1=0
sig2=0
sig3=0

sig1=(((num*ac)**2-(alphay*y3)**2)*sigx3**2+alphay**2*y3**4*sigalphay**2+y3**2*alphay**4*sigy3**2)/(x3**4)
sig2=(((num*ac)**2-(alphay*y2)**2)*sigx2**2+alphay**2*y2**4*sigalphay**2+y2**2*alphay**4*sigy2**2)/(x2**4)
#sig3=(((num*ac)**2-(alphay*y1)**2)*sigx1**2+alphay**2*y1**4*sigalphay**2+y1**2*alphay**4*sigy1**2)/(x1**4)

xmean6=0
xmean6=(c/sig1+b/sig2)/(1/sig1+1/sig2)
Fehlerx6=0
Fehlerx6=1/np.sqrt(1/sig1+1/sig2)
print("Mittelwert: ",xmean6)
print("Fehler: ", Fehlerx6)



print("-- Gesamter Mittelwert --")
print("-> x")

alphax=0
alphax=(xmean1/Fehlerx1**2+xmean2/Fehlerx2**2+xmean3/Fehlerx3**2+xmean4/Fehlerx4**2+xmean5/Fehlerx5**2+xmean6/Fehlerx6**2)/(1/Fehlerx1**2+1/Fehlerx2**2+1/Fehlerx3**2+1/Fehlerx4**2+1/Fehlerx5**2+1/Fehlerx6**2)
alphax=np.mean([xmean1,xmean2,xmean3,xmean4,xmean5,xmean6])
sigalphax=0
sigalphax=1/np.sqrt(1/Fehlerx1**2+1/Fehlerx2**2+1/Fehlerx3**2+1/Fehlerx4**2+1/Fehlerx5**2+1/Fehlerx6**2)
sigalphax=np.sqrt(np.var([Fehlerx1,Fehlerx2,Fehlerx3,Fehlerx4,Fehlerx5,Fehlerx6]))

print("Alpha x: ",alphax, "+-", sigalphax)
print(".................................................................")



# # Gain 4nm
# num=15
# ac=0.246
# x1=0.22
# y1=2.56
# x2=3.07
# y2=1.79
# x3=3.3
# y3=0.78

# print("-- Gain 4nm --")

# print("-> x")
# print(np.sqrt(((num*ac)**2-(alphay*y3)**2)/x3**2))

# print(np.sqrt(((num*ac)**2-(alphay*y2)**2)/x2**2))

# print(np.sqrt(((num*ac)**2-(alphay*y1)**2)/x1**2))

# xmean1=0
# xmean1=np.mean([np.sqrt(((num*ac)**2-(alphay*y3)**2)/x3**2), np.sqrt(((num*ac)**2-(alphay*y2)**2)/x2**2), np.sqrt(((num*ac)**2-(alphay*y1)**2)/x1**2)])

# print(xmean1)


# # Gain 12,5nm
# num=35
# ac=0.246
# x1=0.63
# y1=6.24
# x2=7.69
# y2=1.92
# x3=7.04
# y3=4.35

# print("-- Gain 12,5nm --")

# print("-> x")
# print(np.sqrt(((num*ac)**2-(alphay*y3)**2)/x3**2))

# print(np.sqrt(((num*ac)**2-(alphay*y2)**2)/x2**2))

# print(np.sqrt(((num*ac)**2-(alphay*y1)**2)/x1**2))

# xmean2=0
# xmean2=np.mean([np.sqrt(((num*ac)**2-(alphay*y3)**2)/x3**2), np.sqrt(((num*ac)**2-(alphay*y2)**2)/x2**2), np.sqrt(((num*ac)**2-(alphay*y1)**2)/x1**2)])

# print(xmean2)


# # Gain 1,5nm
# num=5
# ac=0.246
# x1=0.271
# y1=0.595
# x2=1.155
# y2=0.158
# x3=0.882
# y3=0.439

# print("-- Gain 1,5nm --")

# print("-> x")
# print(np.sqrt(((num*ac)**2-(alphay*y3)**2)/x3**2))

# print(np.sqrt(((num*ac)**2-(alphay*y2)**2)/x2**2))

# print(np.sqrt(((num*ac)**2-(alphay*y1)**2)/x1**2))

# xmean3=0
# xmean3=np.mean([np.sqrt(((num*ac)**2-(alphay*y3)**2)/x3**2), np.sqrt(((num*ac)**2-(alphay*y2)**2)/x2**2), np.sqrt(((num*ac)**2-(alphay*y1)**2)/x1**2)])

# print(xmean3)


# # Current 4nm
# num=11
# ac=0.246
# x1=0.02
# y1=2.24
# x2=2.43
# y2=0.71
# x3=2.42
# y3=1.52

# print("-- Current 4nm --")

# print("-> x")
# print(np.sqrt(((num*ac)**2-(alphay*y3)**2)/x3**2))

# print(np.sqrt(((num*ac)**2-(alphay*y2)**2)/x2**2))

# print(np.sqrt(((num*ac)**2-(alphay*y1)**2)/x1**2))

# xmean4=0
# xmean4=np.mean([np.sqrt(((num*ac)**2-(alphay*y3)**2)/x3**2), np.sqrt(((num*ac)**2-(alphay*y2)**2)/x2**2), np.sqrt(((num*ac)**2-(alphay*y1)**2)/x1**2)])

# print(xmean4)


# # Current 3nm
# num=6
# ac=0.246
# x1=1.24
# y1=0.84
# x2=1.38
# y2=0.39
# x3=0.15
# y3=1.23

# print("-- Current 3nm --")

# print("-> x")
# print(np.sqrt(((num*ac)**2-(alphay*y3)**2)/x3**2))

# print(np.sqrt(((num*ac)**2-(alphay*y2)**2)/x2**2))

# print(np.sqrt(((num*ac)**2-(alphay*y1)**2)/x1**2))

# xmean5=0
# xmean5=np.mean([np.sqrt(((num*ac)**2-(alphay*y3)**2)/x3**2), np.sqrt(((num*ac)**2-(alphay*y2)**2)/x2**2), np.sqrt(((num*ac)**2-(alphay*y1)**2)/x1**2)])

# print(xmean5)


# # Current 2nm
# num=6
# ac=0.246
# x1=0.008
# y1=1.25
# x2=1.326
# y2=0.863
# x3=1.348
# y3=0.386

# print("-- Current 2nm --")

# print("-> x")
# print(np.sqrt(((num*ac)**2-(alphay*y3)**2)/x3**2))

# print(np.sqrt(((num*ac)**2-(alphay*y2)**2)/x2**2))

# print(np.sqrt(((num*ac)**2-(alphay*y1)**2)/x1**2))

# xmean6=0
# xmean6=np.mean([np.sqrt(((num*ac)**2-(alphay*y3)**2)/x3**2), np.sqrt(((num*ac)**2-(alphay*y2)**2)/x2**2), np.sqrt(((num*ac)**2-(alphay*y1)**2)/x1**2)])

# print(xmean6)


# print("-> x")
# print(np.mean([xmean1,xmean2,xmean3,xmean4,xmean5,xmean6]))