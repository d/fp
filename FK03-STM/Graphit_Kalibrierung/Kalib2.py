#%%
import numpy as np

# Gain 4nm
num=15
ac=0.246
x1=0.22
y1=2.56
x2=3.07
y2=1.79
x3=3.3
y3=0.78

print(num*ac*np.sqrt((1-(x2/x1)**2)/(y2**2-(y1*x2/x1)**2)))


# Gain 12,5nm
num=35
ac=0.246
x1=0.63
y1=6.24
x2=7.69
y2=1.92
x3=7.04
y3=4.35

print(num*ac*np.sqrt((1-(x2/x1)**2)/(y2**2-(y1*x2/x1)**2)))


# Gain 1,5nm
num=5
ac=0.246
x1=0.271
y1=0.595
x2=1.155
y2=0.158
x3=0.882
y3=0.439

print(num*ac*np.sqrt((1-(x2/x1)**2)/(y2**2-(y1*x2/x1)**2)))


# Current 4nm
num=11
ac=0.246
x1=0.02
y1=2.24
x2=2.43
y2=0.71
x3=2.42
y3=1.52

print(num*ac*np.sqrt((1-(x2/x1)**2)/(y2**2-(y1*x2/x1)**2)))


# Current 3nm
num=6
ac=0.246
x1=1.24
y1=0.84
x2=1.38
y2=0.39
x3=0.15
y3=1.23

print(num*ac*np.sqrt((1-(x2/x3)**2)/(y2**2-(y3*x2/x3)**2)))


# Current 2nm
num=6
ac=0.246
x1=0.008
y1=1.25
x2=1.326
y2=0.863
x3=1.348
y3=0.386

print(num*ac*np.sqrt((1-(x2/x1)**2)/(y2**2-(y1*x2/x1)**2)))
