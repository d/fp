#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 12 14:05:59 2019

@author: schoko
"""


#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 11 15:24:40 2019

@author: schoko
"""
import sys 
if '/home/schoko/Documents/grundpraktikum-2/Python/plottingTemplate' not in sys.path:
    sys.path.append('/home/schoko/Documents/grundpraktikum-2/Python/plottingTemplate')

import plotting as pt
import matplotlib.pyplot as plt
import matplotlib
import scipy as sc
import scipy.optimize as opt
import numpy as np
from scipy.special import gamma

font = {'family' : 'DejaVu Sans',
        'weight' : 'normal',
        'size'   : 18}

matplotlib.rc('font', **font)
#matplotlib.rc('text', usetex=False)

gausscurve = lambda x,A,x0,s,b : A*np.exp(-(x-x0)**2/(2*s**2))+b

poisscurve = lambda x,mu,s : mu**(x)*np.exp(-mu)/gamma(x+1) 

exponcurve = lambda x,A,a : 1/A*np.exp(-1/A*x)

linfit = lambda x,b,a : b+a*x

def chisquare (x,y,xerr,yerr,curve, mean, var):
    h=0.0002
    curveabl=[(curve(xi+h/2 ,mean, var)-curve(xi-h/2,mean, var))/h for xi in x]
    return sum([(y[i]-curve(x[i],mean, var))**2/((yerr[i])**2 +(curveabl[i]*xerr[i])**2 ) if y[i]!=0 else 0 for i in range(len(x))])

def maxablei (x,y):
    best=0
    index=0
    for i in range(len(x)-1):
        abl=(y[i+1]-y[i])/(x[i+1]-x[i])
        #print(abl)
        if abl<best:
            best, index=abl, i
    xerr=(x[index+1]-x[index])/np.sqrt(12)
    yerr=(y[index+1]-y[index])/np.sqrt(12)
    return (x[index+1]+x[index])/2 , (y[index+1]+y[index])/2 , xerr, yerr

filenames=["expe", "theo"]#,"Data"]



angles=[]
counts=[]
j=0
norm=[1,1,1]
for i in range(len(filenames)):
    angles.append([])
    counts.append([])
    for line in open(filenames[i]+".dat","r"):
        if filenames[i]=="Data":
            l=line.split('\t')
            angles[j].append(float(l[0]))
            counts[j].append(float(l[1])*norm[i])
        else:
            l=line.split('     ')
            #print(l)
            angles[j].append(float(l[1]))
            counts[j].append(float(l[2])*norm[i])
    j+=1

plt.figure(figsize=(10,8))
plt.xlabel('$\Theta$ in grad')
plt.ylabel('Intensitaet')
colors=["red", "green", "blue", "brown", "yellow"]
name=['experimentelle Daten', 'Best Fitting Curve']
for i in range(len(filenames)):
    pt.plot(angles[i],counts[i] ,name[i],'best',colors[i],'-')  
#plt.vlines(direkt[n], 0, 10000)

plt.grid()
plt.yscale('log')
#plt.xlim([min(delta1s[n]),max(delta1s[n])])
#plt.xlim([0.3,5])
#plt.ylim([1000,40000])
plt.subplots_adjust(left=0.1, right=1, top=1, bottom=0.1)
plt.savefig('best'+'.pdf')#'Linearscale'+'.pdf')
plt.show()

