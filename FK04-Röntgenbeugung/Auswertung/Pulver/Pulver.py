#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 11 15:24:40 2019

@author: schoko
"""
import sys 
if '/home/schoko/Documents/grundpraktikum-2/Python/plottingTemplate' not in sys.path:
    sys.path.append('/home/schoko/Documents/grundpraktikum-2/Python/plottingTemplate')

import plotting as pt
import matplotlib.pyplot as plt
import matplotlib
import scipy as sc
import scipy.optimize as opt
import numpy as np
from scipy.special import gamma

font = {'family' : 'DejaVu Sans',
        'weight' : 'normal',
        'size'   : 18}

matplotlib.rc('font', **font)
#matplotlib.rc('text', usetex=False)

gausscurve = lambda x,A,x0,s,b : A*np.exp(-(x-x0)**2/(2*s**2))+b

poisscurve = lambda x,mu,s : mu**(x)*np.exp(-mu)/gamma(x+1) 

exponcurve = lambda x,A,a : 1/A*np.exp(-1/A*x)

linfit = lambda x,b,a : b+a*x

def chisquare (x,y,xerr,yerr,curve, mean, var):
    h=0.0002
    curveabl=[(curve(xi+h/2 ,mean, var)-curve(xi-h/2,mean, var))/h for xi in x]
    return sum([(y[i]-curve(x[i],mean, var))**2/((yerr[i])**2 +(curveabl[i]*xerr[i])**2 ) if y[i]!=0 else 0 for i in range(len(x))])

filenames=["Sample1", "Sample2"]

maxcounts=[[27.370,
            31.709,
            45.455,
            53.87,
            56.483,
            66.239,
            73.086,
            75.304,
            84.099,
            90.430
            ],[24.481,
             28.341,
             40.512,
             47.905,
             50.179,
             58.634,
             66.383,
             73.695,
             87.651,
             94.523]]

peakcounts=[[27.370,
            31.709,
            45.455,
            #53.87,
            56.483,
            66.239,
            #73.086,
            75.304,
            84.099,
            #90.430
            ],[#24.481,
             28.341,
             40.512,
             #47.905,
             50.179,
             58.634,
             66.383,
             73.695,
             87.651,
             94.523]]

peakcountsdic={27.370 : "Peak 1",
                31.709: "Peak 2",
                45.455: "Peak 3",
                #53.87: "Peak 4",
                56.483: "Peak 5",
                66.239: "Peak 6",
                #73.086: "Peak 7",
                75.304: "Peak 8",
                84.099: "Peak 9",
                #90.430: "Peak 10",
                #24.481: "Peak 1",
                 28.341: "Peak 2",
                 40.512: "Peak 3",
                 #47.905: "Peak 4",
                 50.179: "Peak 5",
                 58.634: "Peak 6",
                 66.383: "Peak 7",
                 73.695: "Peak 8",
                 87.651: "Peak 9",
                 94.523: "Peak 10"}

lablePeak = ['(NaCl)','(KCl)' ]

for i in range(len(filenames)):
    angle=[]
    counts=[]
    data=10000000
    for line in open(filenames[i]+".txt","r"):
        data-=1
        if "[Data]" in line:
            data=1
        if data<0:
            l=line.split(',')
            angle.append(float(l[0]))
            counts.append(float(l[1]))
    
    #print(angle)
    
    counts=np.array(counts)/max(counts)
    maxcounts[i]=np.array(maxcounts[i])-0.2
    #angle=np.array(angle)/2.0
    
    #delete=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 21, 23, 31]
    #volt = np.delete(np.array(volt), delete)
    #counts = np.delete(np.array(counts), delete)
    
    plt.figure(figsize=(10,8))
    plt.xlabel('$\Theta$ in grad')
    plt.ylabel('$Counts/Count_{max}$')

    
    xpoints=np.linspace(min(angle),max(angle),500)
    
    plt.vlines(maxcounts[i],0,1.01*max(counts) ,"red",label="Theoretische Peaks "+lablePeak[i], linewidth=1.2)
    
    pt.plot(angle,counts,filenames[i],'best','blue','-')
    plt.grid()
    #plt.yscale('log')
    #plt.xlim([25,95])
    plt.subplots_adjust(left=0.1, right=1, top=1, bottom=0.1)
    plt.savefig(filenames[i]+'.pdf')
    plt.show()
    
    

    for peak in peakcounts[i]:
        plt.figure(figsize=(10,8))
        plt.xlabel('$\Theta$ in grad')
        plt.ylabel('$Counts/Count_{max}$')
    
        a,b=0,0
        for xi in angle:
            if xi<(peak-3) :
                a+=1
            elif xi>(peak+3) :
                b-=1
        countint=counts[a:b]
        angleint=angle[a:b]
        xpoints=np.linspace(angle[a],angle[b],500)
        
        guess=[max(countint),peak,0.1, 0.06]
        params, corr = opt.curve_fit(gausscurve, angleint, countint, p0=guess )
        
        print(round(params[0],3),round(corr[0][0],3))
        print(round(params[1],3),round(corr[1][1],3))
        print(round(params[2],3),round(corr[2][2],3))
        print(round(params[3],3),round(corr[3][3],3))
        print(round(params[0]*params[2]*np.sqrt(2*np.pi),5))
        #print([i if counts[i]==1 else '' for i in range(len(counts))])
        #chiq=chisquare( volt, rate,[0 for i in counts], [np.sqrt(i)/30 for i in counts], linfit ,params[0],params[1])
    
        #plt.vlines(maxcounts[i],0,1.01 ,"red",label="$Peak$ = "+str(round(maxcounts[i][0],2)), linewidth=1.2)
        #plt.vlines(maxcounts[i][0],0,1.01 ,"red",label="$K_{alpha 1}$ = "+str(round(maxcounts[i][0],2)), linewidth=1.2)
        #plt.vlines(maxcounts[i][1],0,1.01 ,      label="$K_{alpha 2}$ = "+str(round(maxcounts[i][1],2)), linewidth=1.2)
        pt.plot(angleint,countint,peakcountsdic[peak],'best','blue','-')
        
        pt.plot(xpoints,gausscurve(xpoints, params[0],params[1], params[2],params[3]),'Fit:','best','red','-')
        
        pt.plot([params[1],params[1]+0.0000001], [0,0] ,' $\Theta_0$ = {}\n Hoehe = {}\n Breite = {}\n Flaeche = {}\n '.format(round(params[1],3),round(params[0],3),round(abs(params[2]),3),round(abs(params[0]*params[2]*np.sqrt(2*np.pi)),4)),'best' ,'white')
        
        plt.grid()
        #plt.yscale('log')
        #plt.xlim([25,95])
        plt.subplots_adjust(left=0.1, right=1, top=1, bottom=0.1)
        plt.savefig(filenames[i]+str(int(peak))+'.pdf')
        plt.show()

area=[0.03539,0.4,0.21693,0,0.10459,0,0,0.04952,0.0484,0]
for a in area:
    print(a/max(area))
