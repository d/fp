#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 12 14:05:59 2019

@author: schoko
"""


#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 11 15:24:40 2019

@author: schoko
"""
import sys 
if '/home/schoko/Documents/grundpraktikum-2/Python/plottingTemplate' not in sys.path:
    sys.path.append('/home/schoko/Documents/grundpraktikum-2/Python/plottingTemplate')

import plotting as pt
import matplotlib.pyplot as plt
import matplotlib
import scipy as sc
import scipy.optimize as opt
import numpy as np
from scipy.special import gamma

font = {'family' : 'DejaVu Sans',
        'weight' : 'normal',
        'size'   : 18}

matplotlib.rc('font', **font)
#matplotlib.rc('text', usetex=False)

gausscurve = lambda x,A,x0,s,b : A*np.exp(-(x-x0)**2/(2*s**2))+b

poisscurve = lambda x,mu,s : mu**(x)*np.exp(-mu)/gamma(x+1) 

exponcurve = lambda x,A,a : 1/A*np.exp(-1/A*x)

linfit = lambda x,b,a : b+a*x

def chisquare (x,y,xerr,yerr,curve, mean, var):
    h=0.0002
    curveabl=[(curve(xi+h/2 ,mean, var)-curve(xi-h/2,mean, var))/h for xi in x]
    return sum([(y[i]-curve(x[i],mean, var))**2/((yerr[i])**2 +(curveabl[i]*xerr[i])**2 ) if y[i]!=0 else 0 for i in range(len(x))])

def maxablei (x,y):
    best=0
    index=0
    for i in range(len(x)-1):
        abl=(y[i+1]-y[i])/(x[i+1]-x[i])
        #print(abl)
        if abl<best:
            best, index=abl, i
    xerr=(x[index+1]-x[index])/np.sqrt(12)
    yerr=(y[index+1]-y[index])/np.sqrt(12)
    return (x[index+1]+x[index])/2 , (y[index+1]+y[index])/2 , xerr, yerr

filenames=[["Dun1_02_20", "Dun1_15_40", "Dun1_37_100"],
           [ "Dun2DY_02_20", "Dun2DY_14_25", "Dun2DY_17_40", "Dun2DY_27_40_ni", "Dun2DY_37_100"]]

kalpha=1.54059 /10#in nm

af=np.arcsin(0.6/10)
print(af)
cu1=52.5
norm=[[1,cu1,7000],[1,cu1,cu1,2750,11800]]

delta1s=[[0.95,1.05,1.13,1.22,1.31,1.40,1.49,1.58,1.67,1.76,1.87,1.95,2.05,2.16,2.25,2.35,2.45,2.55,2.65,2.75,2.86,2.96,3.07,3.16,3.27,3.37,3.45,3.56,3.65,3.76,3.85,3.95,4.05,4.15,4.25,4.35,4.45],
         [0.61,0.81,0.90,1.00,1.13,1.24,1.35,1.48,1.60,1.72,1.84,1.95,2.07,     2.21,2.38,2.5 ,2.61,2.77,2.95,3.12,3.24,3.38,3.50,3.62,3.75,3.90,4.00]]

m1=[[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42],
    [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42]]

delta2s=[[1.6,3.2,4.8],[0.55,0.85,1.17,1.53,1.85,2.15,2.55,2.92,3.33,3.7,4.03,4.4]]

m2=[[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],
    [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]]

delta3s=[[],[0,2.5]]

m3=[[1,2,3,4,5,6,7,8,9,10],
    [1,2,3,4,5,6,7,8,9,10]]

critischeab=[[0.5,0.7],[0.45,0.7]]

cutting=[[0,0.75,1.85,6],[0,0.7,0.85,1.35,1.85,6]]

namenfile=['CoPtgegeben', 'CoPtMulti']

direkt=[0.28,0.28]

for n in range(len(filenames)):
    angles=[]
    counts=[]
    j=0
    for i in range(len(filenames[n])):
        angles.append([])
        counts.append([])
        data=10000000
        for line in open(filenames[n][i]+".txt","r"):
            data-=1
            if "[Data]" in line:
                data=1
            if data<0:
                l=line.split(',')
                angles[j].append(float(l[0])/2)
                counts[j].append(float(l[1]))
        j+=1
    
    plt.figure(figsize=(10,8))
    plt.xlabel('$\Theta$ in grad')
    plt.ylabel('Intensitaet')
    colors=["red", "green", "blue", "brown", "yellow"]
    for i in range(len(angles)):
        counts[i]=[counts[i][index]/norm[n][i] for index in range(len(counts[i]))] 
        counts[i]=[counts[i][index]/np.sin(angles[i][index]*np.pi/180) if (angles[i][index]*np.pi/180) < af else counts[i][index]/np.sin(af) for index in range(len(counts[i]))] 
        counts[i]=[counts[i][index] if angles[i][index]>direkt[n] else 0 for index in range(len(counts[i]))] 
        angles[i]=[angles[i][index] if angles[i][index]>direkt[n] else 0 for index in range(len(angles[i]))] 
        
        while 0 in angles[i]:
            angles[i].remove(0) 
        while 0 in counts[i]:
            counts[i].remove(0) 
            
            
    maxcounts=max(counts[0]) 
    for i in range(len(angles)):
        name=filenames[n][i].split('_')
        counts[i]=[counts[i][index]/maxcounts for index in range(len(counts[i]))] 
        
        pt.plot(angles[i],counts[i] ,"Messung von {0} bis {1}".format(float(name[1])/20,float(name[2])/20),'best',colors[i],'-')  
    #plt.vlines(direkt[n], 0, 10000)
    
    a,b = angles[0].index(critischeab[n][0]), angles[0].index(critischeab[n][1])
    xkrit,ykrit,xerrkrit,yerrkrit=maxablei(angles[0][a:b], counts[0][a:b])
    print(xkrit)
    pt.plot_errorbar(xkrit,ykrit,0,0,"kritischer Punkt\n$\Theta_{crit}$ = "+"${0}^\circ$".format(round(xkrit,5)),'best')
    plt.grid()
    plt.yscale('log')
    #plt.xlim([min(delta1s[n]),max(delta1s[n])])
    #plt.xlim([0.2,3])
    #plt.ylim([1000,40000])
    plt.subplots_adjust(left=0.1, right=1, top=1, bottom=0.1)
    plt.savefig(filenames[n][0]+'.pdf')#+'Linearscale'+'.pdf')
    plt.show()
    
    angle=[]
    count=[]
    for m in range(len(angles)):
        for i in range(len(angles[m])):
            if cutting[n][m] < angles[m][i] < cutting[n][m+1] :
                angle.append(angles[m][i])
                count.append(counts[m][i])
        
        
    plt.figure(figsize=(10,8))
    plt.xlabel('$\Theta$ in grad')
    plt.ylabel('Intensitaet')
    colors=["red", "green", "blue", "brown", "yellow"]
    pt.plot(angle,count ,"Messung von {0} bis {1}".format(float(name[1])/20,float(name[2])/20),'best','blue','-')  
    #plt.vlines(direkt[n], 0, 10000)
    plt.grid()
    plt.yscale('log')
    #plt.xlim([min(delta1s[n]),max(delta1s[n])])
    #plt.xlim([0.5,1])
    #plt.ylim([1000,40000])
    plt.subplots_adjust(left=0.1, right=1, top=1, bottom=0.1)
    plt.savefig(filenames[n][0]+'zusammen'+'.pdf')#'Linearscale'+'.pdf')
    plt.show()
    '''
    f=open(namenfile[n]+".dat","w")
    for line in ['         {0}        {1}\n'.format(angle[i],(count[i]*0.9)) for i in range(len(count))]: #*0.9
        f.write(line)
    f.close()
    '''
    
    wlen=[(delta1s[n][i+1]-delta1s[n][i])*np.pi/180 for i in range(len(delta1s[n])-1)]
    print('Delta phi: ',np.mean(wlen), np.sqrt(np.var(wlen)))
    print('d :',kalpha/np.mean(wlen)/2, np.sqrt(np.var(wlen)))
    
    
    wlen=[(delta1s[n][i+1]-delta1s[n][i]) for i in range(len(delta1s[n])-1)]
    print('Delta phi: ',np.mean(wlen), np.sqrt(np.var(wlen)))
    print('d :',kalpha/np.mean(wlen)/2, np.sqrt(np.var(wlen)))
    '''
    plt.figure(figsize=(10,8))
    plt.xlabel('$\Theta$ in grad')
    plt.ylabel('$Counts/Count_{max}$')
    colors=["red", "green", "blue", "brown", "yellow"]
    pt.plot(delta1s[n][0:-1],wlen,' Delta 1','best','red','-')
    plt.grid()
    #plt.yscale('log')
    #plt.xlim([0,5])
    plt.xlim([min(delta1s[n]),max(delta1s[n])])
    #plt.ylim([0.1,0.2])
    plt.subplots_adjust(left=0.1, right=1, top=1, bottom=0.1)
    plt.savefig(filenames[n][0]+"delta2_m_plot"+'.pdf')
    plt.show()
    '''
    wlen=[(delta2s[n][i+1]-delta2s[n][i])*np.pi/180 for i in range(len(delta2s[n])-1)]
    print('Delta phi: ',np.mean(wlen), np.sqrt(np.var(wlen)))
    print('d :',kalpha/np.mean(wlen)/2, np.sqrt(np.var(wlen)))
    wlen=[(delta2s[n][i+1]-delta2s[n][i]) for i in range(len(delta2s[n])-1)]
    print('Delta phi: ',np.mean(wlen), np.sqrt(np.var(wlen)))
    print('d :',kalpha/np.mean(wlen)/2, np.sqrt(np.var(wlen)))
    '''
    plt.figure(figsize=(10,8))
    plt.xlabel('$\Theta$ in grad')
    plt.ylabel('$Counts/Count_{max}$')
    colors=["red", "green", "blue", "brown", "yellow"]
    pt.plot(delta2s[n][0:-1],[(delta2s[n][i+1]-delta2s[n][i]) for i in range(len(delta2s[n])-1)],' Delta 2','best','green','-')
    plt.grid()
    #plt.yscale('log')
    #plt.xlim([0,5])
    #plt.ylim([0.1,0.2])
    plt.subplots_adjust(left=0.1, right=1, top=1, bottom=0.1)
    plt.savefig(filenames[n][0]+"delta2_m_plot"+'.pdf')
    plt.show()
    '''
    wlen=[(delta3s[n][i+1]-delta3s[n][i])*np.pi/180 for i in range(len(delta3s[n])-1)]
    print('Delta phi: ',np.mean(wlen), np.sqrt(np.var(wlen)))
    print('d :',kalpha/np.mean(wlen)/2, np.sqrt(np.var(wlen)))
    wlen=[(delta3s[n][i+1]-delta3s[n][i]) for i in range(len(delta3s[n])-1)]
    print('Delta phi: ',np.mean(wlen), np.sqrt(np.var(wlen)))
    print('d :',kalpha/np.mean(wlen)/2, np.sqrt(np.var(wlen)))
    
    

    
