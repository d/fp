;RAW4.00
[RawHeader]
Date=03/11/2019
EstimatedTotalTime=0
FurtherDQLReading=0
HardwareIndicator=65519
MeasuredTotalTime=0
MeasurementFlag=1
NumberOfMeasuredRanges=1
NumberOfRanges=1
Time=10:41:00

[HardwareConfiguration]
AbsorptionFactor=0
ActivateAbsorber=0
Alpha1=1.5406
Alpha2=1.54439
AlphaAverage=1.54184
AlphaRatio=0.5
Analyzer=0
Anode=Cu
AntiScatteringSlit=0.15
BeamOpticsFlags=32
Beta=1.39222
BetaRelativeIntensity=0
DeactivateAbsorber=0
DetectorSlit=1
DivergenceSlit=0.15
GoniometerControl=9999
GoniometerDiameter=600
GoniometerModel=257
GoniometerStage=6
Monochromator=0
NearSampleSlit=1
PrimarySollerSlit=1
SampleChanger=0
SecondSollerSlit=2
SynchronousAxis=0
ThinFilmAttachment=0.46
WaveUnit=A

[VarInfo]
Type=OSUSER
Flags=0
Value=Praktikum

[VarInfo]
Type=USER
Flags=0
Value=Uwe Klemradt

[VarInfo]
Type=SITE
Flags=0
Value=Aachen

[VarInfo]
Type=CREATOR
Flags=0
Value=XRD Commander

[RangeHeader]
ActuallyUsedLambda=1.5406
AdditionalDetectorMask=0
DataRecordLength=4
DelayTime=0
DisplayPlaneNumber=0
EstimatedScanTime=0
ExtraParametersMask=0
GeneratorCurrent=40
GeneratorVoltage=40
Increment=0.01
Increment3=0
NumberOfCompletedData=201
NumberOfCounts=1
NumberOfDetectors=1
NumberOfDrives=6
NumberOfEncoderDrives=0
NumberOfMeasuredData=201
NumberOfVaryingParameters=0
RangeStartTime=1084227584
RotationSpeed=0
ScanMode=0
ScanType=Unlocked Coupled
SimulatedMeasConditions=0
SlitChangerIn=9999
SmootingWidth=0
Start=68
Steps=201
SynchronousRotation=0
Time=1

[Drive]
DriveFlags=1
DriveName=Theta
DriveNumber=1
DriveOffset=0
OffsetIndex=0
OscillationAmplitude=0
OscillationSpeed=0
StartPosition=33.9912

[Drive]
DriveFlags=1
DriveName=2Theta
DriveNumber=2
DriveOffset=0
OffsetIndex=0
OscillationAmplitude=0
OscillationSpeed=0
StartPosition=68

[Drive]
DriveFlags=1
DriveName=Phi
DriveNumber=3
DriveOffset=0
OffsetIndex=0
OscillationAmplitude=0
OscillationSpeed=0
StartPosition=90

[Drive]
DriveFlags=1
DriveName=Chi
DriveNumber=4
DriveOffset=0
OffsetIndex=0
OscillationAmplitude=0
OscillationSpeed=0
StartPosition=-0.165

[Drive]
DriveFlags=0
DriveName=X-Drive
DriveNumber=5
DriveOffset=0
OffsetIndex=0
OscillationAmplitude=0
OscillationSpeed=0
StartPosition=0

[Drive]
DriveFlags=0
DriveName=Y-Drive
DriveNumber=6
DriveOffset=0
OffsetIndex=0
OscillationAmplitude=0
OscillationSpeed=0
StartPosition=6.408

[Detector]
AmplifierGain=3
ConstantDeadTime=0
DetectorFlags=256
DetectorName=Detector 1
DetectorNumber=1
DetectorType=1
Discriminator1LowerLevel=0.8
Discriminator1WindowWidth=1.4
Discriminator2LowerLevel=0
Discriminator2WindowWidth=0
HighVoltage=900
PileUpTime=0
PulseShaping=9999
TTLDetectorName=

[Data]
     Angle, Det1Disc1,
        68,        67,
     68.01,        78,
     68.02,        63,
     68.03,        77,
     68.04,        70,
     68.05,        68,
     68.06,        78,
     68.07,        67,
     68.08,        60,
     68.09,        80,
      68.1,        61,
     68.11,        68,
     68.12,        84,
     68.13,        74,
     68.14,        86,
     68.15,        83,
     68.16,        96,
     68.17,        83,
     68.18,        75,
     68.19,        88,
      68.2,        81,
     68.21,        88,
     68.22,        92,
     68.23,        83,
     68.24,        78,
     68.25,       108,
     68.26,        96,
     68.27,        72,
     68.28,        79,
     68.29,       101,
      68.3,        97,
     68.31,        77,
     68.32,       101,
     68.33,       100,
     68.34,       110,
     68.35,        84,
     68.36,       116,
     68.37,        95,
     68.38,       121,
     68.39,       101,
      68.4,        98,
     68.41,       114,
     68.42,       119,
     68.43,       109,
     68.44,       124,
     68.45,       117,
     68.46,       130,
     68.47,       143,
     68.48,       119,
     68.49,        94,
      68.5,       143,
     68.51,       137,
     68.52,       144,
     68.53,       155,
     68.54,       150,
     68.55,       144,
     68.56,       140,
     68.57,       160,
     68.58,       162,
     68.59,       155,
      68.6,       164,
     68.61,       161,
     68.62,       179,
     68.63,       196,
     68.64,       183,
     68.65,       170,
     68.66,       200,
     68.67,       196,
     68.68,       223,
     68.69,       291,
      68.7,       271,
     68.71,       256,
     68.72,       258,
     68.73,       332,
     68.74,       349,
     68.75,       377,
     68.76,       440,
     68.77,       430,
     68.78,       503,
     68.79,       542,
      68.8,       580,
     68.81,       635,
     68.82,       611,
     68.83,       696,
     68.84,       693,
     68.85,       737,
     68.86,       755,
     68.87,       780,
     68.88,       725,
     68.89,       762,
      68.9,       784,
     68.91,       799,
     68.92,       780,
     68.93,       810,
     68.94,       834,
     68.95,       853,
     68.96,       936,
     68.97,      1034,
     68.98,      1177,
     68.99,      1290,
        69,      1470,
     69.01,      1841,
     69.02,      2131,
     69.03,      2770,
     69.04,      3465,
     69.05,      4905,
     69.06,      6893,
     69.07,     10679,
     69.08,     15027,
     69.09,     21662,
      69.1,     28013,
     69.11,     34323,
     69.12,     38499,
     69.13,     40584,
     69.14,     41379,
     69.15,     41879,
     69.16,     42743,
     69.17,     43414,
     69.18,     44239,
     69.19,     43712,
      69.2,     39542,
     69.21,     31954,
     69.22,     24124,
     69.23,     17681,
     69.24,     13582,
     69.25,     11147,
     69.26,     10444,
     69.27,     11662,
     69.28,     13506,
     69.29,     17130,
      69.3,     19578,
     69.31,     21744,
     69.32,     22348,
     69.33,     22792,
     69.34,     22140,
     69.35,     21484,
     69.36,     20859,
     69.37,     20391,
     69.38,     19485,
     69.39,     17527,
      69.4,     14994,
     69.41,     11355,
     69.42,      8537,
     69.43,      5968,
     69.44,      4379,
     69.45,      3152,
     69.46,      2382,
     69.47,      1899,
     69.48,      1416,
     69.49,      1218,
      69.5,      1095,
     69.51,       859,
     69.52,       789,
     69.53,       735,
     69.54,       682,
     69.55,       563,
     69.56,       563,
     69.57,       518,
     69.58,       494,
     69.59,       419,
      69.6,       448,
     69.61,       410,
     69.62,       337,
     69.63,       356,
     69.64,       337,
     69.65,       301,
     69.66,       338,
     69.67,       279,
     69.68,       271,
     69.69,       270,
      69.7,       247,
     69.71,       261,
     69.72,       241,
     69.73,       223,
     69.74,       232,
     69.75,       225,
     69.76,       191,
     69.77,       190,
     69.78,       187,
     69.79,       165,
      69.8,       191,
     69.81,       216,
     69.82,       156,
     69.83,       163,
     69.84,       170,
     69.85,       153,
     69.86,       143,
     69.87,       136,
     69.88,       146,
     69.89,       112,
      69.9,       130,
     69.91,       117,
     69.92,       119,
     69.93,       144,
     69.94,       130,
     69.95,       126,
     69.96,       130,
     69.97,       109,
     69.98,       107,
     69.99,       132,
        70,       125,
