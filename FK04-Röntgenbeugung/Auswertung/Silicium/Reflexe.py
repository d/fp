#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 11 15:24:40 2019

@author: schoko
"""
import sys 
if '/home/schoko/Documents/grundpraktikum-2/Python/plottingTemplate' not in sys.path:
    sys.path.append('/home/schoko/Documents/grundpraktikum-2/Python/plottingTemplate')

import plotting as pt
import matplotlib.pyplot as plt
import matplotlib
import scipy as sc
import scipy.optimize as opt
import numpy as np
from scipy.special import gamma

font = {'family' : 'DejaVu Sans',
        'weight' : 'normal',
        'size'   : 18}

matplotlib.rc('font', **font)
#matplotlib.rc('text', usetex=False)

Ka2= 8027.8416 #eV
Ka1= 8047.8227

angst=12398.41875 

Ka2,Ka1 = angst/Ka2 , angst/Ka1
print("Ka1,Ka2:",Ka1,Ka2)

gausscurve = lambda x,A,x0,s,b : A*np.exp(-(x-x0)**2/(2*s**2)) +b

dgausscurve = lambda x,A1,A2,x0,x1,s1,s2,b : A1*np.exp(-(x-x0)**2/(2*s1**2))+A2*np.exp(-(x-x1)**2/(2*s2**2))+b

poisscurve = lambda x,mu,s : mu**(x)*np.exp(-mu)/gamma(x+1) 

exponcurve = lambda x,A,a : 1/A*np.exp(-1/A*x)

linfit = lambda x,b,a : b+a*x

def chisquare (x,y,xerr,yerr,curve, mean, var):
    h=0.0002
    curveabl=[(curve(xi+h/2 ,mean, var)-curve(xi-h/2,mean, var))/h for xi in x]
    return sum([(y[i]-curve(x[i],mean, var))**2/((yerr[i])**2 +(curveabl[i]*xerr[i])**2 ) if y[i]!=0 else 0 for i in range(len(x))])

filenames=["400Reflex","331Reflex","422Reflex"]#,
#filenames=["200ReflexCuAbsorb","200ReflexNiAbsorb"]
lable_curve =["Reflexe zu {400}","Reflexe zu {331}","Reflexe zu {422}"]#,
#lable_curve =["Reflexe zu {200} \nmit Cupfer absorber","Reflexe zu {200} \nmit Nickel Absorber"]

hkl=[4,np.sqrt(19),np.sqrt(24)]

maxcounts=[[69.18, 69.325],[76.4, 76.63],[88.06, 88.33]]#,
#maxcounts=[[32.99],[33]]
for i in range(len(filenames)):
    angle=[]
    counts=[]
    data=10000000
    for line in open(filenames[i]+".txt","r"):
        data-=1
        if "[Data]" in line:
            data=1
        if data<0:
            l=line.split(',')
            angle.append(float(l[0]))
            counts.append(float(l[1]))
    
    #print()
    
    counts=np.array(counts)
    maxcounts[i]=np.array(maxcounts[i])/2.0
    angle=np.array(angle)/2.0
    
    #delete=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 21, 23, 31]
    #volt = np.delete(np.array(volt), delete)
    #counts = np.delete(np.array(counts), delete)

    xpoints=np.linspace(850,1700,500)
    plt.figure(figsize=(11,8))
    plt.xlabel('$\Theta$ in grad')
    plt.ylabel('$Counts/Count_{max}$')
    
    guess=[max(counts), 0.5*max(counts) ,maxcounts[i][0], maxcounts[i][1],0.03,0.03, 0]
    params, corr = opt.curve_fit(dgausscurve, angle, counts, p0=guess )
    '''
    guess=[max(counts),maxcounts[i][0],0.03, 0]
    params, corr = opt.curve_fit(gausscurve, angle, counts, p0=guess )
    '''
    rou=9
    print(hkl[i])
    
    print(round(params[0],rou),round(corr[0][0],rou))
    print(round(params[1],rou),round(corr[1][1],rou))
    print(round(params[2],rou),round(corr[2][2],rou))
    print(round(params[3],rou),round(corr[3][3],rou))
    print(round(params[4],rou),round(corr[4][4],rou))
    print(round(params[5],rou),round(corr[5][5],rou))
    
    print('a dur K1 : ',round(Ka1*hkl[i]/(2*np.sin(params[2]*np.pi/180)),5))
    print('a dur K2 : ',round(Ka2*hkl[i]/(2*np.sin(params[3]*np.pi/180)),5))
    
    flK1, flK1err =params[0]*params[4]*np.sqrt(2*np.pi), np.sqrt(corr[0][0]/params[0]**2+corr[4][4]/params[4]**2)*params[0]*params[4]*np.sqrt(2*np.pi)
    flK2, flK2err =params[1]*params[5]*np.sqrt(2*np.pi), np.sqrt(corr[1][1]/params[1]**2+corr[5][5]/params[5]**2)*params[1]*params[5]*np.sqrt(2*np.pi)
    
    
    print('FlaecheK1: ',round(flK1,5), round(flK1err,5))
    print('FlaecheK2: ',round(flK2,5), round(flK2err,5))
    
    print('Fla K2/K1: ',round(params[1]*params[5]/(params[0]*params[4]),5),round(np.sqrt((flK1err/flK1)**2+(flK2err/flK2)**2)*params[1]*params[5]/(params[0]*params[4]),5))
    
    #print([i if counts[i]==1 else '' for i in range(len(counts))])
    #chiq=chisquare( volt, rate,[0 for i in counts], [np.sqrt(i)/30 for i in counts], linfit ,params[0],params[1])
    #pt.plot_errorbar(volt,rate,[0 for i in counts], [np.sqrt(i)/30 for i in counts],"points", 'best')
    
    xpoints=np.linspace(angle[0],angle[-1],500)
    
    pt.plot(xpoints,dgausscurve(xpoints, params[0],params[1], params[2],params[3], params[4],params[5], params[6]),'Fit ','best','red','-')
    #pt.plot(xpoints,gausscurve(xpoints, params[0],params[1], params[2],params[3]),'Fit ','best','red','-')
    
    #plt.vlines(params[1],0,max(counts)*1.01 ,"red",label="$Peak$ = "+str(round(params[1],2)), linewidth=1.2)
   
    plt.vlines(params[2],0,max(counts)*1.01 ,"red",label="$K_{alpha 1}$ = "+str(round(params[2],2)), linewidth=1.2)
    plt.vlines(params[3],0,max(counts)*1.01 ,      label="$K_{alpha 2}$ = "+str(round(params[3],2)), linewidth=1.2)
    pt.plot(angle,counts,lable_curve[i],'best','blue','-')
    
    plt.grid()
    #plt.yscale('log')
    plt.subplots_adjust(left=0.1, right=1, top=1, bottom=0.1)
    plt.savefig(filenames[i]+'.pdf')
    plt.show()

gittera=[ 5.42863, 5.42934, 5.4293, 5.42995, 5.42901, 5.4296]
print('a und siga:', np.mean(gittera), np.sqrt(np.var(gittera)))


'''
filenames=["200ReflexCuAbsorb","200ReflexNiAbsorb"]
lable_curve =["Reflexe zu {200} \nmit Cupfer absorber","Reflexe zu {200} \nmit Nickel Absorber"]

maxcounts=[[32.99],[33]]
for i in range(len(filenames)):
    angle=[]
    counts=[]
    data=10000000
    for line in open(filenames[i]+".txt","r"):
        data-=1
        if "[Data]" in line:
            data=1
        if data<0:
            l=line.split(',')
            angle.append(float(l[0]))
            counts.append(float(l[1]))
    
    #print()
    
    counts=np.array(counts)
    maxcounts[i]=np.array(maxcounts[i])/2.0
    angle=np.array(angle)/2.0
    
    

    xpoints=np.linspace(850,1700,500)
    plt.figure(figsize=(11,8))
    plt.xlabel('$\Theta$ in grad')
    plt.ylabel('$Counts/Count_{max}$')
    
    
    guess=[max(counts),maxcounts[i][0],0.03, 0]
    params, corr = opt.curve_fit(gausscurve, angle, counts, p0=guess )
    
    rou=9
    print(round(params[0],rou),round(corr[0][0],rou))
    print(round(params[1],rou),round(corr[1][1],rou))
    print(round(params[2],rou),round(corr[2][2],rou))
    print(round(params[3],rou),round(corr[3][3],rou))
    #print([i if counts[i]==1 else '' for i in range(len(counts))])
    #chiq=chisquare( volt, rate,[0 for i in counts], [np.sqrt(i)/30 for i in counts], linfit ,params[0],params[1])
    #pt.plot_errorbar(volt,rate,[0 for i in counts], [np.sqrt(i)/30 for i in counts],"points", 'best')
    
    xpoints=np.linspace(angle[0],angle[-1],500)
    
    pt.plot(xpoints,gausscurve(xpoints, params[0],params[1], params[2],params[3]),'Fit ','best','red','-')
    
    plt.vlines(params[1],0,max(counts)*1.01 ,"red",label="$Peak$ = "+str(round(params[1],2)), linewidth=1.2)
   
    pt.plot(angle,counts,lable_curve[i],'best','blue','-')
    
    plt.grid()
    #plt.yscale('log')
    plt.subplots_adjust(left=0.1, right=1, top=1, bottom=0.1)
    plt.savefig(filenames[i]+'.pdf')
    plt.show()
'''