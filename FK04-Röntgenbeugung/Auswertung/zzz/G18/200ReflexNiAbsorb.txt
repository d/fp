;RAW4.00
[RawHeader]
Date=03/11/2019
EstimatedTotalTime=0
FurtherDQLReading=0
HardwareIndicator=65519
MeasuredTotalTime=0
MeasurementFlag=3
NumberOfMeasuredRanges=1
NumberOfRanges=1
Time=11:58:17

[HardwareConfiguration]
AbsorptionFactor=0
ActivateAbsorber=0
Alpha1=1.5406
Alpha2=1.54439
AlphaAverage=1.54184
AlphaRatio=0.5
Analyzer=0
Anode=Cu
AntiScatteringSlit=0.15
BeamOpticsFlags=32
Beta=1.39222
BetaRelativeIntensity=0
DeactivateAbsorber=0
DetectorSlit=1
DivergenceSlit=0.15
GoniometerControl=9999
GoniometerDiameter=600
GoniometerModel=257
GoniometerStage=6
Monochromator=0
NearSampleSlit=1
PrimarySollerSlit=1
SampleChanger=0
SecondSollerSlit=2
SynchronousAxis=0
ThinFilmAttachment=0.46
WaveUnit=A

[VarInfo]
Type=OSUSER
Flags=0
Value=Praktikum

[VarInfo]
Type=USER
Flags=0
Value=Uwe Klemradt

[VarInfo]
Type=SITE
Flags=0
Value=Aachen

[VarInfo]
Type=CREATOR
Flags=0
Value=XRD Commander

[RangeHeader]
ActuallyUsedLambda=1.5406
AdditionalDetectorMask=0
DataRecordLength=4
DelayTime=0
DisplayPlaneNumber=0
EstimatedScanTime=0
ExtraParametersMask=0
GeneratorCurrent=40
GeneratorVoltage=40
Increment=0.01
Increment3=0
NumberOfCompletedData=191
NumberOfCounts=1
NumberOfDetectors=1
NumberOfDrives=6
NumberOfEncoderDrives=0
NumberOfMeasuredData=191
NumberOfVaryingParameters=0
RangeStartTime=1084227584
RotationSpeed=0
ScanMode=0
ScanType=Unlocked Coupled
SimulatedMeasConditions=0
SlitChangerIn=9999
SmootingWidth=0
Start=32
Steps=201
SynchronousRotation=0
Time=1

[Drive]
DriveFlags=1
DriveName=Theta
DriveNumber=1
DriveOffset=0
OffsetIndex=0
OscillationAmplitude=0
OscillationSpeed=0
StartPosition=15.982

[Drive]
DriveFlags=1
DriveName=2Theta
DriveNumber=2
DriveOffset=0
OffsetIndex=0
OscillationAmplitude=0
OscillationSpeed=0
StartPosition=32

[Drive]
DriveFlags=1
DriveName=Phi
DriveNumber=3
DriveOffset=0
OffsetIndex=0
OscillationAmplitude=0
OscillationSpeed=0
StartPosition=90

[Drive]
DriveFlags=1
DriveName=Chi
DriveNumber=4
DriveOffset=0
OffsetIndex=0
OscillationAmplitude=0
OscillationSpeed=0
StartPosition=0.182

[Drive]
DriveFlags=0
DriveName=X-Drive
DriveNumber=5
DriveOffset=0
OffsetIndex=0
OscillationAmplitude=0
OscillationSpeed=0
StartPosition=0

[Drive]
DriveFlags=0
DriveName=Y-Drive
DriveNumber=6
DriveOffset=0
OffsetIndex=0
OscillationAmplitude=0
OscillationSpeed=0
StartPosition=6.408

[Detector]
AmplifierGain=3
ConstantDeadTime=0
DetectorFlags=256
DetectorName=Detector 1
DetectorNumber=1
DetectorType=1
Discriminator1LowerLevel=0.8
Discriminator1WindowWidth=1.4
Discriminator2LowerLevel=0
Discriminator2WindowWidth=0
HighVoltage=900
PileUpTime=0
PulseShaping=9999
TTLDetectorName=

[Data]
     Angle, Det1Disc1,
        32,         6,
     32.01,         7,
     32.02,         4,
     32.03,         4,
     32.04,         3,
     32.05,         5,
     32.06,         3,
     32.07,         3,
     32.08,         4,
     32.09,         2,
      32.1,         2,
     32.11,         8,
     32.12,         6,
     32.13,         7,
     32.14,         4,
     32.15,         5,
     32.16,         6,
     32.17,        11,
     32.18,         6,
     32.19,         7,
      32.2,         9,
     32.21,         3,
     32.22,         6,
     32.23,         5,
     32.24,         6,
     32.25,        11,
     32.26,        11,
     32.27,         6,
     32.28,        10,
     32.29,         8,
      32.3,        17,
     32.31,        11,
     32.32,        12,
     32.33,        14,
     32.34,        13,
     32.35,         8,
     32.36,         5,
     32.37,         7,
     32.38,        14,
     32.39,         4,
      32.4,        10,
     32.41,         8,
     32.42,         6,
     32.43,        10,
     32.44,         4,
     32.45,         7,
     32.46,         8,
     32.47,         9,
     32.48,         9,
     32.49,        10,
      32.5,         7,
     32.51,        10,
     32.52,        11,
     32.53,         9,
     32.54,         9,
     32.55,        12,
     32.56,         9,
     32.57,        13,
     32.58,         5,
     32.59,         6,
      32.6,        12,
     32.61,         4,
     32.62,         6,
     32.63,        12,
     32.64,        14,
     32.65,        15,
     32.66,         6,
     32.67,         9,
     32.68,         9,
     32.69,        17,
      32.7,        10,
     32.71,        10,
     32.72,         9,
     32.73,        15,
     32.74,        13,
     32.75,        11,
     32.76,        25,
     32.77,        25,
     32.78,        25,
     32.79,        21,
      32.8,        39,
     32.81,        37,
     32.82,        49,
     32.83,        52,
     32.84,        74,
     32.85,        64,
     32.86,       105,
     32.87,       128,
     32.88,       227,
     32.89,       294,
      32.9,       462,
     32.91,       714,
     32.92,      1082,
     32.93,      1692,
     32.94,      1936,
     32.95,      1962,
     32.96,      1929,
     32.97,      1908,
     32.98,      2008,
     32.99,      2214,
        33,      2438,
     33.01,      2504,
     33.02,      2439,
     33.03,      2244,
     33.04,      1942,
     33.05,      1673,
     33.06,      1211,
     33.07,      1053,
     33.08,       960,
     33.09,       842,
      33.1,       797,
     33.11,       598,
     33.12,       429,
     33.13,       291,
     33.14,       220,
     33.15,       122,
     33.16,        84,
     33.17,        54,
     33.18,        41,
     33.19,        35,
      33.2,        35,
     33.21,        33,
     33.22,        30,
     33.23,        18,
     33.24,        25,
     33.25,        15,
     33.26,        25,
     33.27,        18,
     33.28,        22,
     33.29,        15,
      33.3,        16,
     33.31,        16,
     33.32,        17,
     33.33,        18,
     33.34,        11,
     33.35,        30,
     33.36,        19,
     33.37,        23,
     33.38,        24,
     33.39,        17,
      33.4,        13,
     33.41,        20,
     33.42,        14,
     33.43,        16,
     33.44,        13,
     33.45,        16,
     33.46,        14,
     33.47,        13,
     33.48,        18,
     33.49,        13,
      33.5,        14,
     33.51,        20,
     33.52,        15,
     33.53,        14,
     33.54,        15,
     33.55,        21,
     33.56,        20,
     33.57,        15,
     33.58,        13,
     33.59,        12,
      33.6,        22,
     33.61,        15,
     33.62,        19,
     33.63,        13,
     33.64,        16,
     33.65,        17,
     33.66,        12,
     33.67,        15,
     33.68,        12,
     33.69,        22,
      33.7,         8,
     33.71,        15,
     33.72,        15,
     33.73,        15,
     33.74,        16,
     33.75,        11,
     33.76,        17,
     33.77,        18,
     33.78,        17,
     33.79,        11,
      33.8,        17,
     33.81,        18,
     33.82,         9,
     33.83,        17,
     33.84,        15,
     33.85,        14,
     33.86,        11,
     33.87,        19,
     33.88,        17,
     33.89,        14,
      33.9,        14,
