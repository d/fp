////////////////////////////////////////////////////////////////////////
//
// BreitWigner.C  (ROOT script for ROOT version 5.0 and higher)
//
// author = Thomas Hebbeker
//
// purpose: Breit-Wigner fit to Z cross section data
//          F-Praktikum B, L3-Versuch, RWTH Aachen
//
// version 2.1  2007-03-12
// version 2.2  2007-03-23   just a note: probably the "fudge factor"
//                           fac_norm of 1.083 contains a QCD correction 
//                           of 1.04, so only 4% are unaccounted for !
//                           could these be even higher order corrections ?
// version 2.3  2008-03-12   TMath::Max 
//                           update canvas before printing files (speed !)
// version 2.4  2008-03-26   print in .png format instead of .pdf 
//
// history: based on bwigner.f version 2.2 (original version  from 1995!)
//
// note: start this macro in ROOT with   .x  ./BreitWigner.C
//
/////////////////////////////////////////////////////////////////////////
//
// some details:
//
// - chisquare fit of breit-wigner curve to N (>=3) cross section points
//   near the z resonance; includes qed corrections
//
// - uses minimization package MINUIT inside ROOT
//   start points correspond to Z resonance, assuming units GeV and nb
//
// - the fit parameter sigma_0 is the >>> born level <<< peak cross section!
// 
/////////////////////////////////////////////////////////////////////////
//


Double_t QED_radiator(Double_t z, Double_t s)
{
//
/////////////////////////////////////////////////////////////////////////
//   
//  photon radiator function (QED correction to e+e- cross sections)
//  approximated following 
//
//    Z Physics at LEP 1, Vol. I, p. 106 formula (3.21)   [1]
//    (from O. Nicrosini and L. Trentadue, PL B 196 (1987) 551  [2])
//
//  z = s'/s      s' = reduced (center of mass energy)**2 
//                        due to photon radiation
//  s = (center of mass energy/GeV)**2 of e+e- collision
//
// Note: It is known that for hadronic final states qed corrections 
//       change the peak cross section by a factor 0.74
//       The formula used here apparantly does no lead to the 
//       correct absolute normalization, therefore factor fac_norm
//       has been introduced to adjust the cross sections.
//       Attention: this factor depends on the boundaries for the
//                  integration over z, see technical remarks below 
//
//  2007-03-12: explicitly checked that formulae in [1] and [2] agree
//              and that constants cons1 etc are calculated correctly
//              small bug (in cons3) found, but seems not important
//              numerically !
//
/////////////////////////////////////////////////////////////////////////
//

  const Double_t me2 = 2.611200E-7;         // electron mass squared in GeV
  const Double_t alphpi = 2.3228196E-3;     // alpha/pi
  const Double_t zeta2 = 1.644934;          // riemann zeta function (2)
  const Double_t zeta3 = 1.202;             // riemann zeta function (3)

  const Double_t cons1 = 9./8.-2*zeta2;
  const Double_t cons2 = -45./16.+11./2.*zeta2+3.*zeta3;
  const Double_t cons3 = -6./5.*zeta2*zeta2 -9./2.*zeta3 -6.*zeta2*log(2)
                         +3./8.*zeta2 +19./4.;

  //  const Double_t cons1 = -2.16487;
  //  const Double_t cons2 = 9.84064;
  //  const Double_t cons3 = -10.1302;

  const Double_t fac_norm = 1.083;

  // cout << " cons1 = " << cons1 << endl; 
  // cout << " cons2 = " << cons2 << endl; 
  // cout << " cons3 = " << cons3 << endl; 

  Double_t l;
  Double_t logz;
  Double_t beta;
  Double_t del1vs;
  Double_t del2vs;
  Double_t delvs;
  Double_t del1h;
  Double_t delbh;

  l = log(s/me2);
  logz = log(z);
  beta = 2.*alphpi*(l-1.);
  del1vs = alphpi*(3./2.*l+2.*zeta2-2.);
  del2vs = alphpi*alphpi*(cons1*l*l+cons2*l+cons3);
  delvs = 1. + del1vs + del2vs;
  del1h = -alphpi *(1.+z) * (l-1.);
  delbh = alphpi*alphpi/2.*(l-1.)*(l-1.)
           *((1.+z)*(3.*logz-4.*log(1.-z))
	    -4./(1.-z)*logz-5.-z);

  return ( beta * pow(1.-z,beta-1.) * delvs + del1h + delbh ) * fac_norm;
}


Double_t BreitWigner_integrand(Double_t *z, Double_t *par)
{
//
/////////////////////////////////////////////////////////////////////////
//   
// integrand for BreitWigner_QED
//
// z = s'/s      s' = reduced (center of mass energy)**2 
//                       due to photon radiation
//               s = (center of mass energy/GeV)**2 of e+e- collision
//
/////////////////////////////////////////////////////////////////////////
//   
  
  Double_t sigma_0 = par[0];       // peak cross section
  Double_t mZ_2 = par[1];          // Z mass squared
  Double_t GZ_2 = par[2];          // Z total width squared
  Double_t s = par[3];             // center of mass energy squared

  Double_t sz = s * z[0];

  return sigma_0 * sz * GZ_2 / ( (sz-mZ_2)*(sz-mZ_2) + mZ_2*GZ_2 ) 
         * QED_radiator(z[0], s);
}





Double_t BreitWigner_QED(Double_t *ss, Double_t *par)
{
//
/////////////////////////////////////////////////////////////////////////
//
// relativistiv Breit Wigner function with QED corrections folded in  
//
// to be used as ROOT fit function 
//
// input:
//    ss = sqrt s in GeV
//    par: see below   
//
// output:
//    cross section in nb 
//
/////////////////////////////////////////////////////////////////////////
//
  Double_t s = ss[0]*ss[0];        // center of mass energy squared
  Double_t sigma_0 = par[0];       // BORN peak cross section
  Double_t mZ_2 = par[1]*par[1];   // Z mass squared
  Double_t GZ_2 = par[2]*par[2];   // Z total width squared

// parameter choice: see old fortran version of 1995
// and comments at very end of this file
  
  Double_t z_min = 3600./s;
  Double_t delta = 1.E-12;
  Double_t z_max = 1.-delta;
  Double_t epsilon = 1.E-6;

  TF1 *hz = new TF1("BreitWigner_integrand",BreitWigner_integrand,
       z_min,z_max,4);

// Set Breit wigner values and parameter names:

  hz->SetParameters(sigma_0,mZ_2,GZ_2,s);
  hz->SetParNames("sigma_0/nb","mass**2_Z/GeV**2",
                     "Gamma**2_Z/GeV**2","s/GeV**2");

  const Double_t* params =0;
  const Double_t result = hz->Integral(z_min, z_max, params, epsilon);

  delete hz;
   
  return result;
  
}




void BreitWigner()
{
//
/////////////////////////////////////////////////////////////////////////
//   
// main program
//
// documentation see above
//
/////////////////////////////////////////////////////////////////////////
//   

  const int nmax = 100;
  double sqrts[nmax+1], xsect[nmax+1], dxsect[nmax+1];

//
// initialize, get and plot data
//

// say hello:

   cout << endl 
        << " >>> BreitWigner.C <<<  version 2.4   T.H.  2008-03-26 " 
        << endl;

// ask for cross section data:

   double ss = 1.;    
   double xs, dxs;
   int nn = 0;
   while (ss>0)
   { 
     cout << endl << " Eingabe sqrt(s)/GeV (negative Zahl = Ende): ";
     cin >> ss;
     if(ss > 0) 
     {
       nn++;
       cout << " Eingabe Wirkungsquerschnitt/nb            : ";
       cin >> xs;
       cout << " Eingabe Fehler Wirkungsquerschnitt/nb     : ";
       cin >> dxs;
       sqrts[nn] = ss;
       xsect[nn] = xs;
       dxsect[nn] = dxs;
     }
   }     

   cout << endl << " Die Messdaten sind: " << endl
        << "     sqrt s / GeV    cross section / nb " << endl;

   double ymax = 0.;
   for (int k=1; k<=nn; k++)
   {
      cout <<  " " << k << "        " 
           <<  sqrts[k] << "              " 
           <<  xsect[k] 
           << " +- " <<  dxsect[k] << endl;
      ymax = TMath::Max(ymax,xsect[k]);
   }
   cout << endl;


// some general options:
 
   gStyle->SetOptStat(0);

// open frame, white color:

   canvas1 = new TCanvas("canvas1", "Breit-Wigner-Fit");
   canvas1->SetFillColor(10);

// fill data points into histogram:

   const int n_data = 1000;
   const float xmin_data = 86.;
   const float xmax_data = 96.;
   TH1F *hdata = new TH1F("hdata"," L3 ",n_data,xmin_data,xmax_data);

   for (int k=1; k<=nn; k++)
   {
     int kbin = (sqrts[k]-xmin_data)/(xmax_data-xmin_data) * n_data + 1.;
     hdata->SetBinContent(kbin,xsect[k]);
     hdata->SetBinError(kbin,dxsect[k]);
   }

// plot data points:

   hdata->SetMinimum(0.);
   // set vertical axis differently for hadron and muon cross section
   Double_t ymax_rounded;
   if(ymax > 10.) 
   {  
     ymax_rounded = 40.;
   }
   else 
   {  
     ymax_rounded = 2.;
   }
   hdata->SetMaximum(ymax_rounded);
   hdata->SetMarkerStyle(20);
   hdata->SetMarkerSize(2.0);
   hdata->SetMarkerColor(2);
   hdata->SetLineWidth(2);
   hdata->Draw(" "); 

   canvas1->Update();

//
//  perform chisquare fit
//

// Create a Root function based on function BreitWigner_QED above:

   TF1 *htheory = new TF1("BreitWigner_QED",BreitWigner_QED,
       xmin_data,xmax_data,3);

// Set good estimates for initial values and define parameter names:
 
   Double_t sigma_0 = ymax*1.4;
   Double_t mass_Z = 91.;
   Double_t Gamma_Z = 2.5;   

// which fitmode ?

   cout << " Fitmodus: " << endl;
   cout << " 0: sigma_0, mass_Z und Gamma_Z werden gefittet" << endl;
   cout << " 1: nur sigma_0 wird gefittet " << endl;
   Int_t fitmode;
   cout << endl << " Eingabe Fitmodus (0 oder 1): ";   
   cin >> fitmode;
   if(fitmode == 1) 
   {
     cout << " Eingabe mass_Z / GeV: ";
     cin >> mass_Z;
     cout << " Eingabe Gamma_Z / GeV: ";
     cin >> Gamma_Z;
   }

   htheory->SetParameters(sigma_0, mass_Z, Gamma_Z);

   htheory->SetParLimits(0, sigma_0*0.5, sigma_0*1.5);
   htheory->SetParLimits(1, mass_Z*0.9, mass_Z*1.1);
   htheory->SetParLimits(2, Gamma_Z*0.7, Gamma_Z*1.3);

   if(fitmode==1) 
   {
     htheory->FixParameter(1, mass_Z);
     htheory->FixParameter(2, Gamma_Z);
   }	

   htheory->SetParNames("sigma_0/nb","mass_Z/GeV","Gamma_Z/GeV");
	       
   cout << endl << " fitting started ... be patient " << endl << endl;

// Fit histogram in range defined by function:

   hdata->Fit(htheory,"r");

// plot breit wigner fit curve, with some options:

   htheory->SetLineColor(3);
   htheory->SetLineWidth(3);
   htheory->Draw("same"); 

// add some text:

   texty = new TLatex(xmin_data-0.7,ymax_rounded*0.8,"#sigma/nb");
   texty->SetTextAngle(90.); 
   texty->Draw();
   textx = new TLatex(xmax_data-1.5,-ymax*0.13-0.05,"\\sqrt{s} / GeV");
   textx->Draw();

   canvas1->Update();

// print plot as jpg/pdf/gif file:

   cout << endl; 
   if(ymax>10) 
   {
     canvas1->Print("BreitWigner_hadrons.jpg");
     canvas1->Print("BreitWigner_hadrons.png");
     canvas1->Print("BreitWigner_hadrons.gif");
   }
   else
   {
     canvas1->Print("BreitWigner_muons.jpg");
     canvas1->Print("BreitWigner_muons.png");
     canvas1->Print("BreitWigner_muons.gif");
   }

   return;
}



//
/////////////////////////////////////////////////////////////////////////
//   
// technical remarks
//
// test input and output (see zreson-com.pdf), agrees with bwigner.f,
// for version 2.0 of BreitWigner.C
//
//  89.48 -> 9.8 +- 0.4                43.6 +- 1.0 nb
//  91.33 -> 32.1 +- 0.7        --->   91.23 +- 0.04 GeV
//  93.02 -> 15.1 +- 0.5                2.42 +- 0.07 GeV
//
// for version 2.1 of BreitWigner.C (bug in cons3 fixed)
//
//  89.48 -> 9.8 +- 0.4                43.6 +- 1.0 nb
//  91.33 -> 32.1 +- 0.7        --->   91.23 +- 0.04 GeV      same as 
//  93.02 -> 15.1 +- 0.5                2.42 +- 0.07 GeV      version 2.0 !
// 
// numerical results depend on the choice of zmin, zmax (delta), epsilon.
// tests reveal: zmin and epsilon are totally uncritical, but delta
// is delicate; example: keeping the input parameters as given above 
// leads to a weak dependence of m_Z and G_Z on delta, but a strong 
// dependence of sigma_0 on delta:
//      delta = 1E-10   sigma_0 = 45.9
//              1E-11             44.6
//              1E-12             43.6 
//              1E-13             42.8
//           even smaller -> numerical instabilities
// however, since fac_norm is chosen as to compensate for this,
// there is finally no problem - as long as delta and fac_norm are
// adapted simultaneously. 
//
/////////////////////////////////////////////////////////////////////////
//
