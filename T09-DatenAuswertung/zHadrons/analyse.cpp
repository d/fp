//
// ///////////////////////////////////////////////////////////////////////////
//
// >>> analyse.cpp <<<
//
// Beispielprogramm zum Einlesen von Daten und Fuellen eines Histogramms
//
// *********************************************************
//   Versuch 9, Fortgeschrittenenpraktikum B, RWTH Aachen  *
// *********************************************************
//
// T. Hebbeker   Vers.  2.0  2004-10-07
//                      2.1  2006-03-16   (V.Vorwerk: fin.open)
//                      2.2  2007-02-13   (T.Hebbeker: second fabs removed)
//                      2.3  2008-03-26   (T.Hebbeker: x and y axes text)
//                      2.4  2014-02-24   (T.Kress: adaptions for gcc@SL6)
//
// Allgemeine Bemerkungen:
//
//   C++ Kenntnisse werden vorausgesetzt.
//
//   Die Routinen TFile, TH1F, Fill, Write
//   dienen zur Erstellung von Histogrammen
//
// Erlaeuterungen zu diesem Programm:
//
//   - Class cevent dient zur Speicherung eines e+e- - Kollisions-
//       ereignisses und zur Extraktion der Impulse und Massen der
//       zugehoerigen Teilchen. Nicht veraendern!
//
//   - Die am Ende angehaengte subroutine read_event zum Einlesen
//       eines Ereignisses kann als black box betrachtet werden.
//
//   - Der Histogramm-file kann mit dem (graphischen) Analyseprogramm
//       ROOT eingelesen und weiterverarbeitet werden
//       siehe dazu das ROOT-Beispiel-Script plot.C
//
//   - Namensgebung fuer loop-Variable/Indices:
//      n...  = event counter
//      k...  = "particle" counter
//      l     = momentum component x, y, z
//
// ///////////////////////////////////////////////////////////////////////////
//

// general header files:
#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>

#include "math.h"

// needed for ROOT routines:
#include "TROOT.h"
#include "TFile.h"
#include "TH1.h"
#include "TF1.h"

using namespace std;


//
///////////////////////////////////////////////////////////////////////////
//
// class cevent enthaelt e+e- Daten (jeweils ein Ereignis):
//       ======
//
//  _ktotce = Zahl der Teilchen/Kalorimetertreffer ("particle") im Detektor
//
//   Fuer jedes Teilchen/Treffer k (1 ... _ktotce) ist gespeichert:
//
//  _pvecce(k,1) = Impulskomponente in x-Richtung (in LEP-Ebene)
//  _pvecce(k,2) = Impulskomponente in y-Richtung (senkrecht zu LEP-Ebene)
//  _pvecce(k,3) = Impulskomponente in z-Richtung (entlang e- Richtung)
//  _massce(k) = Masse
//  _charge(k) = Ladung (+- 1 fuer Myonen, sonst 0!)
//
//   Im Myonsdetektor rekonstruierte Myonen haben die Masse 0.106 GeV.
//     Diese wird zwar hier nicht gemessen, ist aber bekannt und kann damit
//     den Myonen zugeordnet werden.
//   Bei Kalorimetertreffern gibt es keine eindeutige Zuordnung
//     zwischen Treffer und Teilchen. Man kann insbesondere das Teilchen
//     weder identifizieren noch seine Ladung messen. Da aber die meisten
//     Teilchen in Jets Pionen sind, wird den Kalorimetertreffern
//     (etwas willkuerlich) die Masse 0.14 GeV zugeordnet.
//   Manchmal kann man Kalorimeterhits als elektromagnetischen Ursprungs
//    (Photon, Elektron) erkennen. Dann wird _massce = 0 gesetzt.
//
//  Alle Einheiten: GeV
//
//  Fuer die Analyse duerfen nur die folgenden Funktionen benutzt werden:
//    print();               druckt Inhalt von cevent und mehr (Bildschirm)
//    number_particles();    Zahl der Teilchen im betrachteten Ereignis
//    momentum(k,l);         Impulskomponente l (x,y,z=1,2,3) von Teilchen k
//    mass(k);               Masse von Teilchen k
//    charge(k);             Ladung von Teilchen k (nur sinnvoll fuer Myonen)
//


const int ktotmx=1000;      // maximal zulaessige Teilchenzahl pro Ereignis

class cevent {
private:
  int _ktotce;
  float _pvecce[ktotmx+1][4];
  float _massce[ktotmx+1];
  float _charge[ktotmx+1];
  bool _ready;
public:
  cevent();
  int setnce(int);
  int setce(int,float,float,float,float);
  int print();
  int number_particles();
  float momentum(int,int);
  float mass(int);
  float charge(int);
};


cevent::cevent()                    // Konstruktor
{
  _ktotce = -1;
  _ready = 0;
}


int cevent::setnce(int ktot)        //  zum Setzen von _ktotce
{                                   //  Danach muss alle Teilcheninformation
  int k;                            //  neu gefuellt werden!

  if(ktot<0) {
    cout << "ERROR cevent::setnce: " << ktot << "negative" << endl;
    return -9;
  }
  else if (ktot<ktotmx) {
    _ktotce = ktot;
    for (k=1; k<=_ktotce; k++) {
      _charge[k]=-9.;
    }
    return 0;
  }
  else {
    cout << "ERROR cevent::setnce: " << ktot << "too large" << endl;
    return -9;
  }
}


int cevent::setce(int k, float px, float py, float pz, float m)
{
  int kk;

  if(_ktotce < 1) {
    cout << "ERROR cevent::setce: _ktotce = " << _ktotce << endl;
    return -9;
  }
  else if (k<=_ktotce) {
    _pvecce[k][1] = px;
    _pvecce[k][2] = py;
    _pvecce[k][3] = pz;
    _massce[k] = fabs(m);
    _charge[k] = 0;
    if(_massce[k]>0) _charge[k] = m/fabs(m);
    _ready = 1;
    for (kk=1; kk<=_ktotce; kk++) {
      if(_charge[kk]<-1.) _ready = 0;
    }
    return 0;
  }
  else {
    cout << "ERROR cevent::setce: _ktotce > k = " << k << endl;
    return -8;
  }
}



int cevent::print()
{
  int k;
  float p2, ptotxe, etotxe;
  float pxtote, pytote, pztote, etote;
  float ptote, mtote;

  if(!_ready) {
    cout << "ERROR cevent::print:  event not (fully) initialized "<< endl;
    return -9;
  }

  cout << endl
       << "Dump von cevent + weitere Groessen: "
       << endl << endl
       << "particle"
       << "  px/GeV "
       << "  py/GeV "
       << "  pz/GeV "
       << " mass/GeV"
       << " ptot/GeV"
       << " etot/GeV" << endl << endl;

  pxtote = 0.;
  pytote = 0.;
  pztote = 0.;
  etote = 0.;

  for (k=1; k<=_ktotce; k++) {
//
// calculate more variables...:
//
    p2 = _pvecce[k][1]*_pvecce[k][1] +
         _pvecce[k][2]*_pvecce[k][2] +
         _pvecce[k][3]*_pvecce[k][3];
    ptotxe = sqrt(p2);
    etotxe = sqrt(p2 + _massce[k]*_massce[k]);

    pxtote = pxtote + _pvecce[k][1];
    pytote = pytote + _pvecce[k][2];
    pztote = pztote + _pvecce[k][3];
    etote  = etote  + etotxe;

    cout.width(5);
    cout << k << "  ";
    cout.setf(ios::fixed | ios::right);
    cout.precision(3);
    cout.width(9);
    cout << _pvecce[k][1];
    cout.width(9);
    cout << _pvecce[k][2];
    cout.width(9);
    cout << _pvecce[k][3];
    cout.width(9);
    cout << _massce[k];
    cout.width(9);
    cout << ptotxe;
    cout.width(9);
    cout << etotxe << endl;
  }

  ptote = sqrt(pxtote*pxtote+pytote*pytote+pztote*pztote);
  mtote = sqrt(etote*etote-ptote*ptote);

  cout.width(7);
  cout << endl << " TOTAL ";
  cout.width(9);
  cout << pxtote;
  cout.width(9);
  cout << pytote;
  cout.width(9);
  cout << pztote;
  cout.width(9);
  cout << mtote;
  cout.width(9);
  cout << ptote;
  cout.width(9);
  cout << etote << endl << endl;

  return 0;

}



int cevent::number_particles()
{

  if(!_ready) {
    cout << "ERROR cevent::number_particles: "
         <<  "event not (fully) initialized "<< endl;
    return -9;
  }

  return _ktotce;

}


float cevent::momentum(int k, int l)
{

  if(!_ready) {
    cout << "ERROR cevent::momentum:  event not (fully) initialized "<< endl;
    return -9;
  }

  return _pvecce[k][l];

}



float cevent::mass(int k)
{

  if(!_ready) {
    cout << "ERROR cevent::mass:  event not (fully) initialized "<< endl;
    return -9;
  }

  return _massce[k];

}


float cevent::charge(int k)
{

  if(!_ready) {
    cout << "ERROR cevent::charge:  event not (fully) initialized "<< endl;
    return -9;
  }

  return _charge[k];

}

///////////////////////////////////////////////////////////////////////////

string datfile;                       // name data file

int read_event(cevent &event);


int main()
{
  int n;
  int k;

  int result;

  int ktot;

  float px;

  char* hbooktitle;
  char* hropenname;
  char* hropenfile;
  char* hropenflag;

  int irecl, istat;
  int icycle;

  int nevmax;
  int nevent = 0;

  char hisfile[100];

//
// say hello
//
  cout << endl;
  cout << " >>> analyse.cpp <<<    C++ version  2.4   Feb-2014 \n";
  cout << endl;
//
//  Eingabeinformation:
//
  cout << "Bitte Namen des Datenfiles an 89gev" << endl;
  cin >> datfile;
  datfile=datfile + ".dat";
  cout << "Bitte Namen des Histogrammfiles angeben (e.g. 89gev.root)" << endl;
  cin >> hisfile;

  nevmax=1000000;

//
// initialize analysis package ROOT

   TFile *histofile = new TFile(hisfile,"RECREATE");
//
// HILFE:
//   Hier wurde ein File geoeffnet, in dem alle Histogramme
//   abgespeichert werden sollen.
//

   // TH1F *histo1 = new TH1F("histo1",
   //  " ;  Anzahl Teilchen pro Ereignis ; Anzahl",
   //  100,-0.5,99.5);

    TH1F *histo1 = new TH1F("histo1",
     " ;  normierte Energie des Endzustands ; Anzahl",
     30,-0.5,2.5);

//
// HILFE:
//   Hier wurde ein Histogramm histo1 reserviert,
//      mit 25 Bins zwischen -50. und 50.:
//        ->  histo1 ist der Name des Histogramms
//        -> `x Kompo ....' ist der Histogramm-Titel
//        ->  25 ist die Zahl der Bins = `Kaestchen'
//        -> -50. ist die untere Grenze (hier Impuls in GeV)
//                   der horizontalen Achse
//        -> +50. ... ... obere ....................................
//
//    Es koennen nach Belieben hier mehrere Histogramme definiert werden.
//

//
//  loop over all EVENTS
//
  for(n=1; n<=nevmax; n++) {
//
//  create event of type cevent
//
    cevent event;
//
//  read in one event from data file
//
    result = read_event(event);
//
//  note: read_event returns
//      0  if ....
//     -2 if end of file reached
//

//
// analyse only events which are not "empty"
//
    if(result==0) {

      nevent++;
//
//  print out a few events (debug):
//
      if(nevent<=5) {
        event.print();
      }
//
//  now loop over all PARTICLES within the current event
//
      ktot = event.number_particles();

      //if (ktot>18){
      //  histo1->Fill(ktot);
      //}

      long Etot= 0;
      for(k=1; k<=ktot; k++) {
        Etot+=sqrt(event.mass(k)*event.mass(k)
        +event.momentum(k,1)*event.momentum(k,1)
        +event.momentum(k,2)*event.momentum(k,2)
        +event.momentum(k,3)*event.momentum(k,3));
      }

      if (datfile=="89gev") Etot/=89.48;
      else if (datfile=="91gev") Etot/=91.33;
      else if (datfile=="93gev") Etot/=93.02;
      else Etot/=91.2;


      histo1->Fill(Etot);
    }
    else if(result==-2) {
      break;
    }
  }
//
//  Statistics:
//
  cout << "Zahl der analysierten Ereignisse = " << nevent << endl;

//
// save histograms to file:
//
  histofile->Write();
//[100]
// HILFE:  Alle Histogramme werden auf den oben definierten File
//         geschrieben
//

  if(nevent > 0) {
    return 0;
  }
  else {
    return -9;
  }
}

///////////////////////////////////////////////////////////////////


int read_event(cevent &event)
{
  int k;
  int l;
  int ktot;
  float px, py, pz, m;
  static bool first = 1;
  static ifstream fin;

  if(first) {
    cout << datfile << endl;
    fin.open(datfile,ios::in);
    first=0;
    if (!fin) {
      cout << "ERROR read_event: " << datfile << " cannot be read !" << endl;
      exit(-1);
    }
  }

  if(!fin.eof()) {
    ktot = -1;
    fin >> ktot;
    if(ktot > 0) {
      event.setnce(ktot);
      for (k=1; k<=ktot; k++) {
        fin >> l >> px >> py >> pz >> m;
        event.setce(l,px,py,pz,m);
      }
      return 0;
    }
    else if(ktot==0) {
      return -1;
    }
    else if(ktot <0) {
      fin.close();
      return -2;
    }
  }
  else {
    fin.close();
    return -2;
  }

  return -9;

}
